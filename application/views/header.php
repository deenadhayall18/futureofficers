<div class="menu">
	<form class="menu-search" method="POST" name="header_search_form">
		<!-- <div class="menu-search-icon"><i class="fa fa-search"></i></div> -->
		<div class="menu-search-input">
			<div class="text-center">
				<a href="<?php echo base_url() ?>homepage"><img src="<?php echo base_url(); ?>/packages/assets/img/logo.png" class='responsiveLogo'/></a>
			</div>		
		</div>
	</form>
	<?php if(!empty($this->session->userdata('auth'))){ ?>
	<div class="menu-item dropdown">
		<a href="#" data-toggle="dropdown" data-display="static" class="menu-link">
			<div class="menu-img online">
				<?php if(!empty($this->session->userdata('userpicture'))){ ?>
				<img src="<?php echo $this->session->userdata('userpicture'); ?>" alt="" class="mw-100 mh-100 rounded-circle" />
				<?php }else { ?>
				<img src="<?php echo base_url(); ?>packages/assets/img/user/user.png" alt="" class="mw-100 mh-100 rounded-circle" />
				<?php } ?>
			</div>
			<div class="menu-text"><?php echo ($this->session->userdata('useremail')); ?></div>
		</a>
		<div class="dropdown-menu dropdown-menu-right mr-lg-3">
			<a class="dropdown-item d-flex align-items-center" href="<?php echo base_url() ?>MainController/SignOut">Sign Out <i class="fa fa-toggle-off fa-fw ml-auto text-gray-400 f-s-16"></i></a>
		</div>
	</div>
	<?php }else{ ?>
	<!--Sign Up -->
	<div class="menu-item dropdown">
		<span data-toggle="dropdown" data-display="static"  id="modal">
			<button type="button" class="btn btn-xs btn-default">
				<h6 class=" text-black mb-1 text-center">Sign Up</h6>
			</button>
		</span>
		<div class="dropdown-menu dropdown-menu-right dropdown-notification">
			<div class="card-body">
				<div class="col-sm-12">
					<form method="post" action="MainController/SignUp" name="formUserSignUp" id="formUserSignUp">
						<div class="row">
							<div class="col-xl-12">
								<div class="form-group">
									<label>Enter Name</label>
									<input type="text" name="username" id="username" class="form-control" id="" placeholder="Enter Name" />
								</div>
								<div class="form-group">
									<label>Enter Email ID</label>
									<input type="email" name="useremail" id="useremail" class="form-control" id="" placeholder="Enter valid Email ID" />
								</div>
								<div class="form-group">
									<label>Enter Password</label>
									<input type="password" minlength="8" maxlength="8" id="password" name="password" class="form-control" id="" placeholder="Enter Password" />
								</div>
								<div class="form-group">
									<input type="submit" name="submit" value="Sign Up" class="form-control btn-success"/>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!--Sign Up -->
	&nbsp;&nbsp;
	<!--Sign In -->
	<div class="menu-item dropdown">
		<a href="#" data-toggle="dropdown" data-display="static" id="modal">
			<button type="button" class="btn btn-xs btn-secondary">
				<h6 class=" text-black mb-1 text-center">Sign In</h6>
			</button>
		</a>
		<div class="dropdown-menu dropdown-menu-right dropdown-notification">
			<div class="card-body">
				<div class="col-sm-12">
					<form method="post" action="MainController/SignIn" name="formUserSignIn" id="formUserSignIn">
						<div class="row">
							<div class="col-xl-12">
								<div class="form-group">
									<label>Enter Email ID</label>
									<input type="email" name="useremail" id="useremail" class="form-control" id="" placeholder="Enter valid Email ID" required />
								</div>
								<div class="form-group">
									<label>Enter Password</label>
									<input type="password" name="password" id="password" class="form-control" id="" placeholder="Enter Password" required/>
								</div>
								<div class="form-group">
									<input type="submit" name="submit" value="Sign In" class="form-control btn-success" id="" />
								</div>
								<div class="form-group text-center">
									<div class="col-xs-12">
										<div class="dropdown-notification-icon">
											<div class="menu-img text-center">
												<a href="MainController/GoogleLogin"><img src="<?php echo base_url(); ?>/packages/assets/img/google.png" alt="" class="mw-100 mh-100 rounded-circle" /></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!--Sign In -->
&nbsp;&nbsp;
<?php } ?>
</div>
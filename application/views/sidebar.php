	<!-- BEGIN #sidebar -->
	<sidebar id="sidebar" class="app-sidebar">
		<!-- BEGIN scrollbar -->
		<div class="app-sidebar-content" data-scrollbar="true" data-height="100%">
			<!-- BEGIN menu -->
			<div class="menu">
				<!-- <div class="menu-header">Menu</div> -->
				<div class="menu-item <?php echo (!empty(basename($_SERVER['PHP_SELF'])=='homepage')?'active':null) ?>">
					<a href="<?php echo base_url() ?>homepage" class="menu-link">
						<span class="menu-icon">
							<i class="fa fa-home"></i>
						</span>
						<span class="menu-text">Home</span>
					</a>
				</div>


				<div class="menu-item has-sub <?php echo (!empty(basename($_SERVER['PHP_SELF'])=='aboutTheDayList')?'active':null) ?>">
					<a href="#" class="menu-link">
						<span class="menu-icon"><i class="fa fa-book"></i></span>
						<span class="menu-text">About the Day</span> 
						<span class="menu-caret"><b class="caret"></b></span>
					</a>
					<div class="menu-submenu">
						<div class="menu-item <?php echo (!empty((basename($_SERVER['PHP_SELF'])=='aboutTheDayList')AND($_GET['lang']==2))?'active':null) ?>">
							<a href="<?php echo base_url().'aboutTheDayList?month='.(date('m')).'&'.'year='.(date('Y')).'&lang=2'; ?>" class="menu-link">
								<span class="menu-text">Hindi</span>
							</a>
						</div>
						<div class="menu-item <?php echo (!empty((basename($_SERVER['PHP_SELF'])=='aboutTheDayList')AND($_GET['lang']==1))?'active':null) ?>">
							<a href="<?php echo base_url().'aboutTheDayList?month='.(date('m')).'&'.'year='.(date('Y')).'&lang=1'; ?>" class="menu-link">
								<span class="menu-text">English</span>
							</a>
						</div>
					</div>
				</div>

				<div class="menu-item has-sub <?php echo (!empty(basename($_SERVER['PHP_SELF'])=='dailyCurrentAffairsList')?'active':null) ?>">
					<a href="#" class="menu-link">
						<span class="menu-icon"><i class="fa fa-book"></i></span>
						<span class="menu-text">Daily Current Affairs</span> 
						<span class="menu-caret"><b class="caret"></b></span>
					</a>
					<div class="menu-submenu">
						<div class="menu-item <?php echo (!empty((basename($_SERVER['PHP_SELF'])=='dailyCurrentAffairsList')AND($_GET['lang']==2))?'active':null) ?> ">
							<a href="<?php echo base_url().'dailyCurrentAffairsList?month='.(date('m')).'&'.'year='.(date('Y')).'&lang=2'; ?>" class="menu-link">
								<span class="menu-text">Hindi</span>
							</a>
						</div>
						<div class="menu-item <?php echo (!empty((basename($_SERVER['PHP_SELF'])=='dailyCurrentAffairsList')AND($_GET['lang']==1))?'active':null) ?>">
							<a href="<?php echo base_url().'dailyCurrentAffairsList?month='.(date('m')).'&'.'year='.(date('Y')).'&lang=1'; ?>" class="menu-link">
								<span class="menu-text">English</span>
							</a>
						</div>
					</div>
				</div>


				<div class="menu-item <?php echo (!empty(basename($_SERVER['PHP_SELF'])=='newsPaperAnalysisList')?'active':null) ?>">
					<a href="<?php echo base_url().'newsPaperAnalysisList?month='.(date('m')).'&'.'year='.(date('Y')); ?>" class="menu-link">
						<span class="menu-icon">
							<i class="fa fa-newspaper"></i>
						</span>
						<span class="menu-text">News paper analysis</span>
					</a>
				</div>
				<div class="menu-item <?php echo (!empty(basename($_SERVER['PHP_SELF'])=='dailyUPSCPrelimsQuestions')?'active':null) ?>">
					<a href="<?php echo base_url().'dailyUPSCPrelimsQuestions?month='.(date('m')).'&'.'year='.(date('Y')); ?>" class="menu-link">
						<span class="menu-icon">
							<i class="fa fa-question-circle"></i>
						</span>
						<span class="menu-text">Daily UPSC Prelim Ques</span>
					</a>
				</div>
				<div class="menu-item <?php echo (!empty(basename($_SERVER['PHP_SELF'])=='dailyUPSCMainQuestions')?'active':null) ?>">
					<a href="<?php echo base_url().'dailyUPSCMainQuestions?month='.(date('m')).'&'.'year='.(date('Y')); ?>" class="menu-link">
						<span class="menu-icon">
							<i class="fa fa-question"></i>
						</span>
						<span class="menu-text">Daily UPSC Mains Ques</span>
					</a>
				</div>


				<div class="menu-item <?php echo (!empty(basename($_SERVER['PHP_SELF'])=='preYearPrelimQuesList')?'active':null) ?>">
					<a href="<?php echo base_url().'preYearPrelimQuesList?month='.(date('m')).'&'.'year='.(date('Y')); ?>" class="menu-link">
						<span class="menu-icon">
							<i class="fa fa-list-alt"></i>
						</span>
						<span class="menu-text">Pre-Year UPSC Prelim Ques</span>
					</a>
				</div>

				<div class="menu-item <?php echo (!empty(basename($_SERVER['PHP_SELF'])=='preYearMainQuesList')?'active':null) ?>">
					<a href="<?php echo base_url().'preYearMainQuesList?month='.(date('m')).'&'.'year='.(date('Y')); ?>" class="menu-link">
						<span class="menu-icon">
							<i class="fa fa-list-alt"></i>
						</span>
						<span class="menu-text">Pre-Year UPSC Main Ques</span>
					</a>
				</div>

				<div class="menu-item <?php echo (!empty(basename($_SERVER['PHP_SELF'])=='yojanaList')?'active':null) ?>">
					<a href="<?php echo base_url().'yojanaList?month='.(date('m')).'&'.'year='.(date('Y')); ?>" class="menu-link" class="menu-link">
						<span class="menu-icon">
							<i class="fa fa-list"></i>
						</span>
						<span class="menu-text">Yojana</span>
					</a>
				</div>
				<div class="menu-item <?php echo (!empty(basename($_SERVER['PHP_SELF'])=='kurukshetraList')?'active':null) ?>">
					<a href="<?php echo base_url().'kurukshetraList?month='.(date('m')).'&'.'year='.(date('Y')); ?>" class="menu-link" class="menu-link">
						<span class="menu-icon">
							<i class="fa fa-file"></i>
						</span>
						<span class="menu-text">Kurukshetra</span>
					</a>
				</div>

				<div class="menu-item dropdown">

					<span class="p-3" data-toggle="dropdown" data-display="static"  id="modal">
						<button type="button" class="btn btn-block btn-secondary font-weight-600 rounded-pill">
							<h6 class=" text-black mb-1 text-center"><i class="fa fa-envelope mr-1 ml-n1 opacity-5"></i> Subscribe</h6>
						</button>
					</span>

					<div class="dropdown-menu dropdown-menu-center dropdown-notification">
						<div class="card-body">
							<div class="col-sm-12">
								<form method="post" action="MainController/Subscribe" name="frmSubscribe" id="frmSubscribe">
									<div class="row">
										<div class="col-xl-8">
											<div class="form-group">
												<label>Enter Email ID</label>
												<input type="email" name="sub_email" id="sub_email" class="form-control" id="" placeholder="Enter valid Email ID" required />
											</div>
											<div class="form-group">
												<input type="submit" name="submit" value="Subscribe" class="form-control btn-success"/>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>



				<div class="menu-item dropdown" style="padding-top:1px">
					<span class="p-3 text-center" data-toggle="dropdown" data-display="static"  id="modal">
						<img src="<?php echo base_url(); ?>/packages/assets/img/logo_left_side_bar.png" height="120px" width="160px" />
					</span><br>
					<span class="p-3" data-toggle="dropdown" data-display="static"  id="modal">
						<h6 class=" text-black mb-1 text-center">Copyright&nbsp;
							<i class="fa fa-copyright mr-1 ml-n1 opacity-5"></i>Future Officers</h6>
						</span>
					</div>
				</div>
				<!-- END menu -->
			</div>
			<!-- END scrollbar -->
			<!-- BEGIN mobile-sidebar-backdrop -->
			<button class="app-sidebar-mobile-backdrop" data-dismiss="sidebar-mobile"></button>
			<!-- END mobile-sidebar-backdrop -->
		</sidebar>
		<!-- END #sidebar -->
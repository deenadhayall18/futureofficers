<!-- BEGIN #content -->
<div id="content" class="app-content">
	<h1 class="page-header mb-3">
		Previous Year Main Question
	</h1> 		

	<!-- BEGIN row -->
	<div class="row">
		<!-- BEGIN col-6 -->
		<div class="col-xl-12">
			<!-- BEGIN row -->
			<div class="row">
				<!-- BEGIN col-6 -->
				<?php if(!empty($pymqMonthlyList)){
					foreach ($pymqMonthlyList as $pymqKey => $pymqVal) {?>
					<div class="col-sm-3">
						<!-- BEGIN card -->
						<?php $pymq = strrev(base64_encode($pymqVal['pymq_id'])); ?>
						<a style="text-decoration:none !important;" href="<?php echo base_url().'preYearMainQues?pymq_id='.$pymq.'&heading='.(!empty($pymqVal['pymq_heading'])?strtolower(json_decode($pymqVal['pymq_heading'])):null); ?>">
							<div class="card mb-3 overflow-hidden fs-13px border-0 " style="min-height: 202px;">
								<!-- BEGIN card-body -->
								<div class="card-body position-relative">
									<?php if($pymqVal['pymq_date']==date('Y-m-d')){?>
									<div class="d-flex mb-3">
										<div class=" d-flex align-items-center">
											<i class="fa fa-circle fs-2px fa-fw text-danger mr-2 blink_me"></i> <span style="color:red !important;font-weight:bold !important;text-decoration:none !important;cursor:pointer !important;">Posted Today</span>
										</div>
									</div>
									<?php } ?>
									<h5 class="text-black-transparent-8 mb-3 fs-16px">
										<!-- <?php echo ucwords($pymqVal['pymq_category']); ?> -->
									</h5>
									<img src="<?php echo str_replace('web', 'admin',base_url()); ?>/uploads/featured_images/<?php echo $pymqVal['pymq_featured_image'].'.png' ?>" width="100%" height="110px">
									<div><a href="<?php echo base_url().'preYearMainQues?pymq_id='.$pymq.'&heading='.(!empty($pymqVal['pymq_heading'])?strtolower(json_decode($pymqVal['pymq_heading'])):null); ?>" class="text-black d-flex align-items-center text-decoration-none">&nbsp;&nbsp;&nbsp;<span style="color:#834705;font-weight:bold"><?php echo  date('M d,  Y',strtotime($pymqVal['pymq_date'])); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="color:black;font-weight:bold"><?php echo (!empty($pymqVal['pymq_read_time'])?$pymqVal['pymq_read_time']:null) ?> min read</span></a></div>
								</div>
								<!-- BEGIN card-body -->
							</div>
						</a>
						<!-- END card -->
					</div>
					<?php } } ?>
				</div>
				<!-- END row -->
			</div>
			<!-- END col-6 -->
		</div>
		<!-- END row -->







	</div>
		<!-- END #content -->
<!-- BEGIN #content -->
<div id="content" class="app-content">
	<h1 class="page-header mb-3">
		News Paper Analysis List
	</h1> 		

	<!-- BEGIN row -->
	<div class="row">
		<!-- BEGIN col-6 -->
		<div class="col-xl-12">
			<!-- BEGIN row -->
			<div class="row">
				<!-- BEGIN col-6 -->
				<?php if(!empty($npaMonthlyList)){
					foreach ($npaMonthlyList as $npaKey => $npaVal) {?>
					<div class="col-sm-3">
						<!-- BEGIN card -->
						<?php $npa = strrev(base64_encode($npaVal['npa_id'])); ?>
						<a style="text-decoration:none !important;" href="<?php echo base_url().'newsPaperAnalysis?npa_id='.$npa.'&heading='.(!empty($npaVal['npa_heading'])?strtolower(json_decode($npaVal['npa_heading'])):null); ?>">
							<div class="card mb-3 overflow-hidden fs-13px border-0 " style="min-height: 202px;">
								<!-- BEGIN card-body -->
								<div class="card-body position-relative">
									<?php if($npaVal['npa_date']==date('Y-m-d')){?>
									<div class="d-flex mb-3">
										<div class=" d-flex align-items-center">
											<i class="fa fa-circle fs-2px fa-fw text-danger mr-2 blink_me"></i> <span style="color:red !important;font-weight:bold !important;text-decoration:none !important;cursor:pointer !important;">Posted Today</span>
										</div>
									</div>
									<?php } ?>
									<h5 class="text-black-transparent-8 mb-3 fs-16px">
										<!-- <?php echo ucwords($npaVal['npa_category']); ?> -->
									</h5>
									<img src="<?php echo str_replace('web', 'admin',base_url()); ?>/uploads/featured_images/<?php echo $npaVal['npa_featured_image'].'.png' ?>" width="100%" height="110px">
									<div><a href="<?php echo base_url().'newsPaperAnalysis?npa_id='.$npa.'&heading='.(!empty($npaVal['npa_heading'])?strtolower(json_decode($npaVal['npa_heading'])):null); ?>" class="text-black d-flex align-items-center text-decoration-none">&nbsp;&nbsp;&nbsp;<span style="color:#834705;font-weight:bold"><?php echo  date('M d,  Y',strtotime($npaVal['npa_date'])); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="color:black;font-weight:bold"><?php echo (!empty($npaVal['npa_read_time'])?$npaVal['npa_read_time']:null) ?> min read</span></a></div>
								</div>
								<!-- BEGIN card-body -->
							</div>
						</a>
						<!-- END card -->
					</div>
					<?php } } ?>
				</div>
				<!-- END row -->
			</div>
			<!-- END col-6 -->
		</div>
		<!-- END row -->







	</div>
		<!-- END #content -->
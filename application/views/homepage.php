


<div id="content" class="app-content">

	<?php if(!empty($this->session->flashdata("msg"))){ ?>	
	<div class="col-xl-6">
		<div class="alert alert-primary alert-dismissable show p-3">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $this->session->flashdata("msg"); ?>
		</div>
	</div>
	<?php } ?>




	<?php if(!empty($dcaList)){ ?>
	<!-- Daily Current Affairs  - Start -->
	<h1 class="page-header mb-3">
		Daily Current Affairs <small>Latest Posts</small>
	</h1> 		
	<?php } ?>


	<!-- BEGIN row -->
	<div class="row">
		<!-- BEGIN col-6 -->
		<div class="col-xl-12">
			<!-- BEGIN row -->
			<div class="row">
				<!-- BEGIN col-6 -->
				<?php if(!empty($dcaList)){ 
					foreach ($dcaList as $dcaKey => $dcaVal) {?>
					<div class="col-sm-3">
						<!-- BEGIN card -->
						<?php $dca = strrev(base64_encode($dcaVal['dca_id'])); ?>
						<a href="<?php echo base_url().'dailyCurrentAffairs?dca_id='.$dca.'&heading='.(!empty($dcaVal['dca_heading'])?strtolower(json_decode($dcaVal['dca_heading'])):null); ?>">
							<div class="card mb-3 overflow-hidden fs-13px border-0 " style="min-height: 202px;">
								<!-- BEGIN card-body -->
								<div class="card-body position-relative">
									<h5 class="text-black-transparent-8 mb-3 fs-16px"></h5>
									<img src="<?php echo str_replace('web', 'admin',base_url()); ?>/uploads/featured_images/<?php echo $dcaVal['dca_featured_image'].'.png' ?>" width="100%" height="110px">
									<div><a href="<?php echo base_url().'dailyCurrentAffairs?dca_id='.$dca.'&heading='.(!empty($dcaVal['dca_heading'])?strtolower(json_decode($dcaVal['dca_heading'])):null); ?>" class="text-black d-flex align-items-center text-decoration-none">&nbsp;&nbsp;&nbsp;<span style="color:red;font-weight:bold"><?php echo  date('M d,  Y',strtotime($dcaVal['dca_date'])); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="color:black;font-weight:bold"><?php echo (!empty($dcaVal['dca_read_time'])?$dcaVal['dca_read_time']:null) ?> min read</span></a></div>
								</div>
								<!-- BEGIN card-body -->
							</div>
						</a>
						<!-- END card -->
					</div>
					<?php } } ?>
				</div>
				<!-- END row -->
			</div>
			<!-- END col-6 -->
		</div>
		<!-- END row -->
		<!-- Daily Current Affairs  - End -->



		<?php if(!empty($aboutdayList)){ ?>
		<!-- About the Day  - Start -->
		<h1 class="page-header mb-3">
			About the Day <small>Latest Posts</small>
		</h1> 		
		<?php } ?>


		<!-- BEGIN row -->
		<div class="row">
			<!-- BEGIN col-6 -->
			<div class="col-xl-12">
				<!-- BEGIN row -->
				<div class="row">
					<!-- BEGIN col-6 -->
					<?php if(!empty($aboutdayList)){		
						foreach ($aboutdayList as $aboutkey => $aboutVal) {?>
						<div class="col-sm-3">
							<!-- BEGIN card -->
							<?php $dca = strrev(base64_encode($aboutVal['aboutday_id'])); ?>
							<a href="<?php echo base_url().'aboutTheDay?aboutday_id='.$dca.'&heading='.(!empty($aboutVal['aboutday_heading'])?strtolower(json_decode($aboutVal['aboutday_heading'])):null); ?>">
								<div class="card mb-3 overflow-hidden fs-13px border-0 " style="min-height: 202px;">
									<!-- BEGIN card-body -->
									<div class="card-body position-relative">
										<h5 class="text-black-transparent-8 mb-3 fs-16px"></h5>
										<img src="<?php echo str_replace('web','admin',base_url()); ?>/uploads/featured_images/<?php echo $aboutVal['aboutday_featured_image'].'.png' ?>" width="100%" height="110px">
										<div><a href="<?php echo base_url().'aboutTheDay?aboutday_id='.$dca.'&heading='.(!empty($aboutVal['aboutday_heading'])?strtolower(json_decode($aboutVal['aboutday_heading'])):null); ?>" class="text-black d-flex align-items-center text-decoration-none">&nbsp;&nbsp;&nbsp;<span style="color:red;font-weight:bold"><?php echo  date('M d,  Y',strtotime($aboutVal['aboutday_date'])); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="color:black;font-weight:bold"><?php echo (!empty($aboutVal['aboutday_read_time'])?$aboutVal['aboutday_read_time']:null) ?> min read</span></a></div>
									</div>
									<!-- BEGIN card-body -->
								</div>
							</a>
							<!-- END card -->
						</div>
						<?php } } ?>
					</div>
					<!-- END row -->
				</div>
				<!-- END col-6 -->
			</div>
			<!-- END row -->
			<!-- About the Day  - End -->




			<?php if(!empty($npaList)){ ?>
			<!-- About the Day  - Start -->
			<h1 class="page-header mb-3">
				News Paper Analysis <small>Latest Posts</small>
			</h1> 		
			<?php } ?>


			<!-- BEGIN row -->
			<div class="row">
				<!-- BEGIN col-6 -->
				<div class="col-xl-12">
					<!-- BEGIN row -->
					<div class="row">
						<!-- BEGIN col-6 -->
						<?php 
						if(!empty($npaList)){
							foreach ($npaList as $npaKey => $npaVal) {?>
							<div class="col-sm-3">
								<!-- BEGIN card -->
								<?php $npa = strrev(base64_encode($npaVal['npa_id'])); ?>
								<a href="<?php echo base_url().'newsPaperAnalysis?npa_id='.$npa.'&heading='.(!empty($npaVal['npa_heading'])?strtolower(json_decode($npaVal['npa_heading'])):null); ?>">
									<div class="card mb-3 overflow-hidden fs-13px border-0 " style="min-height: 202px;">
										<!-- BEGIN card-body -->
										<div class="card-body position-relative">
											<h5 class="text-black-transparent-8 mb-3 fs-16px"></h5>
											<img src="<?php echo str_replace('web','admin',base_url()); ?>/uploads/featured_images/<?php echo $npaVal['npa_featured_image'].'.png' ?>" width="100%" height="110px">
											<div><a href="<?php echo base_url().'newsPaperAnalysis?npa_id='.$npa.'&heading='.(!empty($npaVal['npa_heading'])?strtolower(json_decode($npaVal['npa_heading'])):null); ?>" class="text-black d-flex align-items-center text-decoration-none">&nbsp;&nbsp;&nbsp;<span style="color:red;font-weight:bold"><?php echo  date('M d,  Y',strtotime($npaVal['npa_date'])); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="color:black;font-weight:bold"><?php echo (!empty($npaVal['npa_read_time'])?$npaVal['npa_read_time']:null) ?> min read</span></a></div>
										</div>
										<!-- BEGIN card-body -->
									</div>
								</a>
								<!-- END card -->
							</div>
							<?php } } ?>
						</div>
						<!-- END row -->
					</div>
					<!-- END col-6 -->
				</div>
				<!-- END row -->
				<!-- About the Day  - End -->






			</div>
		<!-- END #content
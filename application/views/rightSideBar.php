<!-- BEGIN col-3 -->
<div class="col-xl-3">
	<!-- BEGIN #sidebar-bootstrap -->
	<nav id="sidebar-bootstrap" class="navbar navbar-sticky d-none d-xl-block">
		<nav class="nav">
			<a class="nav-link" href="<?php echo base_url(); ?>rightsidebarPages?component=governPolSchemes" target='_blank'>Govern policies and Schemes</a>
			<a class="nav-link" href="<?php echo base_url(); ?>rightsidebarPages?component=staticGk" target='_blank'>Static GK</a>

			<div class="dropdown">
				<a class="nav-link dropdown-toggle" type="button" data-toggle="dropdown">History</a>
				<ul class="dropdown-menu text-left">
					<li><a class="nav-link" href="<?php echo base_url(); ?>rightsidebarPages?component=historyAncientIndia" target='_blank'>History - Ancient India</a></li>
					<li><a class="nav-link" href="<?php echo base_url(); ?>rightsidebarPages?component=historyMedievalIndia" target='_blank'>History - Medieval India</a></li>
					<li><a class="nav-link" href="<?php echo base_url(); ?>rightsidebarPages?component=historyModernIndia" target='_blank'>History - Modern India</a></li>
				</ul>
			</div>

			<a class="nav-link" href="<?php echo base_url(); ?>rightsidebarPages?component=indianArtCulture" target='_blank'>Indian Art and Culture</a>

			<div class="dropdown">
				<a class="nav-link dropdown-toggle" type="button" data-toggle="dropdown">Geography</a>
				<ul class="dropdown-menu text-left">
					<li><a class="nav-link" href="<?php echo base_url(); ?>rightsidebarPages?component=geoWorld" target='_blank'>World Geography</a></li>
					<li><a class="nav-link" href="<?php echo base_url(); ?>rightsidebarPages?component=geoIndian" target='_blank'>Indian Geography</a></li>
				</ul>
			</div>

			<a class="nav-link" href="<?php echo base_url(); ?>rightsidebarPages?component=enbio" target='_blank'>Environment and Bio Diversity</a>
			<a class="nav-link" href="<?php echo base_url(); ?>rightsidebarPages?component=scienceTech" target='_blank'>Science and Technology</a>
			<a class="nav-link" href="<?php echo base_url(); ?>rightsidebarPages?component=indianPol" target='_blank'>Indian Polity</a>
			<a class="nav-link" href="<?php echo base_url(); ?>rightsidebarPages?component=govern" target='_blank'>Governance</a>
			<a class="nav-link" href="<?php echo base_url(); ?>rightsidebarPages?component=indianEco" target='_blank'>Indian Economy</a>
			<a class="nav-link" href="<?php echo base_url(); ?>rightsidebarPages?component=ethics" target='_blank'>Ethics</a>

			<div class="dropdown">
				<a class="nav-link dropdown-toggle" type="button" data-toggle="dropdown">Optional Paper</a>
				<ul class="dropdown-menu text-left">
					<li><a class="nav-link" href="<?php echo base_url(); ?>rightsidebarPages?component=optionalPaperSociology" target='_blank'>Optional Paper - Sociology</a></li>
					<li><a class="nav-link" href="<?php echo base_url(); ?>rightsidebarPages?component=optionalPaperPublicAdmin" target='_blank'>Optional Paper - Public Admin</a></li>
				</ul>
			</div>
			<?php if(basename($_SERVER['PHP_SELF'])!=('CalendarDateDataNotFound')){ ?>
			<div id="datepickerRightSideBar" style="padding-left:16px"></div>
			<?php } ?>

		</nav>
	</nav>
	<!-- END #sidebar-bootstrap -->
</div>
<!-- END col-3 -->
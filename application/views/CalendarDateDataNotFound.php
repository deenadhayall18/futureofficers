<!-- BEGIN #content -->
<div id="content" class="app-content">
	<!-- BEGIN container -->
	<div class="container">
		<!-- BEGIN row -->
		<div class="row justify-content-center">
			<!-- BEGIN col-10 -->
			<div class="col-xl-12">
				<!-- BEGIN row -->
				<div class="row">
					<!-- BEGIN col-9 -->
					<div class="col-xl-9">


						<ul class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url() ?>homepage">HomePage</a></li>
							
						</ul>

						<hr class="mb-4" />

						<!-- BEGIN #abbreviations -->
						<div id="abbreviations" class="mb-5">


							
							<br>
							<div class="text-center">
								<h4 class="text-center">Selected Date does not have any post ! Please visit to Homepage to explore more ! </h4>
							</div>
							
							
						</div>
						<!-- END #abbreviations -->
					</div>
					<!-- END col-9-->
					<?php $this->load->view('rightSideBar'); ?>
				</div>
				<!-- END row -->
			</div>
			<!-- END col-10 -->
		</div>
		<!-- END row -->
	</div>
	<!-- END container -->
</div>
			<!-- END #content -->
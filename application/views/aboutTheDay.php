<!-- BEGIN #content -->
<div id="content" class="app-content">
	<!-- BEGIN container -->
	<div class="container">
		<!-- BEGIN row -->
		<div class="row justify-content-center">
			<!-- BEGIN col-10 -->
			<div class="col-xl-12">
				<!-- BEGIN row -->
				<div class="row">
					<!-- BEGIN col-9 -->
					<div class="col-xl-9">
						<ul class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url() ?>homepage">About the Day</a></li>
							<li class="breadcrumb-item active"><?php echo json_decode($aboutTheDayData[0]['aboutday_category']); ?></li>
							<li class="breadcrumb-item active">Posted On : <?php echo ($aboutTheDayData[0]['aboutday_date']); ?></li>
						</ul>
						<!-- <hr class="mb-4" />
						<h1 class="page-header mb-3">
							<?php echo ucwords(str_replace("-"," ",json_decode($aboutTheDayData[0]['aboutday_heading']))); ?>
						</h1> -->
						<!-- BEGIN #abbreviations -->
						<div id="abbreviations" class="mb-5">
							<p style="width: 100% !important;height: auto"><?php		
							echo html_entity_decode(json_decode($aboutTheDayData[0]['aboutday_content']));
							?>
						</p>
					</div>
					<!-- END #abbreviations -->
				</div>
				<!-- END col-9-->
				<?php $this->load->view('rightSideBar'); ?>
			</div>
			<!-- END row -->
		</div>
		<!-- END col-10 -->
	</div>
	<!-- END row -->
</div>
<!-- END container -->
</div>
			<!-- END #content -->
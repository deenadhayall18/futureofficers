<!-- BEGIN #content -->
<div id="content" class="app-content">
	<h1 class="page-header mb-3">
		Daily Current Affairs List
	</h1> 		

	<!-- BEGIN row -->
	<div class="row">
		<!-- BEGIN col-6 -->
		<div class="col-xl-12">
			<!-- BEGIN row -->
			<div class="row">
				<!-- BEGIN col-6 -->
				<?php if(!empty($dcaMonthlyList)){
					foreach ($dcaMonthlyList as $dcaKey => $dcaVal) {?>
					<div class="col-sm-3">
						<!-- BEGIN card -->
						<?php $dca = strrev(base64_encode($dcaVal['dca_id'])); ?>
						<a style="text-decoration:none !important;" href="<?php echo base_url().'dailyCurrentAffairs?dca_id='.$dca.'&heading='.(!empty($dcaVal['dca_heading'])?strtolower(json_decode($dcaVal['dca_heading'])):null); ?>">
							<div class="card mb-3 overflow-hidden fs-13px border-0 " style="min-height: 202px;">
								<!-- BEGIN card-body -->
								<div class="card-body position-relative">
									<?php if($dcaVal['dca_date']==date('Y-m-d')){?>
									<div class="d-flex mb-3">
										<div class=" d-flex align-items-center">
											<i class="fa fa-circle fs-2px fa-fw text-danger mr-2 blink_me"></i> <span style="color:red !important;font-weight:bold !important;text-decoration:none !important;cursor:pointer !important;">Posted Today</span>
										</div>
									</div>
									<?php } ?>
									<h5 class="text-black-transparent-8 mb-3 fs-16px">
										<!-- <?php echo ucwords($dcaVal['dca_category']); ?> -->
									</h5>
									<img src="<?php echo str_replace('web', 'admin',base_url()); ?>/uploads/featured_images/<?php echo $dcaVal['dca_featured_image'].'.png' ?>" width="100%" height="110px">
									<div>
										<a href="<?php echo base_url().'dailyCurrentAffairs?dca_id='.$dca.'&heading='.(!empty($dcaVal['dca_heading'])?strtolower(json_decode($dcaVal['dca_heading'])):null); ?>" class="text-black d-flex align-items-center text-decoration-none">&nbsp;&nbsp;&nbsp;
											<span style="color:#834705;font-weight:bold"><?php echo  date('M d,  Y',strtotime($dcaVal['dca_date'])); ?></span>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
											<span style="color:black;font-weight:bold">
												<?php echo (!empty($dcaVal['dca_read_time'])?$dcaVal['dca_read_time']:null) ?> min read
											</span>
										</a>
									</div>
								</div>
								<!-- BEGIN card-body -->
							</div>
						</a>
						<!-- END card -->
					</div>
					<?php } } ?>
				</div>
				<!-- END row -->
			</div>
			<!-- END col-6 -->
		</div>
		<!-- END row -->







	</div>
		<!-- END #content -->
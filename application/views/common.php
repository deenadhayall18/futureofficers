<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-167955626-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-167955626-1');
	</script>

	<meta charset="utf-8" />
	<title>Future Officers</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN core-css ================== -->
	<link href="<?php echo base_url(); ?>packages/assets/css/app.min.css" rel="stylesheet" />
	<!-- ================== END core-css ================== -->
	
	<!-- ================== BEGIN page-css ================== -->
	<link href="<?php echo base_url(); ?>packages/assets/plugins/%40fullcalendar/core/main.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>packages/assets/plugins/%40fullcalendar/bootstrap/main.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>packages/assets/plugins/%40fullcalendar/timegrid/main.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>packages/assets/plugins/%40fullcalendar/daygrid/main.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>packages/assets/plugins/%40fullcalendar/list/main.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>packages/assets/plugins/jvectormap/jquery-jvectormap.css" rel="stylesheet" />
	<!-- ================== END page-css ================== -->

	<style type="text/css">
	.responsiveLogo { width: 100%; max-width: 212px; height: auto; }
	.SocialMediaresponsiveLogo { width: 100%; max-width: 39px; height: auto; }
	.blink_me {animation: blinker .5s linear infinite; } @keyframes blinker {50% {opacity: 0; } }
</style>
<script src="<?php echo base_url(); ?>packages/assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>packages/assets/css/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>packages/assets/css/smoothness-jquery-ui.css">
<script src="<?php echo base_url(); ?>packages/assets/js/jquery-ui.min.js"></script>


<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script> -->

<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5ed3a2d77cfa4a0012b4774a&product=inline-share-buttons&cms=website' async='async'></script>

<script>
	$(function() {
		$("#datepickerRightSideBar").datepicker();
	});
</script>




</head>
<body>
	<!-- BEGIN #app -->
	<div id="app" class="app">
		<!-- BEGIN #header -->
		<!-- BEGIN #header -->
		<header id="header" class="app-header">
			<!-- BEGIN mobile-toggler -->
			<div class="mobile-toggler">
				<button type="button" class="menu-toggler" data-toggle="sidebar-mobile">
					<span class="bar"></span>
					<span class="bar"></span><span class="bar"></span>
				</button>
			</div>
			<!-- END mobile-toggler -->
			
			<!-- BEGIN brand -->
			<div class="brand">
				<div class="desktop-toggler">
					<button type="button" class="menu-toggler" data-toggle="sidebar-minify">
						<span class="bar"></span><span class="bar"></span>
						<span class="bar"></span>
					</button>
				</div>
				
				<a href="#" class="brand-logo">
					<!-- <img src="<?php echo base_url(); ?>/packages/assets/img/logo.png" height="190px" width="200px" /> -->
					<!-- <h5 class="text-black mb-1 text-center">Future Officers</h5> -->
				</a>
			</div>
			<!-- END brand -->
			
			<!-- BEGIN menu -->
			<?php $this->load->view('header'); ?>
			<!-- END menu -->

		</header>
		<!-- END #header -->
		
		<!-- BEGIN #sidebar -->
		<?php $this->load->view('sidebar');  ?>
		<!-- END #sidebar -->




		<?php $this->load->view($url); ?>

		<!-- BEGIN btn-scroll-top -->
		<a href="#" data-click="scroll-top" class="btn-scroll-top fade"><i class="fa fa-arrow-up"></i></a>
		<!-- END btn-scroll-top -->
	</div>
	<!-- END #app -->

	<!-- ================== BEGIN core-js ================== -->
	<script src="<?php echo base_url(); ?>packages/assets/js/app.min.js"></script>
	<!-- ================== END core-js ================== -->
	<!-- ================== BEGIN page-js ================== -->
	<script src="<?php echo base_url(); ?>packages/assets/js/demo/dashboard.demo.js"></script>
	<script src="<?php echo base_url();?>packages/assets/plugins/ckeditor/ckeditor.js"></script>

	<!-- ================== END page-js ================== -->
</body>
<script type="text/javascript">
	$("#datepickerRightSideBar").datepicker({ dateFormat: "yy-mm-dd" });	
	$(function(){
		$('#datepickerRightSideBar').change(function() {
			var SelDate = $('#datepickerRightSideBar').val();
			var URL = window.location.pathname; 
			var page = URL.substring(URL.lastIndexOf('/') + 1); 
			var baseurl = "<?php echo base_url(); ?>";
			var RedirectURL = baseurl+'MainController/'+page+'?selectedDate='+SelDate;
			window.location.href = RedirectURL;
		});
	});
</script>

</html>







﻿<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">

	<title>Future Officers</title>
	<!-- Favicon-->
	<!-- <link rel="icon" href="<?php base_url(); ?>packages/admin/favicon.ico" type="image/x-icon"> -->
	<link href="<?php base_url(); ?>packages/admin/css/google_text_css.css" rel="stylesheet" type="text/css">
	<link href="<?php base_url(); ?>packages/admin/css/google_text_icons.css" rel="stylesheet" type="text/css">
	<!-- Bootstrap Core Css -->
	<link href="<?php base_url(); ?>packages/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
	<!-- Waves Effect Css -->
	<link href="<?php base_url(); ?>packages/admin/plugins/node-waves/waves.css" rel="stylesheet" />
	<!-- Animation Css -->
	<link href="<?php base_url(); ?>packages/admin/plugins/animate-css/animate.css" rel="stylesheet" />
	<!-- JQuery DataTable Css -->
	<link href="<?php base_url(); ?>packages/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

	<link href="<?php base_url(); ?>packages/admin/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">

	<link href="<?php base_url(); ?>packages/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />



	<!-- Custom Css -->
	<link href="<?php base_url(); ?>packages/admin/css/style.css" rel="stylesheet">
	<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
	<link href="<?php base_url(); ?>packages/admin/css/themes/all-themes.css" rel="stylesheet" />
	<script src="<?php echo base_url();?>packages/admin/plugins/ckeditor/ckeditor.js"></script>
	<style type="text/css">

	.errStyle{color:green;font-weight:bold;font-size:16px;}
	.errStyle1{color:red;font-weight:bold;font-size:16px;}
</style>


</head>

<body class="theme-red">
	<!-- Page Loader -->
	<div class="page-loader-wrapper">
		<div class="loader">
			<div class="preloader">
				<div class="spinner-layer pl-red">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div>
			<p>Please wait...</p>
		</div>
	</div>
	
	<div class="overlay"></div>
	
	<div class="search-bar">
		<div class="search-icon">
			<i class="material-icons" style="color:darkblue">search</i>
		</div>
		<input type="text" placeholder="START TYPING...">
		<div class="close-search">
			<i class="material-icons" style="color:darkblue">close</i>
		</div>
	</div>
	<nav class="navbar">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
				<a href="javascript:void(0);" class="bars"></a>
				<a class="navbar-brand" href="dashboard">Future Officers</a>
			</div>
			<div class="collapse navbar-collapse" id="navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a  class="btn btn-primary" href="AdminCon/logout" >
							Log Out
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</nav>

	<section>
		<aside id="leftsidebar" class="sidebar">
			<div class="menu">
				<ul class="list">
					<li class="<?php echo(($url=='dashboard')?'active':"");?>">
						<a href="dashboard">
							<i class="material-icons" style="color:darkblue">home</i>
							<span>Home</span>
						</a>
					</li>
					<li  class="<?php echo(($url=='usersList')?'active':"");?>">
						<a href="usersList">
							<i class="material-icons" style="color:red">person_add</i>
							<span>Users List</span>
						</a>
					</li>
					<li class="<?php echo(($url=='addFeaturedImage')?'active':"");?>  <?php echo(($url=='featuredImageList')?'active':"");?>">
						<a href="javascript:void(0);" class="menu-toggle">
							<i class="material-icons" style="color:green">image</i>
							<span>Featured Image</span>
						</a>
						<ul class="ml-menu">
							<li class="<?php echo(($url=='addFeaturedImage')?'active':"");?>">
								<a href="addFeaturedImage">Add Featured Image</a>
							</li>
							<li class="<?php echo(($url=='featuredImageList')?'active':"");?>">
								<a href="featuredImageList">Featured Image List</a>
							</li>
						</ul>
					</li>

					<li class="<?php echo(($url=='addAttachment')?'active':"");?>  <?php echo(($url=='attachmentList')?'active':"");?>">
						<a href="javascript:void(0);" class="menu-toggle">
							<i class="material-icons" style="color:brown">attach_file</i>
							<span>Attachment</span>
						</a>
						<ul class="ml-menu">
							<li class="<?php echo(($url=='addAttachment')?'active':"");?>">
								<a href="addAttachment">Add Attachment</a>
							</li>
							<li class="<?php echo(($url=='attachmentList')?'active':"");?>">
								<a href="attachmentList">Attachment List</a>
							</li>
						</ul>
					</li>



					<li class="<?php echo(($url=='subscribersList')?'active':"");?>">
						<a href="subscribersList"> 
							<i class="material-icons" style="color:magenta">playlist_add_check</i>
							<span>Subscribers List</span>
						</a>
					</li>

					<li class="<?php echo(($url=='addDailyCurrentAffairs')?'active':"");?>  <?php echo(($url=='dailyCurrentAffairsCategoryList')?'active':"");?>">
						<a href="javascript:void(0);" class="menu-toggle">
							<i class="material-icons" style="color:orange">note_add</i>
							<span>Daily Current Affairs</span>
						</a>
						<ul class="ml-menu">
							<li class="<?php echo(($url=='addDailyCurrentAffairs')?'active':"");?>">
								<a href="addDailyCurrentAffairs">Add Daily Current Affairs</a>
							</li>
							<li class="<?php echo(($url=='dailyCurrentAffairsCategoryList')?'active':"");?>">
								<a href="dailyCurrentAffairsCategoryList">Daily Current Affairs List</a>
							</li>
						</ul>
					</li>

					<li class="<?php echo(($url=='addAboutTheDay')?'active':"");?>">
						<a href="addAboutTheDay"> 
							<i class="material-icons" style="color:aqua">library_add</i>
							<span>Add About the Day</span>
						</a>
					</li>
					<li class="<?php echo(($url=='addPreviousYearPrelimsQues')?'active':"");?>">
						<a href="addPreviousYearPrelimsQues"> 
							<i class="material-icons" style="color:grey">add_box</i>
							<span>Add Previous Year Prelims Ques</span>
						</a>
					</li>
					<li class="<?php echo(($url=='addDailyMainQues')?'active':"");?>">
						<a href="addDailyMainQues"> 
							<i class="material-icons" style="color:darkblue">playlist_add</i>
							<span>Add Daily Main Ques</span>
						</a>
					</li>
					<li class="<?php echo(($url=='addGovnSchemes')?'active':"");?>">
						<a href="addGovnSchemes"> 
							<i class="material-icons" style="color:lightgreen">playlist_add</i>
							<span>Add Government Schemes</span>
						</a>
					</li>
					<li class="<?php echo(($url=='addHinduAnalysis')?'active':"");?>">
						<a href="addHinduAnalysis"> 
							<i class="material-icons" style="color:darkred">playlist_add</i>
							<span>Add Hindu Analysis</span>
						</a>
					</li>
					<li class="<?php echo(($url=='addStaticGK')?'active':"");?>">
						<a href="addStaticGK"> 
							<i class="material-icons" style="color:indigo">playlist_add</i>
							<span>Add Static GK</span>
						</a>
					</li>

				</ul>
			</div>

			<!-- <div class="legal">
				<div class="copyright">
					<a href="javascript:void(0);">Future Officers	</a>
				</div>
				<div class="version">
					<b>Version: </b> 1.0.0
				</div>
			</div> -->

		</aside>

		

	</section>

	<?php  

	$this->load->view($url);

	?>
	<!-- Jquery Core Js -->
	<script src="<?php base_url(); ?>packages/admin/plugins/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core Js -->
	<script src="<?php base_url(); ?>packages/admin/plugins/bootstrap/js/bootstrap.js"></script>

	<!-- Select Plugin Js -->
	<script src="<?php base_url(); ?>packages/admin/plugins/bootstrap-select/js/bootstrap-select.js"></script>

	<!-- Slimscroll Plugin Js -->
	<!-- <script src="<?php base_url(); ?>packages/admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script> -->

	<!-- Waves Effect Plugin Js -->
	<script src="<?php base_url(); ?>packages/admin/plugins/node-waves/waves.js"></script>

	<!-- Jquery DataTable Plugin Js -->
	<script src="<?php base_url(); ?>packages/admin/plugins/jquery-datatable/jquery.dataTables.js"></script>
	<script src="<?php base_url(); ?>packages/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
	<script src="<?php base_url(); ?>packages/admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
	<!-- <script src="<?php base_url(); ?>packages/admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script> -->
	<script src="<?php base_url(); ?>packages/admin/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
	<script src="<?php base_url(); ?>packages/admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
	<script src="<?php base_url(); ?>packages/admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
	<script src="<?php base_url(); ?>packages/admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
	<script src="<?php base_url(); ?>packages/admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
	<script src="<?php base_url(); ?>packages/admin/plugins/bootstrap-material-datetimepicker/js/	bootstrap-material-datetimepicker.js"></script>
	<!-- Custom Js -->
	<script src="<?php base_url(); ?>packages/admin/js/admin.js"></script>
	<script src="<?php base_url(); ?>packages/admin/js/pages/tables/jquery-datatable.js"></script>

	<!--Validation -->
	<script src="<?php base_url();?>packages/admin/js/jquery.validate.min.js"> </script>
	<script src="<?php base_url();?>packages/admin/js/validate.js"></script>
	<script src="<?php base_url();?>packages/admin/js/jquery-ui.min.js"></script>
	<script src="<?php base_url();?>packages/admin/js/jqval-additional-methods.min.js" type="text/javascript"></script>
	<script src="<?php base_url();?>packages/admin/js/validate.js" type="text/javascript"></script>
	<script src="<?php base_url();?>packages/admin/js/functions.js" type="text/javascript"></script>


	<script type="text/javascript">

		$(document).ready(function(){
			$('#btn_allow_attachment_upload').click(function(){
				$('#showExistFImage').hide();
				$('#uploadFImage').show();

			});
		});



		// DCA - Validation
		$(function(){
			$('#frmAddDCA').validate({
				rules:{
					dca_date:{required:true},
					dca_category:{required:true,minlength:3,maxlength:15},
					dca_content:{required:true,minlength:10},
					dca_featured_image:{required:true},
					dca_tags:{required:true},
					dca_attachments:{required:true},
				},
				messages:{
					dca_date:{required:'Enter Date'},
					dca_category:{required:'Enter Category',minlength:'Minimum of 3 characters required',maxlength:'Maximum of 15 characters allowed'},
					dca_content:{required:'Enter Content',minlength:'Minimum of 10 characters required'},
					dca_featured_image:{required:'Select Featured Image'},
					dca_tags:{required:'Enter Tags'},
					dca_attachments:{required:'Select Attachment'},
				}
			})
		});

		$(function(){
			$('#frmAddFeaturedImage').validate({
				rules:{
					f_i_name:{required:true},
					f_image:{required:true,maxfilesize:5120,extension:"jpeg|jpg|png"}
				},
				messages:{
					f_i_name:{required:'Enter Name'},
					f_image:{required:'Upload Featured Image',maxfilesize:'Maximum of 5 MB only allowed',extension:"jpeg | jpg | png only allowed"},
				}
			})
		});

		$(function(){
			$('#frmAddAttachment').validate({
				rules:{
					a_name:{required:true},
					a_file:{required:true,maxfilesize:10240,extension:"pdf"}
				},
				messages:{
					a_name:{required:'Enter Name'},
					a_file:{required:'Upload Attachment',maxfilesize:'Maximum of 10 MB only allowed',extension:"PDF only allowed"},
				}
			})
		});


	// // DCA - button
	// $(function(){
	// 	$("#btn_get_dca_content_by_date").click(function(){
	// 		var date_val =  $('#get_dca_content_by_date').val();
	// 		var url = '<?php echo base_url().'dailyCurrentAffairsCategoryList?date=';?>'
	// 		window.location.replace(url);
	// 	});
	// });
	




// log
$(function(){
	$('#FrmLogDetails').validate({
		rules:{
			date:{required:true},
			project:{required:true,minlength:5},
			dailylog:{required:true,minlength:5},
		},
		messages:{
			date:{required:"Enter the date"},
			project:{required:"Enter Project Name"},
			dailylog:{required:"Enter Daily Log"},
		}
	})
});

setTimeout(
	function(){
		$('#err_hide').fadeOut("slow");
	},1500);

$(document).ready(function(){
	$('#md_checkbox_40').change(function(){
		$('#delButtonPopUp').toggle();
	});
});








</script>


</body>

</html>
		<section class="content">
			<div class="container-fluid">
				<?php if(!empty($this->session->flashdata('Succ'))){ ?>
				<div class="text-center" style="padding-bottom:10px" id="err_hide">
					<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
				</div>  
				<?php } ?>
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="header">
								<div class="align-right"><a type="button" class="btn btn-warning btn-sm" href="<?php echo base_url().'admin/addAboutTheDay' ?>">+ Add About the Day List</a></div>
								<h2>
									<b>About the Day List</b>
								</h2>
							</div>
							<div class="body">
								<form method="post" name="frmDCAList" id="frmDCAList" enctype="multipart/form-data">
									<label>Select Date</label>
									<div class="form-group">
										<div class="form-line">
											<input type="date"  name="aboutday_date"  id="aboutday_date" class="form-control" placeholder="Enter date" value="<?php echo date("Y-m-d");?>" required>

										</div>
										<input type="submit" name="submit" class="btn btn-success m-t-15 waves-effect" value="Submit">&nbsp;
										<a class="btn btn-info m-t-15" href="<?php $_SERVER['PHP_SELF']; ?>">Reset</a>
									</div>



									<?php if(!empty($aboutday_details)){?>
									<div class="table-responsive">
										<table class="table table-bordered table-hover table-striped">
											<thead style="background-color:#000;color: #fff">
												<tr>
													<th width="5%">No</th>
													<th width="10%">Date</th>
													<th width="30%">Category</th>
													<th width="20%">Action</th>
												</tr>
											</thead>
											<tbody>
												<?php $i=0; foreach ($aboutday_details as $key => $value) {$i++; 		
													?>
													<tr>
														<td><?php echo $i; ?></td>
														<td><?php echo $value['aboutday_date'];  ?></td>
														<td><?php echo json_decode($value['aboutday_category']);   ?></td>
														<td>
															<input type="hidden" name="aboutday_id" id="aboutday_id" value="<?php echo $value['aboutday_id'];?>">
															
															<a class="btn btn-xs btn-warning" title="Edit" href="<?php echo base_url().'admin/editAboutTheDay?aboutday_id='.($value["aboutday_id"]); ?>" target='_blank'> 
																<i class="material-icons" >mode_edit</i>
															</a>
															&nbsp;
															&nbsp;
															
														</td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
										<?php } ?>

									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>


			<script>


			</script>

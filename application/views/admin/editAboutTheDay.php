<section class="content">
	<div class="container-fluid">
		<div class="text-center" style="padding-bottom:10px" id="err_hide">
			<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
		</div>  
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<div class="align-right"><a type="button" class="btn btn-warning btn-sm" href="<?php echo base_url().'admin/aboutTheDayCategoryList' ?>">About the Day List</a></div>
						<h2>
							<b>Edit About the Day</b>
						</h2>
					</div>
					<div class="body">
						<form method="post" name="frmAboutDay" id="frmAboutDay">
							<label for="email_address">Date (Cannot be edited)</label>
							<div class="form-group">
								<div class="form-line">
									<?php $dateVal =  (!empty($aboutDayData[0]['aboutday_date'])?$aboutDayData[0]['aboutday_date']:date("Y-m-d")); ?>
									<input type="date" readonly class="form-control" placeholder="Enter date" value="<?php echo $dateVal; ?>" required>
								</div>
							</div>
							<label>Select Language</label>
							<div class="form-group">
								<div class="form-line">
									<select class="form-control show-tick" name="aboutday_lang" id="aboutday_lang" required="true" >
										<option value="">-- Please select --</option>
										<option value="1" <?php echo (!empty($aboutDayData[0]['aboutday_lang']==1)?'selected':null) ?>>English</option>
										<option value="2" <?php echo (!empty($aboutDayData[0]['aboutday_lang']==2)?'selected':null) ?>>Hindi</option>
									</select>
								</div>
							</div>
							<label>Category</label>
							<div class="form-group">
								<div class="form-line">
									<input type="text" name="aboutday_category" id="aboutday_category" class="form-control" placeholder="Enter Category" value="<?php echo (!empty($aboutDayData[0]['aboutday_category'])?json_decode($aboutDayData[0]['aboutday_category']):null); ?>" >
								</div>
							</div>

							<label>Select Featured Image</label>
							<div class="form-group">
								<div class="form-line">
									<select class="form-control show-tick" name="aboutday_featured_image" id="aboutday_featured_image" >
										<option value="">-- Please select --</option>
										<?php if(!empty($FimagesList)){
											foreach ($FimagesList as $key => $value) { ?>
											<option value="<?php echo $value['f_i_id']; ?>" <?php echo (!empty($aboutDayData[0]['aboutday_featured_image']==$value['f_i_id'])?'selected':null) ?>><?php echo ucwords($value['f_i_name']); ?></option>
											<?php } } ?>
										</select>
									</div>
								</div>

								<label>Tags</label>
								<div class="form-group">
									<div class="form-line">
										<textarea class="form-control" name="aboutday_tags" id="aboutday_tags" placeholder="Enter Tags"><?php echo (!empty($aboutDayData[0]['aboutday_tags'])?json_decode($aboutDayData[0]['aboutday_tags']):null); ?></textarea>
									</div>
								</div>
								<label>Heading</label>
								<div class="form-group">
									<div class="form-line">
										<input type="textbox" name="aboutday_heading" id="aboutday_heading" value="<?php echo (!empty($aboutDayData[0]['aboutday_heading'])? str_replace("-"," ",json_decode($aboutDayData[0]['aboutday_heading'])):null); ?>" class="form-control" placeholder="Enter heading" required>
									</div>
								</div>

								<label>About the Day Details</label>
								<div class="form-group">
									<div class="form-line">
										<?php		
										echo $this->ckeditor->editor("aboutday_content",json_decode($aboutDayData[0]['aboutday_content']));
										?>
									</div>
								</div>

								<label>Read Time</label>
								<div class="form-group">
									<div class="form-line">
										<input type="text"  minlength="1" maxlength="3" name="aboutday_read_time" id="aboutday_read_time" class="form-control" placeholder="Enter Read Time (1 to 120)" value="<?php echo (!empty($aboutDayData[0]['aboutday_read_time'])?$aboutDayData[0]['aboutday_read_time']:null); ?>" required>
									</div>
								</div>

								<div class="form-group">
									<div class="form-line">
										<?php $mode = (!empty($aboutDayData[0]['aboutday_trigger_email']=='yes')?'checked':null); ?>
										<input type="checkbox" id="md_checkbox_21" name="aboutday_trigger_email" class="filled-in chk-col-pink" <?php echo $mode; ?> />
										<label for="md_checkbox_21">Share to Subscribed Users Email</label>
									</div>
								</div>

								<input type="submit" name="submit" class="btn btn-success m-t-15 waves-effect" value="Submit">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
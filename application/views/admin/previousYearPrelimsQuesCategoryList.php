<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>Previous Year Prelims Question Categories List</h2>
		</div>
		<br>
		<div class="row clearfix">
			<div class="body">
				<div class="button-demo">
					<button type="button" class="btn bg-brown waves-effect">Category 1</button><br>
					<button type="button" class="btn bg-blue waves-effect">Category 2</button><br>
					<button type="button" class="btn bg-pink waves-effect">Category 3</button><br>
				</div>
			</div>
		</div>
	</div>
</section>
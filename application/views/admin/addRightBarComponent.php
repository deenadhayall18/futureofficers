<section class="content">
	<div class="container-fluid">
		<div class="text-center" style="padding-bottom:10px" id="err_hide">
			<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
		</div>  
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<div class="align-right"><a type="button" class="btn btn-warning btn-sm" href="<?php echo base_url().'admin/rightBarComponentList' ?>">Right Sidebar Component List</a></div> 
						<h2>
							<b>Add Right Sidebar Component</b> &nbsp;
							<?php if(!empty($this->session->flashdata('previewContent'))){
								$prev = $this->session->flashdata('previewContent'); 
								?>
								<a target='_blank' href="<?php echo base_url().'rightsidebarInnerPages?rightsidebar_id='.strrev(base64_encode($prev['id'])).'&heading='.strtolower(($prev['heading'])); ?>" class="btn btn-xs btn-success">Preview</a>
								&nbsp; 
								<a  target="_blank" class="btn btn-xs btn-danger" href="<?php echo base_url().'admin/editRightBarComponent?rightsidebar_id='.($prev['id']);?>">Edit</a>
								<!-- &nbsp; 
									<a class="btn btn-xs btn-danger">Delete</a> -->
									&nbsp; 
									<a href="<?php $_SERVER['PHP_SELF']; ?>" class="btn btn-xs btn-default">Add New Post</a>
									&nbsp; 
									<a target ='_blank' href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url().'rightsidebarInnerPages?rightsidebar_id='.strrev(base64_encode($prev['id'])).'&heading='.strtolower(($prev['heading'])); ?>" class="btn btn-xs btn-primary">Facebook</a>
									&nbsp; 
									<a target="_blank" href="https://telegram.me/share/url?url=<?php echo base_url().'rightsidebarInnerPages?rightsidebar_id='.strrev(base64_encode($prev['id'])).'&heading='.strtolower(($prev['heading'])); ?>" class="btn btn-xs btn-primary">Telegram</a>
									<?php } ?>
								</h2>
							</div>
							<div class="body">
								<form method="post" name="frmAddrightsidebar" id="frmAddrightsidebar" enctype="multipart/form-data">
									<label>Date</label>
									<div class="form-group">
										<div class="form-line">
											<input type="date"  name="rightsidebar_date"  id="rightsidebar_date" class="form-control" placeholder="Enter date" value="<?php echo date("Y-m-d");?>" required>

										</div>
									</div>

									<label>Select Language</label>
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" name="rightsidebar_lang" id="rightsidebar_lang" required="true" >
												<option value="">-- Please select --</option>
												<option value="1">English</option>
												<option value="2">Hindi</option>
											</select>
										</div>
									</div>
									<label>Category</label>
									<div class="form-group">
										<div class="form-line">
											<input type="textbox" name="rightsidebar_category" id="rightsidebar_category" class="form-control" placeholder="Enter Category" required>
										</div>
									</div>


									<label>Select Component</label>
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" name="rightsidebar_component" id="rightsidebar_component" >
												<option value="">-- Please select --</option>
												<?php if(!empty($components)){
													foreach ($components as $key => $value) { ?>
													<option value="<?php echo $key; ?>"><?php echo ucwords($value); ?></option>
													<?php } } ?>
												</select>
											</div>
										</div>





										<label>Select Featured Image</label>
										<div class="form-group">
											<div class="form-line">
												<select class="form-control show-tick" name="rightsidebar_featured_image" id="rightsidebar_featured_image" >
													<option value="">-- Please select --</option>
													<?php if(!empty($FimagesList)){
														foreach ($FimagesList as $key => $value) { ?>
														<option value="<?php echo $value['f_i_id']; ?>"><?php echo ucwords($value['f_i_name']); ?></option>
														<?php } } ?>
													</select>
												</div>
											</div>


											<label>Tags</label>
											<div class="form-group">
												<div class="form-line">
													<textarea class="form-control" name="rightsidebar_tags" id="rightsidebar_tags" placeholder="Enter Tags"></textarea>
												</div>
											</div>

											<label>Heading</label>
											<div class="form-group">
												<div class="form-line">
													<input type="textbox" name="rightsidebar_heading" id="rightsidebar_heading" value="<?php echo (!empty($dcaDbData[0]['rightsidebar_heading'])? str_replace("-"," ",json_decode($dcaDbData[0]['rightsidebar_heading'])):null); ?>" class="form-control" placeholder="Enter heading" required>
												</div>
											</div>

											<label>Right Sidebar Component</label>
											<div class="form-group">
												<div class="form-line">
													<?php		
													echo $this->ckeditor->editor("rightsidebar_content");
													?>
													<label>
														<?php echo (!empty(form_error('rightsidebar_content')))?form_error('rightsidebar_content'):""; ?></label>
													</div>
												</div>


												<label>Read Time</label>
												<div class="form-group">
													<div class="form-line">
														<input type="text"  minlength="1" maxlength="3" name="rightsidebar_read_time" id="rightsidebar_read_time" class="form-control" placeholder="Enter Read Time (1 to 120)" required>
													</div>
												</div>


												<label>Select Attachment</label>
												<div class="form-group">
													<div class="form-line">
														<select class="form-control show-tick" name="rightsidebar_attachments" id="rightsidebar_attachments" >
															<option value="">-- Please select --</option>
															<?php if(!empty($attachmentList)){
																foreach ($attachmentList as $key => $value) { ?>
																<option value="<?php echo $value['a_id']; ?>"><?php echo ucwords($value['a_name']); ?></option>
																<?php } } ?>
															</select>
														</div>
													</div>


													<div class="form-group">
														<div class="form-line">
															<input type="checkbox" id="md_checkbox_21" name="rightsidebar_trigger_email" class="filled-in chk-col-pink" />
															<label for="md_checkbox_21">Share to Subscribed Users Email</label>
															<?php echo (!empty(form_error('rightsidebar_attachments')))?form_error('rightsidebar_attachments'):""; ?></label>
														</div>
													</div>
													<input type="submit" name="submit" class="btn btn-success m-t-15 waves-effect" value="Submit">
												</form>
											</div>

										</div>
									</div>
								</div>
							</div>
						</section>
						<script>


						</script>

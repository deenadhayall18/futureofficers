<section class="content">
	<div class="container-fluid">
		<div class="text-center" style="padding-bottom:10px" id="err_hide">
			<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
		</div>  
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<div class="align-right"><a type="button" class="btn btn-warning btn-sm" href="<?php echo base_url().'admin/yojanaList' ?>">Yojana List</a></div>
						<h2>
							<b>Edit Yojana</b>
						</h2>
					</div>
					<div class="body">
						<form method="post" name="frmAddyoj" id="frmAddyoj" enctype="multipart/form-data">
							<label>Date (Cannot be edited)</label>
							<div class="form-group">
								<div class="form-line">
									<?php $dateVal =  (!empty($yojDbData[0]['yoj_date'])?$yojDbData[0]['yoj_date']:date("Y-m-d")); ?>
									<input type="date" readonly class="form-control" placeholder="Enter date" value="<?php echo $dateVal; ?>" required>
									
								</div>
							</div>
							
							<label>Select Language</label>
							<div class="form-group">
								<div class="form-line">
									<select class="form-control show-tick" name="yoj_lang" id="yoj_lang" required="true" >
										<option value="">-- Please select --</option>
										<option value="1" <?php echo (!empty($yojDbData[0]['yoj_lang']==1)?'selected':null) ?>>English</option>
										<option value="2" <?php echo (!empty($yojDbData[0]['yoj_lang']==2)?'selected':null) ?>>Hindi</option>
									</select>
								</div>
							</div>

							<label>Category</label>
							<div class="form-group">
								<div class="form-line">
									<input type="textbox" name="yoj_category" id="yoj_category" class="form-control" placeholder="Enter Category"  value="<?php echo (!empty($yojDbData[0]['yoj_category'])?json_decode($yojDbData[0]['yoj_category']):null); ?>"  required>
								</div>
							</div>

							<label>Select Featured Image</label>
							<div class="form-group">
								<div class="form-line">
									<select class="form-control show-tick" name="yoj_featured_image" id="yoj_featured_image" >
										<option value="">-- Please select --</option>
										
										<?php if(!empty($FimagesList)){
											foreach ($FimagesList as $key => $value) { ?>
											<option value="<?php echo $value['f_i_id']; ?>" <?php echo (!empty($yojDbData[0]['yoj_featured_image']==$value['f_i_id'])?'selected':null) ?>><?php echo ucwords($value['f_i_name']); ?></option>
											<?php } } ?>
										</select>
									</div>
								</div>

								<label>Tags</label>
								<div class="form-group">
									<div class="form-line">
										<textarea class="form-control" name="yoj_tags" id="yoj_tags" placeholder="Enter Tags" required><?php echo (!empty($yojDbData[0]['yoj_category'])?json_decode($yojDbData[0]['yoj_category']):null); ?></textarea>
									</div>
								</div>

								<label>Heading</label>
								<div class="form-group">
									<div class="form-line">
										<input type="textbox" name="yoj_heading" id="yoj_heading" value="<?php echo (!empty($yojDbData[0]['yoj_heading'])? str_replace("-"," ",json_decode($yojDbData[0]['yoj_heading'])):null); ?>" class="form-control" placeholder="Enter heading" required>
									</div>
								</div>

								<label>Yojana</label>
								<div class="form-group">
									<div class="form-line">
										<?php		
										echo $this->ckeditor->editor("yoj_content",json_decode($yojDbData[0]['yoj_content']));
										?>
										<label>
											<?php echo (!empty(form_error('yoj_content')))?form_error('yoj_content'):""; ?></label>
										</div>
									</div>

									<label>Read Time</label>
									<div class="form-group">
										<div class="form-line">
											<input type="text"  minlength="1" maxlength="3" name="yoj_read_time" id="yoj_read_time" class="form-control" placeholder="Enter Read Time (1 to 120)" value="<?php echo (!empty($yojDbData[0]['yoj_read_time'])?$yojDbData[0]['yoj_read_time']:null); ?>" required>
										</div>
									</div>


									<label>Select Attachment</label>
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" name="yoj_attachments" id="yoj_attachments" >
												<option value="">-- Please select --</option>
												<?php if(!empty($attachmentList)){
													foreach ($attachmentList as $key => $value) { ?>
													<option value="<?php echo $value['a_id']; ?>" <?php echo (!empty($yojDbData[0]['yoj_attachments']==$value['a_id'])?'selected':null) ?>><?php echo ucwords($value['a_name']); ?></option>
													<?php } } ?>
												</select>
											</div>
										</div>

										<div class="form-group">
											<div class="form-line">
												<?php $mode = (!empty($yojDbData[0]['yoj_trigger_email']=='yes')?'checked':null); ?>
												<input type="checkbox" id="md_checkbox_21" name="yoj_trigger_email" class="filled-in chk-col-pink" <?php echo $mode; ?> />
												<label for="md_checkbox_21">Share to Subscribed Users Email</label>
												<?php echo (!empty(form_error('yoj_attachments')))?form_error('yoj_attachments'):""; ?></label>
											</div>
										</div>
										<input type="submit" name="submit" class="btn btn-success m-t-15 waves-effect" value="Submit">
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<script>


			</script>

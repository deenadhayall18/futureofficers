<section class="content">
	<div class="container-fluid">
		<div class="text-center" style="padding-bottom:10px" id="err_hide">
			<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
		</div>  
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<div class="align-right"><a type="button" class="btn btn-warning btn-sm" href="<?php echo base_url().'admin/addUPSCPrelimQuestionsList' ?>">UPSC Daily Prelims Question List</a></div>
						<h2>
							<b>Edit UPSC Daily Prelims Question</b>
						</h2>
					</div>
					
					<div class="body">
						<form method="post" name="frmAddUPSCDailyPrelimQues" id="frmAddUPSCDailyPrelimQues" enctype="multipart/form-data">
							<label>Date (Cannot be edited)</label>
							<div class="form-group">
								<div class="form-line">
									<?php $dateVal =  (!empty($dpqDbData[0]['dpq_date'])?$dpqDbData[0]['dpq_date']:date("Y-m-d")); ?>
									<input type="date" readonly class="form-control" placeholder="Enter date" value="<?php echo $dateVal; ?>" required>
								</div>
							</div>
							<label>Select Language</label>
							<div class="form-group">
								<div class="form-line">
									<select class="form-control show-tick" name="dpq_lang" id="dpq_lang" required="true" >
										<option value="">-- Please select --</option>
										<option value="1" <?php echo (!empty($dpqDbData[0]['dpq_lang']==1)?'selected':null) ?>>English</option>
										<option value="2" <?php echo (!empty($dpqDbData[0]['dpq_lang']==2)?'selected':null) ?>>Hindi</option>
									</select>
								</div>
							</div>
							<label>Category</label>
							<div class="form-group">
								<div class="form-line">
									<input type="textbox" name="dpq_category" id="dpq_category" class="form-control" placeholder="Enter Category"  value="<?php echo (!empty($dpqDbData[0]['dpq_category'])?json_decode($dpqDbData[0]['dpq_category']):null); ?>"  required>
								</div>
							</div>

							<label>Select Featured Image</label>
							<div class="form-group">
								<div class="form-line">
									<select class="form-control show-tick" name="dpq_featured_image" id="dpq_featured_image" >
										<option value="">-- Please select --</option>
										
										<?php if(!empty($FimagesList)){
											foreach ($FimagesList as $key => $value) { ?>
											<option value="<?php echo $value['f_i_id']; ?>" <?php echo (!empty($dpqDbData[0]['dpq_featured_image']==$value['f_i_id'])?'selected':null) ?>><?php echo ucwords($value['f_i_name']); ?></option>
											<?php } } ?>
										</select>
									</div>
								</div>

								<label>Tags</label>
								<div class="form-group">
									<div class="form-line">
										<textarea class="form-control" name="dpq_tags" id="dpq_tags" placeholder="Enter Tags" required><?php echo (!empty($dpqDbData[0]['dpq_tags'])?json_decode($dpqDbData[0]['dpq_tags']):null); ?></textarea>
									</div>
								</div>

								<label>Heading</label>
								<div class="form-group">
									<div class="form-line">
										<input type="textbox" name="dpq_heading" id="dpq_heading" value="<?php echo (!empty($dpqDbData[0]['dpq_heading'])? str_replace("-"," ",json_decode($dpqDbData[0]['dpq_heading'])):null); ?>" class="form-control" placeholder="Enter heading" required>
									</div>
								</div>

								<label>UPSC Daily Prelims Question</label>
								<div class="form-group">
									<div class="form-line">
										<?php		
										echo $this->ckeditor->editor("dpq_content",json_decode($dpqDbData[0]['dpq_content']));
										?>
										<label>
											<?php echo (!empty(form_error('dpq_content')))?form_error('dpq_content'):""; ?></label>
										</div>
									</div>

									<label>Read Time</label>
									<div class="form-group">
										<div class="form-line">
											<input type="text"  minlength="1" maxlength="3" name="dpq_read_time" id="dpq_read_time" class="form-control" placeholder="Enter Read Time (1 to 120)" value="<?php echo (!empty($dpqDbData[0]['dpq_read_time'])?$dpqDbData[0]['dpq_read_time']:null); ?>" required>
										</div>
									</div>


									<label>Select Attachment</label>
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" name="dpq_attachments" id="dpq_attachments" >
												<option value="">-- Please select --</option>
												<?php if(!empty($attachmentList)){
													foreach ($attachmentList as $key => $value) { ?>
													<option value="<?php echo $value['a_id']; ?>" <?php echo (!empty($dpqDbData[0]['dpq_attachments']==$value['a_id'])?'selected':null) ?>><?php echo ucwords($value['a_name']); ?></option>
													<?php } } ?>
												</select>
											</div>
										</div>




										<div class="form-group">
											<div class="form-line">
												<?php $mode = (!empty($dpqDbData[0]['dpq_trigger_email']=='yes')?'checked':null); ?>
												<input type="checkbox" id="md_checkbox_21" name="dpq_trigger_email" class="filled-in chk-col-pink" <?php echo $mode; ?> />
												<label for="md_checkbox_21">Share to Subscribed Users Email</label>
												<?php echo (!empty(form_error('dpq_attachments')))?form_error('dpq_attachments'):""; ?></label>
											</div>
										</div>
										<input type="submit" name="submit" class="btn btn-success m-t-15 waves-effect" value="Submit">
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<script>


			</script>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>Sign In</title>
	<!-- Favicon-->
	<link rel="icon" href="<?php echo base_url();?>admin_packages/admin/favicon.ico" type="image/x-icon">
	<!-- Bootstrap Core Css -->
	<link href="<?php echo base_url();?>admin_packages/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
	<!-- Waves Effect Css -->
	<link href="<?php echo base_url();?>admin_packages/admin/plugins/node-waves/waves.css" rel="stylesheet" />
	<!-- Animation Css -->
	<link href="<?php echo base_url();?>admin_packages/admin/plugins/animate-css/animate.css" rel="stylesheet" />
	<!-- Custom Css -->
	<link href="<?php echo base_url();?>admin_packages/admin/css/style.css" rel="stylesheet">
	<style type="text/css">
	.error{color:red;}
	.error1{color:red;text-align:center;font-weight:bold;font-size:19px;}
</style>
<script type="text/javascript">
	setTimeout(
		function(){
			$('#err_hide').fadeOut("slow");
		},1500);
	</script>
</head>

<body class="login-page">
	<div class="login-box">
		<div class="logo" style="text-align:center">
			<span style="font-size:28px;color:#fff;text-align:center"><b>FUTURE OFFICERS</b></span>
			<br>
		</div>

		<div class="card">
			<div class="body">
				<?php $attr = array('id'=>'FrmSignIn','name'=>'FrmSignIn','method'=>'post');
				echo form_open('',$attr); ?>
				<div class="error1" id="err_hide"><?php echo $this->session->flashdata("ErrMsg"); ?></div><br>
				<div class="msg">Sign In</div>

				<div class="input-group">

					<div class="form-line">
						<input type="text" class="form-control" name="username" placeholder="Enter Username"  autofocus>
						<label class="error"><?php echo (!empty(form_error('username'))?form_error('username') :" "); ?></label>
					</div>
				</div>
				<div class="input-group">

					<div class="form-line">
						<input type="password" class="form-control" name="password" placeholder="Enter Password" >
						<label class="error"><?php echo (!empty(form_error('password'))?form_error('password') :" "); ?></label>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-xs-offset-4">
						<input type="submit" name="submit" id="submit" class="btn btn-block bg-pink waves-effect" value="SIGN IN">
					</div>
				</div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
	<!-- Jquery Core Js -->
	<script src="<?php echo base_url();?>admin_packages/admin/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap Core Js -->
	<script src="<?php echo base_url();?>admin_packages/admin/plugins/bootstrap/js/bootstrap.js"></script>
	<!-- Waves Effect Plugin Js -->
	<script src="<?php echo base_url();?>admin_packages/admin/plugins/node-waves/waves.js"></script>
	<script src="<?php echo base_url();?>admin_packages/admin/js/jquery.validate.min.js"> </script>
	<script src="<?php echo base_url();?>admin_packages/admin/js/validate.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#FrmSignIn').validate({
				rules:{
					username:{required:true,letterspaceonly:true},
					password:{required:true},
				},
				messages:{
					username:{required:"Enter the Username",letterspaceonly:"Enter Valid Username"},
					password:{required:"Enter Password"},
				}
			})
		});
	</script>
</body>
</html>
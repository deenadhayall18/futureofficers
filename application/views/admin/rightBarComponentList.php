		<script type="text/javascript">

			function delrecord(id){
				var yes = confirm("Are you Sure ?");
				if(yes){
					frmDCAList.del_rightsidebar_id.value=id;
					frmDCAList.submit();
					return true;
				}else{
					frmDCAList.del_rightsidebar_id.value=null;
					return false;
				}
			}
		</script>

		<section class="content">
			<div class="container-fluid">
				<?php if(!empty($this->session->flashdata('Succ'))){ ?>
				<div class="text-center" style="padding-bottom:10px" id="err_hide">
					<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
				</div>  
				<?php } ?>
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="header">
								<div class="align-right"><a type="button" class="btn btn-warning btn-sm" href="<?php echo base_url().'admin/addRightBarComponent' ?>">+ Add Right Side Bar Component</a></div>
								<h2>
									<b>Right Side Bar List</b>
								</h2>
							</div>
							<div class="body">
								<form method="post" name="frmDCAList" id="frmDCAList" enctype="multipart/form-data">
									
									<label>Select Date</label>
									<div class="form-group">
										<div class="form-line">
											<input type="date"  name="rightsidebar_date"  id="rightsidebar_date" class="form-control" placeholder="Enter date" value="<?php echo date("Y-m-d");?>" required>
										</div>

										<br><br>

										<label>Select Component</label>
										<div class="form-group">
											<!-- <div class="form-line"> -->

												<select required class="form-control show-tick" name="rightsidebar_component" id="rightsidebar_component" >
													<option value="">-- Please select --</option>

													<?php if(!empty($components)){
														foreach ($components as $key => $value) { ?>
														<option value="<?php echo $key; ?>" 

															<?php
															if(!empty($_POST['rightsidebar_component'])){
																echo (!empty($key==$_POST['rightsidebar_component'])?'selected':null);
															}
															?>

															><?php echo ucwords($value); ?></option>
															<?php } } ?>

														</div>
														<input type="submit" name="submit" class="btn btn-success m-t-15 waves-effect" value="Submit">&nbsp;
														<a class="btn btn-info m-t-15" href="<?php $_SERVER['PHP_SELF']; ?>">Reset</a>
													</div>


													<?php if(!empty($rightsidebar_details)){?>
													<div class="table-responsive">
														<table class="table table-bordered table-hover table-striped">
															<thead style="background-color:#000;color: #fff">
																<tr>
																	<th width="5%">No</th>
																	<th width="10%">Date</th>
																	<th width="30%">Category</th>
																	<th width="20%">Action</th>
																</tr>
															</thead>
															<tbody>
																<?php $i=0; foreach ($rightsidebar_details as $key => $value) {
																	$i++; 		
																	?>
																	<tr>
																		<td><?php echo $i; ?></td>
																		<td><?php echo $value['rightsidebar_date'];  ?></td>
																		<td><?php echo json_decode($value['rightsidebar_category']);   ?></td>
																		<td>
																			<input type="hidden" name="rightsidebar_id" id="rightsidebar_id" value="<?php echo $value['rightsidebar_id'];?>">
																			
																			<a class="btn btn-xs btn-warning" title="Edit" href="<?php echo base_url().'admin/editRightBarComponent?rightsidebar_id='.($value["rightsidebar_id"]); ?>" target='_blank'> 
																				<i class="material-icons" >mode_edit</i>
																			</a>
																			
																		</td>
																	</tr>
																	<?php } ?>
																</tbody>
															</table>
														</div>
														<?php } ?>

													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>


							<script>


							</script>

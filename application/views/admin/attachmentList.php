		<script type="text/javascript">

			function delrecord(id){
				var yes = confirm("Are you Sure ?");
				if(yes){
					frmList.deleteid.value=id;
					frmList.submit();
					return true;
				}else{
					frmList.deleteid.value=null;
					return false;
				}
			}
		</script>
		<section class="content">
			<div class="container-fluid">
				<!-- Exportable Table -->
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="header">
								<h2>
									<b>Attachment List</b>
								</h2>
								<div class="text-center" style="padding-bottom:10px" id="err_hide">
									<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
									<span class="errStyle1"><?php echo $this->session->flashdata('DelSucc'); ?></span >
								</div>  
							</div>
							<form method="post" name="frmList" id="frmList">
								
								<div class="body">
									<div class="table-responsive">
										<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
											<thead>
												<tr>
													<th width="5%">Sl.No</th>
													<th width="60%">Name</th>
													<th width="20%">Attachment</th>
													<!-- <th width="15">Action</th> -->
												</tr>
											</thead>
											<tbody>
												<?php $i = 0; foreach ($dbAttachmentList as $value) {
													$i++;	?>
													<tr>
														<td><?php echo $i; ?></td>
														<td><?php echo $value['a_name']; ?></td>
														<td>
															<a target="_blank" class="btn btn-sm btn-primary" href="<?php echo base_url(); ?>uploads/attachment/<?php echo $value['a_name']?>.pdf" class="">Click to view Attachment</a>

														</td>
														
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- #END# Exportable Table -->
				</div>
			</section>

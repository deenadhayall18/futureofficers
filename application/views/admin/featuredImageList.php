		<script type="text/javascript">

			function delrecord(id){
				var yes = confirm("Are you Sure ?");
				if(yes){
					frmList.deleteid.value=id;
					frmList.submit();
					return true;
				}else{
					frmList.deleteid.value=null;
					return false;
				}
			}
		</script>
		<section class="content">
			<div class="container-fluid">
				<!-- Exportable Table -->
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="header">
								<h2>
									<b>Featured Images List</b>
								</h2>
								<div class="text-center" style="padding-bottom:10px" id="err_hide">
									<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
									<span class="errStyle1"><?php echo $this->session->flashdata('DelSucc'); ?></span >
								</div>  
							</div>
							<form method="post" name="frmList" id="frmList">
								
								<div class="body">
									<div class="table-responsive">
										<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
											<thead>
												<tr>
													<th width="5%">Sl.No</th>
													<th width="60%">Name</th>
													<th width="20%">Featured Image</th>
													<!-- <th width="15">Action</th> -->
												</tr>
											</thead>
											<tbody>
												<?php $i = 0; foreach ($dbFIList as $value) {
													$i++;	?>
													<tr>
														<td><?php echo $i; ?></td>
														<td><?php echo $value['f_i_name']; ?></td>
														<td>
															<img width="60px" height="60px" style="border: 1px solid black" src="<?php echo base_url() ?>/uploads/featured_images/<?php echo $value['f_i_name'].'.png' ?>" >
														</td>
														<!-- <td> -->
															<!-- <input type="hidden" name="f_i_id" id="f_i_id" value="<?php echo $value['f_i_id'];?>"> -->
															<!-- <button  type="button" title="Delete" class="btn btn-xs btn-danger" onclick="delrecord(<?php echo $value['f_i_id'];?>)">
																<i class="material-icons">delete_forever</i>
															</button>
															&nbsp;
															&nbsp; -->
															<!-- <a class="btn btn-xs btn-warning" title="Edit" href="<?php echo base_url().'editFeaturedImage?f_i_id='.($value["f_i_id"]); ?>" target='_blank'> 
																<i class="material-icons" >mode_edit</i>
																<input type="hidden" name="editid" id="editid" value="<?php echo $value['f_i_id'];?>">
															</a> -->
															
															<!-- </td> -->
														</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<!-- #END# Exportable Table -->
					</div>
				</section>

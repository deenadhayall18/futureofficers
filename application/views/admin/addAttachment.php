<section class="content">
	<div class="container-fluid">
		<div class="text-center" style="padding-bottom:10px" id="err_hide">
			<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
		</div>  
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<!-- <div class="align-right"><a type="button" class="btn btn-warning btn-sm" href="<?php echo base_url().'dailyLogList' ?>">View Cone List</a></div> -->
						<h2>
							<b>Add Attachment File</b>
						</h2>
					</div>
					<div class="body">
						<form method="post" name="frmAddAttachment" id="frmAddAttachment" enctype="multipart/form-data">
							<label>Name</label>
							<div class="form-group">
								<div class="form-line">
									<input type="text"  name="a_name"  id="a_name" class="form-control" placeholder="Enter Name" >
								</div>
							</div>

							<label>Upload Attachments</label>
							<div class="form-group">
								<div class="form-line">
									<input type="file"  name="a_file"  id="a_file" class="form-control" >
								</div>
							</div>


							<input type="submit" name="submit" class="btn btn-success m-t-15 waves-effect" value="Submit">
						</form>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>
<script>

</script>


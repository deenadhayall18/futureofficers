<section class="content">
	<div class="container-fluid">
		<div class="text-center" style="padding-bottom:10px" id="err_hide">
			<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
		</div>  
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<!-- <div class="align-right"><a type="button" class="btn btn-warning btn-sm" href="<?php echo base_url().'dailyLogList' ?>">View Cone List</a></div> -->
						<h2>
							<b>Add Government Schemes</b>
						</h2>
					</div>
					<div class="body">
						<!-- <form method="post" name="FrmLogDetails" id="FrmLogDetails"> -->
							<?php $attr = array('id'=>'FrmLogDetails','name'=>'FrmLogDetails');
							echo form_open('',$attr); ?>
							<label for="email_address">Date</label>
							<div class="form-group">
								<div class="form-line">
									<input type="date"  name="date"  id="date" class="form-control" placeholder="Enter date" value="<?php echo date("Y-m-d");?>">
								</div>
							</div>
							<label>Category</label>
							<div class="form-group">
								<div class="form-line">
									<input type="textbox"  class="form-control" placeholder="Enter Category" >
								</div>
							</div>

							<label>Featured Image</label>
							<div class="form-group">
								<div class="form-line">
									<input type="file"  class="form-control" placeholder="Enter Catgory" >
								</div>
							</div>

							<label>Tags</label>
							<div class="form-group">
								<div class="form-line">
									<textarea class="form-control" placeholder="Enter Tags"></textarea>
								</div>
							</div>
							<label>Add Government Schemes</label>
							<div class="form-group">
								<div class="form-line">
									<?php		
									echo $this->ckeditor->editor("cat_content");
									?>
									<label id="project" class="error" for="project">
										<?php echo (!empty(form_error('project')))?form_error('project'):""; ?></label>
									</div>
								</div>
								<input type="submit" name="submit" class="btn btn-success m-t-15 waves-effect" value="Submit">
								<!-- </form> -->
								<?php echo form_close(); ?>
							</div>

						</div>
					</div>
				</div>
			</div>
		</section>
		<script>

		</script>


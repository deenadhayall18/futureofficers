		<script type="text/javascript">

			function delrecord(id){
				var yes = confirm("Are you Sure ?");
				if(yes){
					frmList.deleteid.value=id;
					frmList.submit();
					return true;
				}else{
					frmList.deleteid.value=null;
					return false;
				}
			}
		</script>
		<section class="content">
			<div class="container-fluid">
				<!-- Exportable Table -->
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="header">
								<h2>
									<b>Registered Users Details</b>
								</h2>
								<div class="text-center" style="padding-bottom:10px" id="err_hide">
									<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
									<span class="errStyle1"><?php echo $this->session->flashdata('DelSucc'); ?></span >
								</div>  
							</div>
							<form method="post" name="frmList" id="frmList">
								
								<div class="body">
									<div class="table-responsive">
										<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
											<thead>
												<tr>
													<th width="4%">Sl.No</th>
													<th width="40%">Name</th>
													<th width="56%">Email</th>
												</tr>
											</thead>
											<tbody>
												<?php $i = 0; foreach ($dbUserData as $value) {
													$i++;	?>
													<tr>
														<td><?php echo $i; ?></td>
														<td><?php echo $value['username']; ?></td>
														<td><?php echo $value['useremail']; ?></td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- #END# Exportable Table -->
				</div>
			</section>

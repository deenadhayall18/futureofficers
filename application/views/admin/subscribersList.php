<script type="text/javascript">
	function delrecord(id){
		var yes = confirm("Are you sure ?");
		if(yes){
			frmTaskList.deleteid.value = id;
			frmTaskList.submit();
		}else{
			frmTaskList.deleteid.value =null;
		}
	}
</script>	
<section class="content">
	<div class="container-fluid">
		<div class="text-center" style="padding-bottom:10px" id="err_hide">
			<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
			<span class="errStyle1"><?php echo $this->session->flashdata('DelSucc'); ?></span >
		</div> 
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<div id="delButtonPopUp" style="display:none;margin-right:60px" class="text-right">
							<div class="row clearfix js-sweetalert">
								<button class="btn btn-xs btn-danger" ><i class="material-icons" title="Delete Log" >delete_forever</i>Delete Selected  </button>
							</div>
						</div>
						<h2>
							<b>Subscribers List</b>
						</h2>
					</div>
					<div class="body">
						<!-- <form name="frmTaskList" id="frmTaskList" method="post" > -->
							<?php $attr = array('name'=>'frmTaskList','method'=>'post','id'=>'frmTaskList'); 
							echo form_open('',$attr);
							?>
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
									<thead>
										<tr>
											<th width="4%">Sl.No</th>
											<th width="35%">Email</th>
										</tr>
									</thead>
									<tbody>
										<?php if(!empty($dbSubUserData)){
											$i =0;
											foreach ($dbSubUserData as $value) {$i++
												?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $value['sub_email']; ?></td>
													<?php } 
												} ?>
											</tbody>
										</table>
										<?php echo form_close(); ?>
										<!-- </form> -->
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- #END# Exportable Table -->
				</div>
			</section>

﻿<!DOCTYPE html>
<html>

<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-167955626-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-167955626-1');
	</script>

	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">

	<title>Future Officers</title>
	<!-- Favicon-->
	<!-- <link rel="icon" href="<?php echo base_url(); ?>admin_packages/admin/favicon.ico" type="image/x-icon"> -->
	<link href="<?php echo base_url(); ?>admin_packages/admin/css/google_text_css.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>admin_packages/admin/css/google_text_icons.css" rel="stylesheet" type="text/css">
	<!-- Bootstrap Core Css -->
	<link href="<?php echo base_url(); ?>admin_packages/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
	<!-- Waves Effect Css -->
	<link href="<?php echo base_url(); ?>admin_packages/admin/plugins/node-waves/waves.css" rel="stylesheet" />
	<!-- Animation Css -->
	<link href="<?php echo base_url(); ?>admin_packages/admin/plugins/animate-css/animate.css" rel="stylesheet" />
	<!-- JQuery DataTable Css -->
	<link href="<?php echo base_url(); ?>admin_packages/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

	<link href="<?php echo base_url(); ?>admin_packages/admin/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">

	<link href="<?php echo base_url(); ?>admin_packages/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />



	<!-- Custom Css -->
	<link href="<?php echo base_url(); ?>admin_packages/admin/css/style.css" rel="stylesheet">
	<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
	<link href="<?php echo base_url(); ?>admin_packages/admin/css/themes/all-themes.css" rel="stylesheet" />
	<script src="<?php echo base_url();?>admin_packages/admin/plugins/ckeditor/ckeditor.js"></script>
	<style type="text/css">

	.errStyle{color:green;font-weight:bold;font-size:16px;}
	.errStyle1{color:red;font-weight:bold;font-size:16px;}
</style>

<!-- Facebook -->
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v7.0"></script>
<!-- Facebook -->
<script type="text/javascript">
	$(function(){
		$(".SubmitEditImage").hide();
	});

</script>

</head>

<body class="theme-red">
	<!-- Page Loader -->
	<div class="page-loader-wrapper">
		<div class="loader">
			<div class="preloader">
				<div class="spinner-layer pl-red">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div>
			<p>Please wait...</p>
		</div>
	</div>
	
	<div class="overlay"></div>
	
	<div class="search-bar">
		<div class="search-icon">
			<i class="material-icons" style="color:darkblue">search</i>
		</div>
		<input type="text" placeholder="START TYPING...">
		<div class="close-search">
			<i class="material-icons" style="color:darkblue">close</i>
		</div>
	</div>
	<nav class="navbar">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
				<a href="javascript:void(0);" class="bars"></a>
				<a class="navbar-brand" href="dashboard">Future Officers</a>
			</div>
			<div class="collapse navbar-collapse" id="navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a class="btn btn-primary" href="<?php echo base_url() ?>admin/logout/" >
							Log Out
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</nav>

	<section>
		<aside id="leftsidebar" class="sidebar">
			<div class="menu">
				<ul class="list">
					<li class="<?php echo(($url=='dashboard')?'active':"");?>">
						<a href="dashboard">
							<i class="material-icons" style="color:darkblue">home</i>
							<span>Home</span>
						</a>
					</li>


					<li class="<?php echo(($url=='usersList')?'active':"");?>  <?php echo(($url=='subscribersList')?'active':"");?>">
						<a href="javascript:void(0);" class="menu-toggle">
							<i class="material-icons" style="color:grey">person</i>
							<span>User Website Data</span>
						</a>
						<ul class="ml-menu">
							<li class="<?php echo(($url=='usersList')?'active':"");?>">
								<a href="usersList">Users List</a>
							</li>
							<li class="<?php echo(($url=='subscribersList')?'active':"");?>">
								<a href="subscribersList">Subscribers List</a>
							</li>
						</ul>
					</li>

					<li class="<?php echo(($url=='addFeaturedImage')?'active':"");?>  <?php echo(($url=='featuredImageList')?'active':"");?>  <?php echo(($url=='editFeaturedImage')?'active':"");?>">
						<a href="javascript:void(0);" class="menu-toggle">
							<i class="material-icons" style="color:green">image</i>
							<span>Featured Image</span>
						</a>
						<ul class="ml-menu">
							<li class="<?php echo(($url=='addFeaturedImage')?'active':"");?>">
								<a href="addFeaturedImage">Add Featured Image</a>
							</li>
							<li class="<?php echo(($url=='featuredImageList')?'active':"");?>">
								<a href="featuredImageList">Featured Image List</a>
							</li>
						</ul>
					</li>

					<li class="<?php echo(($url=='addAttachment')?'active':"");?>  <?php echo(($url=='attachmentList')?'active':"");?>">
						<a href="javascript:void(0);" class="menu-toggle">
							<i class="material-icons" style="color:brown">attach_file</i>
							<span>Attachment</span>
						</a>
						<ul class="ml-menu">
							<li class="<?php echo(($url=='addAttachment')?'active':"");?>">
								<a href="addAttachment">Add Attachment</a>
							</li>
							<li class="<?php echo(($url=='attachmentList')?'active':"");?>">
								<a href="attachmentList">Attachment List</a>
							</li>
						</ul>
					</li>



					

					<li class="<?php echo(($url=='addDailyCurrentAffairs')?'active':"");?>  <?php echo(($url=='dailyCurrentAffairsCategoryList')?'active':"");?>">
						<a href="javascript:void(0);" class="menu-toggle">
							<i class="material-icons" style="color:orange">note_add</i>
							<span>Daily Current Affairs</span>
						</a>
						<ul class="ml-menu">
							<li class="<?php echo(($url=='addDailyCurrentAffairs')?'active':"");?>">
								<a href="addDailyCurrentAffairs">Add Daily Current Affairs</a>
							</li>
							<li class="<?php echo(($url=='dailyCurrentAffairsCategoryList')?'active':"");?>">
								<a href="dailyCurrentAffairsCategoryList">Daily Current Affairs List</a>
							</li>
						</ul>
					</li>

					<li class="<?php echo(($url=='addAboutTheDay')?'active':"");?>">
						<a href="javascript:void(0);" class="menu-toggle">
							<i class="material-icons" style="color:aqua">note_add</i>
							<span>About the Day</span>
						</a>
						<ul class="ml-menu">
							<li class="<?php echo(($url=='addAboutTheDay')?'active':"");?>">
								<a href="addAboutTheDay">Add About the Day</a>
							</li>
							<li class="<?php echo(($url=='aboutTheDayCategoryList')?'active':"");?>">
								<a href="aboutTheDayCategoryList">About the Day List</a>
							</li>
						</ul>
					</li>

					<li class="<?php echo(($url=='addnewsPaperAnalysis')?'active':"");?> <?php echo(($url=='newsPaperAnalysisList')?'active':"");?>">
						<a href="javascript:void(0);" class="menu-toggle">
							<i class="material-icons" style="color:coral">note_add</i>
							<span>News paper analysis</span>
						</a>
						<ul class="ml-menu">
							<li class="<?php echo(($url=='addnewsPaperAnalysis')?'active':"");?>">
								<a href="addnewsPaperAnalysis">Add News paper analysis</a>
							</li>
							<li class="<?php echo(($url=='newsPaperAnalysisList')?'active':"");?>">
								<a href="newsPaperAnalysisList">News paper analysis List</a>
							</li>
						</ul>
					</li>



					<li class="<?php echo(($url=='addUPSCPrelimQuestions')?'active':"");?> <?php echo(($url=='addUPSCPrelimQuestionsList')?'active':"");?>">
						<a href="javascript:void(0);" class="menu-toggle">
							<i class="material-icons" style="color:blue">note_add</i>
							<span>Daily UPSC Prelim Questions</span>
						</a>
						<ul class="ml-menu">
							<li class="<?php echo(($url=='addUPSCPrelimQuestions')?'active':"");?>">
								<a href="addUPSCPrelimQuestions">Add Daily UPSC Prelim Questions</a>
							</li>
							<li class="<?php echo(($url=='addUPSCPrelimQuestionsList')?'active':"");?>">
								<a href="addUPSCPrelimQuestionsList">Daily UPSC Prelim Questions List</a>
							</li>
						</ul>
					</li>


					<li class="<?php echo(($url=='addUPSCDailyQuestions')?'active':"");?> <?php echo(($url=='addUPSCDailyQuestionsList')?'active':"");?>">
						<a href="javascript:void(0);" class="menu-toggle">
							<i class="material-icons" style="color:green">note_add</i>
							<span>Daily UPSC Main Questions</span>
						</a>
						<ul class="ml-menu">
							<li class="<?php echo(($url=='addUPSCDailyQuestions')?'active':"");?>">
								<a href="addUPSCDailyQuestions">Add Daily UPSC Main Questions</a>
							</li>
							<li class="<?php echo(($url=='addUPSCDailyQuestionsList')?'active':"");?>">
								<a href="addUPSCDailyQuestionsList">Daily UPSC Main Questions List</a>
							</li>
						</ul>
					</li>

					<li class="<?php echo(($url=='addPreviousYearPrelimQuestions')?'active':"");?> <?php echo(($url=='addPreviousYearPrelimQuestionsList')?'active':"");?>">
						<a href="javascript:void(0);" class="menu-toggle">
							<i class="material-icons" style="color:black">note_add</i>
							<span>Previous Year Prelim Questions</span>
						</a>
						<ul class="ml-menu">
							<li class="<?php echo(($url=='addPreviousYearPrelimQuestions')?'active':"");?>">
								<a href="addPreviousYearPrelimQuestions">Add Previous Year Prelim Questions</a>
							</li>
							<li class="<?php echo(($url=='previousYearPrelimQuestionsList')?'active':"");?>">
								<a href="previousYearPrelimQuestionsList">Previous Year Prelim Questions List</a>
							</li>
						</ul>
					</li>

					<li class="<?php echo(($url=='addPreviousYearMainQuestions')?'active':"");?> <?php echo(($url=='addPreviousYearMainQuestionsList')?'active':"");?>">
						<a href="javascript:void(0);" class="menu-toggle">
							<i class="material-icons" style="color:violet">note_add</i>
							<span>Previous Year Main Questions</span>
						</a>
						<ul class="ml-menu">
							<li class="<?php echo(($url=='addPreviousYearMainQuestions')?'active':"");?>">
								<a href="addPreviousYearMainQuestions">Add Previous Year Main Questions</a>
							</li>
							<li class="<?php echo(($url=='previousYearMainQuestionsList')?'active':"");?>">
								<a href="previousYearMainQuestionsList">Previous Year Main Questions List</a>
							</li>
						</ul>
					</li>


					<li class="<?php echo(($url=='addyojana')?'active':"");?> <?php echo(($url=='yojanaList')?'active':"");?>">
						<a href="javascript:void(0);" class="menu-toggle">
							<i class="material-icons" style="color:magenta">note_add</i>
							<span>Yojana</span>
						</a>
						<ul class="ml-menu">
							<li class="<?php echo(($url=='addyojana')?'active':"");?>">
								<a href="addyojana">Add Yojana</a>
							</li>
							<li class="<?php echo(($url=='yojanaList')?'active':"");?>">
								<a href="yojanaList">Yojana List</a>
							</li>
						</ul>
					</li>


					<li class="<?php echo(($url=='addKurukshetra')?'active':"");?> <?php echo(($url=='kurukshetraList')?'active':"");?>">
						<a href="javascript:void(0);" class="menu-toggle">
							<i class="material-icons" style="color:#012C37">note_add</i>
							<span>Kurukshetra</span>
						</a>
						<ul class="ml-menu">
							<li class="<?php echo(($url=='addKurukshetra')?'active':"");?>">
								<a href="addKurukshetra">Add Kurukshetra</a>
							</li>
							<li class="<?php echo(($url=='KurukshetraList')?'active':"");?>">
								<a href="KurukshetraList">Kurukshetra List</a>
							</li>
						</ul>
					</li>





					<li class="<?php echo(($url=='addRightBarComponent')?'active':"");?> <?php echo(($url=='rightBarComponentList')?'active':"");?>">
						<a href="javascript:void(0);" class="menu-toggle">
							<i class="material-icons" style="color:orange">note_add</i>
							<span>Right Sidebar Component</span>
						</a>
						<ul class="ml-menu">
							<li class="<?php echo(($url=='addRightBarComponent')?'active':"");?>">
								<a href="addRightBarComponent">Add Right Sidebar Component</a>
							</li>
							<li class="<?php echo(($url=='rightBarComponentList')?'active':"");?>">
								<a href="rightBarComponentList">Right Sidebar Component List</a>
							</li>
						</ul>
					</li>




					

				</ul>
			</div>

		<!-- 	<div class="legal text-left">
				<div class="copyright">
					<a href="javascript:void(0);">@ Copy rights reserved</a>
				</div>
				<div class="version">
					<b>Version: </b> 1.0.0
				</div>
			</div> -->

		</aside>

		

	</section>

	<?php $this->load->view($url); ?>

	<!-- Jquery Core Js -->
	<script src="<?php echo base_url(); ?>admin_packages/admin/plugins/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core Js -->
	<script src="<?php echo base_url(); ?>admin_packages/admin/plugins/bootstrap/js/bootstrap.js"></script>

	<!-- Select Plugin Js -->
	<script src="<?php echo base_url(); ?>admin_packages/admin/plugins/bootstrap-select/js/bootstrap-select.js"></script>

	<!-- Slimscroll Plugin Js -->
	<!-- <script src="<?php echo base_url(); ?>admin_packages/admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script> -->

	<!-- Waves Effect Plugin Js -->
	<script src="<?php echo base_url(); ?>admin_packages/admin/plugins/node-waves/waves.js"></script>

	<!-- Jquery DataTable Plugin Js -->
	<script src="<?php echo base_url(); ?>admin_packages/admin/plugins/jquery-datatable/jquery.dataTables.js"></script>
	<script src="<?php echo base_url(); ?>admin_packages/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
	<script src="<?php echo base_url(); ?>admin_packages/admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
	<!-- <script src="<?php echo base_url(); ?>admin_packages/admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script> -->
	<script src="<?php echo base_url(); ?>admin_packages/admin/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
	<script src="<?php echo base_url(); ?>admin_packages/admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
	<script src="<?php echo base_url(); ?>admin_packages/admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
	<script src="<?php echo base_url(); ?>admin_packages/admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
	<script src="<?php echo base_url(); ?>admin_packages/admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
	<script src="<?php echo base_url(); ?>admin_packages/admin/plugins/bootstrap-material-datetimepicker/js/	bootstrap-material-datetimepicker.js"></script>
	<!-- Custom Js -->
	<script src="<?php echo base_url(); ?>admin_packages/admin/js/admin.js"></script>
	<script src="<?php echo base_url(); ?>admin_packages/admin/js/pages/tables/jquery-datatable.js"></script>

	<!--Validation -->
	<script src="<?php echo base_url();?>admin_packages/admin/js/jquery.validate.min.js"> </script>
	<!-- <script src="<?php echo base_url();?>admin_packages/admin/js/validate.js"></script> -->
	<script src="<?php echo base_url();?>admin_packages/admin/js/jquery-ui.min.js"></script>
	<script src="<?php echo base_url();?>admin_packages/admin/js/jqval-additional-methods.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>admin_packages/admin/js/validate.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>admin_packages/admin/js/functions.js" type="text/javascript"></script>


	<script type="text/javascript">

		$(document).ready(function(){
			$('#btn_allow_attachment_upload').click(function(){
				$('#showExistFImage').hide();
				$('#uploadFImage').show();

			});
		});



		// DCA - Validation
		$(function(){
			$('#frmAddDCA').validate({
				rules:{
					dca_date:{required:true},
					dca_category:{required:true,minlength:3,maxlength:120},
					dca_content:{required:true,minlength:10},
					dca_read_time:{required:true,minValue:1,maxValue:120},
					dca_featured_image:{required:true},
					dca_tags:{required:true},
					dca_heading:{required:true},
				},
				messages:{
					dca_date:{required:'Enter Date'},
					dca_category:{required:'Enter Category',minlength:'Minimum of 3 characters required',maxlength:'Maximum of 120 characters allowed'},
					dca_content:{required:'Enter Content',minlength:'Minimum of 10 characters required'},
					dca_read_time:{required:'Enter Read Time',minValue:'Minimum of 1 min allowed',maxValue:'Maximum of 120 min allowed'},
					dca_featured_image:{required:'Select Featured Image'},
					dca_tags:{required:'Enter Tags'},
					dca_heading:{required:'Enter the Heading Parkavi !'},
				}
			})
		});

		// NPA
		$(function(){
			$('#frmAddnpa').validate({
				rules:{
					npa_date:{required:true},
					npa_category:{required:true,minlength:3,maxlength:120},
					npa_content:{required:true,minlength:10},
					npa_read_time:{required:true,minValue:1,maxValue:120},
					npa_featured_image:{required:true},
					npa_tags:{required:true},
					npa_heading:{required:true},
				},
				messages:{
					npa_date:{required:'Enter Date'},
					npa_category:{required:'Enter Category',minlength:'Minimum of 3 characters required',maxlength:'Maximum of 120 characters allowed'},
					npa_content:{required:'Enter Content',minlength:'Minimum of 10 characters required'},
					npa_read_time:{required:'Enter Read Time',minValue:'Minimum of 1 min allowed',maxValue:'Maximum of 120 min allowed'},
					npa_featured_image:{required:'Select Featured Image'},
					npa_tags:{required:'Enter Tags'},
					npa_heading:{required:'Enter the Heading Parkavi !'},
				}
			})
		});



		$(function(){
			$('#frmAboutDay').validate({
				rules:{
					aboutday_date:{required:true},
					aboutday_category:{required:true,minlength:3,maxlength:120},
					aboutday_featured_image:{required:true,minlength:10},
					aboutday_read_time:{required:true,minValue:1,maxValue:120},
					aboutday_featured_image:{required:true},
					aboutday_tags:{required:true},
					aboutday_heading:{required:true},
				},
				messages:{
					aboutday_date:{required:'Enter Date'},
					aboutday_category:{required:'Enter Category',minlength:'Minimum of 3 characters required',maxlength:'Maximum of 120 characters allowed'},
					aboutday_featured_image:{required:'Enter Content',minlength:'Minimum of 10 characters required'},
					aboutday_read_time:{required:'Enter Read Time',minValue:'Minimum of 1 min allowed',maxValue:'Maximum of 120 min allowed'},
					aboutday_featured_image:{required:'Select Featured Image'},
					aboutday_tags:{required:'Enter Tags'},
					aboutday_heading:{required:'Enter Heading'},
				}
			})
		});


		$(function(){
			$('#frmAddUPSCDailyPrelimQues').validate({
				rules:{
					dpq_date:{required:true},
					dpq_category:{required:true,minlength:3,maxlength:120},
					dpq_featured_image:{required:true},
					dpq_tags:{required:true},
					dpq_heading:{required:true},
					dpq_read_time:{required:true,minValue:1,maxValue:120},
				},
				messages:{
					dpq_date:{required:'Enter Date'},
					dpq_category:{required:'Enter Category',minlength:'Minimum of 3 characters required',maxlength:'Maximum of 120 characters allowed'}, 
					dpq_tags:{required:'Enter tags'},
					dpq_read_time:{required:'Enter Read Time',minValue:'Minimum of 1 min allowed',maxValue:'Maximum of 120 min allowed'},
					dpq_heading:{required:'Enter Heading'},
				}
			})
		});

		$(function(){
			$('#frmAddDailyMainQuestions').validate({
				rules:{
					dmq_date:{required:true},
					dmq_category:{required:true,minlength:3,maxlength:120},
					dmq_featured_image:{required:true},
					dmq_tags:{required:true},
					dmq_heading:{required:true},
					dmq_read_time:{required:true,minValue:1,maxValue:120},
				},
				messages:{
					dmq_date:{required:'Enter Date'},
					dmq_category:{required:'Enter Category',minlength:'Minimum of 3 characters required',maxlength:'Maximum of 120 characters allowed'}, 
					dmq_tags:{required:'Enter tags'},
					dmq_read_time:{required:'Enter Read Time',minValue:'Minimum of 1 min allowed',maxValue:'Maximum of 120 min allowed'},
					dmq_heading:{required:'Enter Heading'},
				}
			})
		});

		$(function(){
			$('#frmAddrightsidebar').validate({
				rules:{
					rightsidebar_date:{required:true},
					rightsidebar_category:{required:true,minlength:3,maxlength:120},
					rightsidebar_component:{required:true},
					rightsidebar_featured_image:{required:true},
					rightsidebar_tags:{required:true},
					rightsidebar_heading:{required:true},
					rightsidebar_read_time:{required:true,minValue:1,maxValue:120},
				},
				messages:{
					rightsidebar_date:{required:'Enter Date'},
					rightsidebar_category:{required:'Enter Category',minlength:'Minimum of 3 characters required',maxlength:'Maximum of 120 characters allowed'}, 
					rightsidebar_tags:{required:'Enter tags'},
					rightsidebar_component:{required:'Enter Content'},
					rightsidebar_read_time:{required:'Enter Read Time',minValue:'Minimum of 1 min allowed',maxValue:'Maximum of 120 min allowed'},
					rightsidebar_heading:{required:'Enter Heading'},
				}
			})
		});


		$(function(){
			$('#frmAddFeaturedImage').validate({
				rules:{
					f_i_name:{required:true},
					f_image:{required:true,maxfilesize:5120,extension:"jpeg|jpg|png"}
				},
				messages:{
					f_i_name:{required:'Enter Name'},
					f_image:{required:'Upload Featured Image',maxfilesize:'Maximum of 5 MB only allowed',extension:"jpeg | jpg | png only allowed"},
				}
			})
		});

		$(function(){
			$('#frmAddAttachment').validate({
				rules:{
					a_name:{required:true},
					a_file:{required:true,maxfilesize:10240,extension:"pdf"}
				},
				messages:{
					a_name:{required:'Enter Name'},
					a_file:{required:'Upload Attachment',maxfilesize:'Maximum of 10 MB only allowed',extension:"PDF only allowed"},
				}
			})
		});





		setTimeout(
			function(){
				$('#err_hide').fadeOut("slow");
			},1500);

		$(document).ready(function(){
			$('#md_checkbox_40').change(function(){
				$('#delButtonPopUp').toggle();
			});
		});








	</script>


</body>

</html>
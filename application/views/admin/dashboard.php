<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>DASHBOARD</h2>
		</div>
		<div class="row clearfix">
			<a href="<?php echo base_url (); ?>admin/usersList">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="info-box bg-orange hover-zoom-effect">
						<div class="icon">
							<i class="material-icons">person</i>
						</div>
						<div class="content">
							<div class="text" style="font-size:16px !important">Users Registered</div>
							<div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20"><?php echo $UsersListCount; ?></div>
						</div>
					</div>
				</div>
			</a>
			<a href="<?php echo base_url (); ?>admin/subscribersList">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="info-box bg-grey hover-zoom-effect">
						<div class="icon">
							<i class="material-icons">email</i>
						</div>
						<div class="content">
							<div class="text" style="font-size:16px !important">Subscribers</div>
							<div class="number count-to" data-from="0" data-to="125" data-speed="2000" data-fresh-interval="20"><?php echo $SubscriberListCount; ?></div>
						</div>
					</div>
				</div>
			</a>
			<a target="_blank" href="https://analytics.google.com/analytics/web/?authuser=1#/report-home/a167955626w234253886p219669317">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="info-box bg-pink hover-zoom-effect">
						<div class="icon">
							<i class="material-icons">assessment</i>
						</div>
						<div class="content">
							<div class="text" style="font-size:16px !important">Google Analytics</div>
							<div class="number count-to" data-from="0" data-to="125" data-speed="2000" data-fresh-interval="20"></div>
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
</section>

<script type="text/javascript">
	function showUploadIcon(){
		var yes = confirm("Are you Sure ?");
		if(yes){
			document.getElementById('submit').style.display = 'block';
			document.getElementById('f_image').style.display = 'block';
			return true;
		}else{
			document.getElementById('submit').style.display = 'none';
			document.getElementById('f_image').style.display = 'none';
			return true;
		}
	}
</script>
<section class="content">
	<div class="container-fluid">
		<div class="text-center" style="padding-bottom:10px" id="err_hide">
			<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
		</div>  
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<!-- <div class="align-right"><a type="button" class="btn btn-warning btn-sm" href="<?php echo base_url().'dailyLogList' ?>">View Cone List</a></div> -->
						<h2>
							<b>Edit Featured Image</b>
						</h2>
					</div>
					<div class="body">
						<form method="post" name="frmAddFeaturedImage" id="frmAddFeaturedImage" enctype="multipart/form-data">
							<label>Name [Name cannot be edited]</label>
							<div class="form-group">
								<div class="form-line">
									<input type="text" value="<?php echo $dbFIData[0]['f_i_name']; ?>" readonly class="form-control" placeholder="Enter Name" >
								</div>
							</div>
							<label>Upload Image</label>
							<div class="form-group">
								<div class="form-line">
									<?php if(!empty($dbFIData[0]['f_i_name'])){ ?>
									<img width="100px" height="100px" src="<?php echo base_url() ?>/uploads/featured_images/<?php echo $dbFIData[0]['f_i_name'].'.png'; ?>">
									<?php } ?>
									<button type="button" class="btn btn-warning" onclick="showUploadIcon()">Upload New Image</button>
									<input type="file" name="f_image" id="f_image" class="form-control SubmitEditImage">
								</div>
							</div>
							<input type="submit" name="submit" id="submit" class="btn btn-success m-t-15 waves-effect" value="Submit" style="visibility:none !important">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
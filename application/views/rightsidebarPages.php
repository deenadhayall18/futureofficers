<!-- BEGIN #content -->
<div id="content" class="app-content">
	<h1 class="page-header mb-3">
		<?php 
		// $componentsArr = array(
		// 	'Government policies and Schemes' => 'governPolSchemes',
		// 	'Static GK' => 'staticGk',
		// 	'History - Ancient India, Medieval India, Modern India' => 'history',
		// 	'Indian Art and Culture' => 'indianArtCulture',
		// 	'Geography - World geography, Indian Geography' => 'geo',
		// 	'Environment and Bio Diversity' => 'enbio',
		// 	'Science and Technology' => 'scienceTech',
		// 	'Indian Polity' => 'indianPol',
		// 	'Governance' => 'govern',
		// 	'Indian Economy' => 'indianEco',
		// 	'Ethics' => 'ethics'
		// );
		$componentsArr = array(
			'Government policies and Schemes' => 'governPolSchemes',
			'Static GK' => 'staticGk',
			'Indian Art and Culture' => 'indianArtCulture',
			'Geography - World Geography' => 'geoWorld',
			'Geography - Indian Geography' => 'geoIndian',
			'Environment and Bio Diversity' => 'enbio',
			'Science and Technology' => 'scienceTech',
			'Indian Polity' => 'indianPol',
			'Governance' => 'govern',
			'Indian Economy' => 'indianEco',
			'Ethics' => 'ethics',
			'History - Ancient India'=>'historyAncientIndia',
			'History - Medieval India'=>'historyMedievalIndia',
			'History - Modern India'=>'historyModernIndia',
			'Optional Paper - Sociology'=>'optionalPaperSociology',
			'Optional Paper - Public Administration'=>'optionalPaperPublicAdmin'
		);

		echo $r =array_search($rightSideBarData[0]['rightsidebar_component'],$componentsArr,true);
		?>
	</h1> 		

	<!-- BEGIN row -->
	<div class="row">
		<!-- BEGIN col-6 -->
		<div class="col-xl-12">
			<!-- BEGIN row -->
			<div class="row">
				<!-- BEGIN col-6 -->
				<?php if(!empty($rightSideBarData)){
					foreach ($rightSideBarData as $rsbkey => $rsbval) {?>
					<div class="col-sm-3">
						<!-- BEGIN card -->
						<?php $rightsidebar = strrev(base64_encode($rsbval['rightsidebar_id'])); ?>
						<a href="<?php echo base_url().'rightsidebarInnerPages?rightsidebar_id='.$rightsidebar ?>">
							<div class="card mb-3 overflow-hidden fs-13px border-0 " style="min-height: 202px;">
								<!-- BEGIN card-body -->
								<div class="card-body position-relative">
									<?php if($rsbval['rightsidebar_date']==date('Y-m-d')){?>
									<div class="d-flex mb-3">
										<div class=" d-flex align-items-center">
											<i class="fa fa-circle fs-2px fa-fw text-danger mr-2 blink_me"></i> <span style="color:red !important;font-weight:bold !important;text-decoration:none !important;cursor:pointer !important;">Posted Today</span>
										</div>
									</div>
									<?php } ?>
									
									<img src="<?php echo str_replace('web', 'admin',base_url()); ?>/uploads/featured_images/<?php echo $rsbval['rightsidebar_featured_image'].'.png' ?>" width="100%" height="110px">
									<div><a href="<?php echo base_url().'rightsidebarInnerPages?rightsidebar_id='.$rightsidebar ?>" class="text-black d-flex align-items-center text-decoration-none">&nbsp;&nbsp;&nbsp;<span style="color:red;font-weight:bold"><?php echo  date('M d,  Y',strtotime($rsbval['rightsidebar_date'])); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="color:black;font-weight:bold"><?php echo (!empty($rsbval['rightsidebar_read_time'])?$rsbval['rightsidebar_read_time']:null) ?> min read</span></a></div>
								</div>
								<!-- BEGIN card-body -->
							</div>
						</a>
						<!-- END card -->
					</div>
					<?php } } ?>
				</div>
				<!-- END row -->
			</div>
			<!-- END col-6 -->
		</div>
		<!-- END row -->







	</div>
		<!-- END #content -->
<!-- BEGIN #content -->
<div id="content" class="app-content">
	<!-- BEGIN container -->
	<div class="container">
		<!-- BEGIN row -->
		<div class="row justify-content-center">
			<!-- BEGIN col-10 -->
			<div class="col-xl-12">
				<!-- BEGIN row -->
				<div class="row">
					<!-- BEGIN col-9 -->
					<div class="col-xl-9">


						<ul class="breadcrumb">
							<?php 
							$componentsArr = array(
								'Government policies and Schemes' => 'governPolSchemes',
								'Static GK' => 'staticGk',
								'Indian Art and Culture' => 'indianArtCulture',
								'Geography - World Geography' => 'geoWorld',
								'Geography - Indian Geography' => 'geoIndian',
								'Environment and Bio Diversity' => 'enbio',
								'Science and Technology' => 'scienceTech',
								'Indian Polity' => 'indianPol',
								'Governance' => 'govern',
								'Indian Economy' => 'indianEco',
								'Ethics' => 'ethics',
								'History - Ancient India'=>'historyAncientIndia',
								'History - Medieval India'=>'historyMedievalIndia',
								'History - Modern India'=>'historyModernIndia',
								'Optional Paper - Sociology'=>'optionalPaperSociology',
								'Optional Paper - Public Administration'=>'optionalPaperPublicAdmin'
							);



							$r =array_search($rightSideBarInnerPageData[0]['rightsidebar_component'],$componentsArr,true);
							?>
							<li class="breadcrumb-item"><a href="<?php echo base_url() ?>rightsidebarPages?component=<?php echo $rightSideBarInnerPageData[0]['rightsidebar_component'] ?>"><?php echo $r ?></a></li>
							<li class="breadcrumb-item active"><?php echo json_decode($rightSideBarInnerPageData[0]['rightsidebar_category']); ?></li>
						</ul>

						<hr class="mb-4" />

						<h1 class="page-header mb-3">
							<?php echo ucwords(str_replace("-"," ",json_decode($rightSideBarInnerPageData[0]['rightsidebar_heading']))); ?>
						</h1> 	

						<!-- BEGIN #abbreviations -->
						<div id="abbreviations" class="mb-5">

							<p style="width: 100% !important;height: auto"><?php		
							echo html_entity_decode(json_decode($rightSideBarInnerPageData[0]['rightsidebar_content']));
							?>
						</p>
						<?php if(empty($_SESSION['useremail'])){ ?> 
						<br>
						<div class="text-center">
							<button type="button" data-toggle="modal" data-target="#modalLg" class="btn btn-outline-danger">Download PDF Here</button>
							<div class="modal fade" id="modalLg">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title">You want to see what is inside, just satisfied with the preview? I guess not! <br>For a wholesome experience just click <b>Sign In</b>!</h5>
											<button type="button" class="close" data-dismiss="modal">
												<span>&times;</span>
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php }elseif(!empty($rightSideBarInnerPageData[0]['rightsidebar_attachments'])){ ?>
						<div class="text-center"><a href="<?php echo str_replace('web', 'admin',base_url()); ?>uploads/attachments/<?php echo $rightSideBarInnerPageData[0]['rightsidebar_attachments'].'.pdf' ?>"  target='_blank'><button type="button" class="btn btn-outline-danger">Download PDF Here</button></a></div>
						<?php } ?>
					</div>
					<!-- END #abbreviations -->
				</div>
				<!-- END col-9-->
				<?php $this->load->view('rightSideBar'); ?>
			</div>
			<!-- END row -->
		</div>
		<!-- END col-10 -->
	</div>
	<!-- END row -->
</div>
<!-- END container -->
</div>
			<!-- END #content -->
<?php 
class Admin_model extends CI_Model{


	public function checkUserExists($logindata){
		$data['userid']=$logindata['username'];
		$data['password'] = $logindata['password'];
		$res = $this->db->select("*")->from("tbl_login")->where($data);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function StoreDCAData($data_to_store){
		$res = $this->db->insert("tbl_dca",$data_to_store);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}
	public function StoreNPAData($data_to_store){
		$res = $this->db->insert("tbl_news_paper_analysis",$data_to_store);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	public function StorekurukshetraData($data_to_insert){
		$res = $this->db->insert("tbl_kurukshetra",$data_to_insert);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	public function StorePYmqData($data_to_store){
		$res = $this->db->insert("tbl_previous_year_main_question",$data_to_store);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	public function StoreDPQData($data_to_store){
		$res = $this->db->insert("tbl_daily_prelims_question",$data_to_store);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	} 

	public function StorePYPQData($data_to_insert){
		$res = $this->db->insert("tbl_previous_year_prelim_question",$data_to_insert);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	} 

	public function StoreyojanaData($data_to_insert){
		$res = $this->db->insert("tbl_yojana",$data_to_insert);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	public function StoreAboutDayData($data_to_store){
		$res = $this->db->insert("tbl_about_the_day",$data_to_store);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	public function StoredmqData($data_to_insert){
		$res = $this->db->insert("tbl_daily_main_question",$data_to_insert);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	public function StorerightsidebarData($data_to_insert){
		$res = $this->db->insert("tbl_rightsidebar",$data_to_insert);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	public function Addfeaturedimage($data_to_store){
		$res = $this->db->insert("tbl_featured_image",$data_to_store);
		return $res;
	}
	public function AddAttachment($data_to_store){
		$res = $this->db->insert("tbl_attachment",$data_to_store);
		return $res;
	}



	public function CheckFImageExistence($finame){
		$res = $this->db->select("*")->from("tbl_featured_image")->where('f_i_name',$finame);
		$res = $this->db->get()->result_array();
		return $res;
	}
	public function CheckFImageExistenceFI_id($f_i_id){
		$res = $this->db->select("*")->from("tbl_featured_image")->where('f_i_id',$f_i_id);
		$res = $this->db->get('')->result_array();
		return $res;
	}

	public function CheckAttachmentExistence($filename){
		$res = $this->db->select("*")->from("tbl_attachment")->where('a_name',$filename);
		$res = $this->db->get()->result_array();
		return $res;	
	}

	public function getDCADetails($dca_date){
		$res = $this->db->select("*")->from("tbl_dca")->where('dca_date',$dca_date);
		$res = $this->db->get()->result_array();
		return $res;
	}
	public function getrightsidebarData($rightsidebar_date,$rightsidebar_component){
		$res = $this->db->select("*")->from("tbl_rightsidebar")->where('rightsidebar_date',$rightsidebar_date)->where('rightsidebar_component',$rightsidebar_component);
		$res = $this->db->get()->result_array();
		return $res;	
	}


	public function getdpq($dpq_date){
		$res = $this->db->select("*")->from("tbl_daily_prelims_question")->where('dpq_date',$dpq_date);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getdmq($dmq_date){
		$res = $this->db->select("*")->from("tbl_daily_main_question")->where('dmq_date',$dmq_date);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getNPADetails($npa_date){
		$res = $this->db->select("*")->from("tbl_news_paper_analysis")->where('npa_date',$npa_date);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getkurukshetraDetails($kurukshetra_date){
		$res = $this->db->select("*")->from("tbl_kurukshetra")->where('kurukshetra_date',$kurukshetra_date);
		$res = $this->db->get()->result_array();
		return $res;	
	}




	public function getyojDetails($yoj_date){
		$res = $this->db->select("*")->from("tbl_yojana")->where('yoj_date',$yoj_date);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getpypqDetails($pypq_date){
		$res = $this->db->select("*")->from("tbl_previous_year_prelim_question")->where('pypq_date',$pypq_date);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getpymqDetails($pymq_date){
		$res = $this->db->select("*")->from("tbl_previous_year_main_question")->where('pymq_date',$pymq_date);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getAboutDay($aboutday_date){
		$res = $this->db->select("*")->from("tbl_about_the_day")->where('aboutday_date',$aboutday_date);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getAboutDayByAboutDayId($aboutDayId){
		$res = $this->db->select("*")->from("tbl_about_the_day")->where('aboutday_id',$aboutDayId);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getpypqDetailsBypypq_ID($pypq_id){
		$res = $this->db->select("*")->from("tbl_previous_year_prelim_question")->where('pypq_id',$pypq_id);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getpymqDetailsBypymq_ID($pymq_id){
		$res = $this->db->select("*")->from("tbl_previous_year_main_question")->where('pymq_id',$pymq_id);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function UpdateAboutDayDetailsByAboutDayId($data_to_update,$aboutDayId){
		$this->db->where('aboutday_id',$aboutDayId);
		$this->db->update("tbl_about_the_day",$data_to_update);
		return true;
	}

	public function UpdatepypqDetailsBypypq_ID($data_to_update,$pypq_id){
		$this->db->where('pypq_id',$pypq_id);
		$this->db->update("tbl_previous_year_prelim_question",$data_to_update);
		return true;
	}

	public function UpdatepymqDetailsBypymq_ID($data_to_update,$pymq_id){
		$this->db->where('pymq_id',$pymq_id);
		$this->db->update("tbl_previous_year_main_question",$data_to_update);
		return true;
	}

	public function getDCADetailsByDCA_ID($dca_id){
		$res = $this->db->select("*")->from("tbl_dca")->where('dca_id',$dca_id);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getkurukshetraDetailsBykurukshetra_ID($kurukshetra_id){
		$res = $this->db->select("*")->from("tbl_kurukshetra")->where('kurukshetra_id',$kurukshetra_id);
		$res = $this->db->get()->result_array();
		return $res;
	}


	public function getyojDetailsByyoj_ID($yoj_id){
		$res = $this->db->select("*")->from("tbl_yojana")->where('yoj_id',$yoj_id);
		$res = $this->db->get()->result_array();
		return $res;
	}


	public function getrightsidebarByrightsidebarId($rightsidebar_id){
		$res = $this->db->select("*")->from("tbl_rightsidebar")->where('rightsidebar_id',$rightsidebar_id);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function UpdaterightsidebarDetailsByrightsidebarId($data_to_update,$rightsidebarId){
		$this->db->where('rightsidebar_id',$rightsidebarId);
		$this->db->update("tbl_rightsidebar",$data_to_update);
		return true;
	}

	public function UpdateyojDetailsByyoj_ID($data_to_update,$yoj_id){
		$this->db->where('yoj_id',$yoj_id);
		$this->db->update("tbl_yojana",$data_to_update);
		return true;
	}

	public function UpdatekurukshetraDetailsBykurukshetra_ID($data_to_update,$kurukshetra_id){
		$this->db->where('kurukshetra_id',$kurukshetra_id);
		$this->db->update("tbl_kurukshetra",$data_to_update);
		return true;
	}


	public function getdpqDetailsBydpq_ID($dpq_id){
		$res = $this->db->select("*")->from("tbl_daily_prelims_question")->where('dpq_id',$dpq_id);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getdmqDetailsBydmq_ID($dmq_id){
		$res = $this->db->select("*")->from("tbl_daily_main_question")->where('dmq_id',$dmq_id);
		$res = $this->db->get()->result_array();
		return $res;
	}


	public function getNPADetailsByNPA_ID($npa_id){
		$res = $this->db->select("*")->from("tbl_news_paper_analysis")->where('npa_id',$npa_id);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function UpdateDCADetailsByDCA_ID($data_to_update,$dca_id){
		$this->db->where('dca_id',$dca_id);
		$this->db->update("tbl_dca",$data_to_update);
		return true;
	}
	public function UpdatedpqDetailsBydpq_ID($data_to_update,$dpq_id){
		$this->db->where('dpq_id',$dpq_id);
		$this->db->update("tbl_daily_prelims_question",$data_to_update);
		return true;
	}

	public function UpdatedmqDetailsBydmq_ID($data_to_update,$dmq_id){
		$this->db->where('dmq_id',$dmq_id);
		$this->db->update("tbl_daily_main_question",$data_to_update);
		return true;	
	}


	public function UpdateNPADetailsBynpa_ID($data_to_update,$npa_id){
		$this->db->where('npa_id',$npa_id);
		$this->db->update("tbl_news_paper_analysis",$data_to_update);
		return true;
	}

	

	public function getDCACount() {
		$res = $this->db->count_all_results('tbl_dca');
		return $res;
	}


	public function getFeaturedImagesList(){
		$temp = $this->db->get("tbl_featured_image")->result_array();
		return $temp;
	}

	public function getAttachmentList(){
		$temp = $this->db->get("tbl_attachment")->result_array();
		return $temp;	
	}


	public function getUsersList(){
		$temp = $this->db->get("tbl_registration")->result_array();
		return $temp;
	}
	public function getSubscriberList(){
		$temp = $this->db->get("tbl_subscriber")->result_array();
		return $temp;
	}

	public function DelDCA($dca_id){
		$this->db->where('dca_id',$dca_id);
		$this->db->delete("tbl_dca");
		return true;
	}


}
?>
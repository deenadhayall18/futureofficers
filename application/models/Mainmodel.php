<?php 
class Mainmodel extends CI_Model{


	public function checkUserExists($useremail){
		$data['useremail'] = $useremail;
		$res = $this->db->select("*")->from("tbl_registration")->where($data);
		$res = $this->db->get()->result_array();
		return $res;
	}


	public function UserSignUp($signup){
		$res = $this->db->insert("tbl_registration",$signup);
		return $res;
	}

	public function UserSignIn($signin){
		$res = $this->db->select("*")->from("tbl_registration")->where($signin);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function AddSubscription($signup){
		$res = $this->db->insert("tbl_subscriber",$signup);
		return $res;
	}

	public function checkEmailSubscription($signup){
		$res = $this->db->select("*")->from("tbl_subscriber")->where($signup);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getDCAList() {
		$res = $this->db->order_by("dca_date desc")->limit(4)->get('tbl_dca')->result_array();
		return $res;
	}
	public function getDCAListByMonth($month,$year,$lang){
		$query = 'SELECT * from tbl_dca where YEAR(dca_date) ='.$year.' AND dca_lang = '.$lang.' order by dca_date desc LIMIT 30';
		$res = $this->db->query($query)->result_array();
		// echo $this->db->last_query();
		// die;
		return $res;
	}

	public function getyojanaListByMonth($month,$year){
		// $query = 'SELECT * from tbl_yojana where MONTH(yoj_date)='.$month.' and YEAR(yoj_date) ='.$year.' order by yoj_date desc LIMIT 30';
		$query = 'SELECT * from tbl_yojana where YEAR(yoj_date) ='.$year.' order by yoj_date desc LIMIT 30';
		$res = $this->db->query($query)->result_array();
		return $res;	
	}

	public function getpypqListByMonth($month,$year){
		$query = 'SELECT * from tbl_previous_year_prelim_question where YEAR(pypq_date) ='.$year.' order by pypq_date desc LIMIT 30';
		$res = $this->db->query($query)->result_array();
		return $res;
	}


	public function getkurukshetraListByMonth($month,$year){
		$query = 'SELECT * from tbl_kurukshetra where YEAR(kurukshetra_date) ='.$year.' order by kurukshetra_date desc LIMIT 30';
		$res = $this->db->query($query)->result_array();
		return $res;	
	}

	public function getABboutDayListByMonth($month,$year){
		$query = 'SELECT * from tbl_about_the_day where YEAR(aboutday_date) ='.$year.' order by aboutday_date desc LIMIT 30';
		$res = $this->db->query($query)->result_array();
		return $res;
	}
	


	public function getdailyUPSCPRelimsQuesListByMonth($month,$year){
		$query = 'SELECT * from tbl_daily_prelims_question where YEAR(dpq_date) ='.$year.' order by dpq_date desc LIMIT 30';
		$res = $this->db->query($query)->result_array();
		return $res;	
	}

	//calendar
	public function getdpqListByCalendarDate($dpq_cal_date){
		$res = $this->db->select("*")->from("tbl_daily_prelims_question")->where('dpq_date',$dpq_cal_date)->order_by('dpq_date','desc')->limit('1')->get()->result_array();
		return $res;	
	}

	public function getdcaListByCalendarDate($cal_date){
		$res = $this->db->select("*")->from("tbl_dca")->where('dca_date',$cal_date)->order_by('dca_date','desc')->limit('1')->get()->result_array();
		return $res;	
	}

	public function getnpaListByCalendarDate($cal_date){
		$res = $this->db->select("*")->from("tbl_news_paper_analysis")->where('npa_date',$cal_date)->order_by('npa_date','desc')->limit('1')->get()->result_array();
		return $res;
	}


	public function getdmqqListByCalendarDate($cal_date){
		$res = $this->db->select("*")->from("tbl_daily_main_question")->where('dmq_date',$cal_date)->order_by('dmq_date','desc')->limit('1')->get()->result_array();
		return $res;
	}

	public function getpypqListByCalendarDate($cal_date){
		$res = $this->db->select("*")->from("tbl_previous_year_prelim_question")->where('pypq_date',$cal_date)->order_by('pypq_date','desc')->limit('1')->get()->result_array();
		return $res;
	}

	public function getpymqListByCalendarDate($cal_date){
		$res = $this->db->select("*")->from("tbl_previous_year_main_question")->where('pymq_date',$cal_date)->order_by('pymq_date','desc')->limit('1')->get()->result_array();
		return $res;
	}

	public function getyojanaListByCalendarDate($cal_date){
		$res = $this->db->select("*")->from("tbl_yojana")->where('yoj_date',$cal_date)->order_by('yoj_date','desc')->limit('1')->get()->result_array();
		return $res;
	}

	public function getkurukshetraListByCalendarDate($cal_date){
		$res = $this->db->select("*")->from("tbl_kurukshetra")->where('kurukshetra_date',$cal_date)->order_by('kurukshetra_date','desc')->limit('1')->get()->result_array();
		return $res;
	}
	public function getAboutDayListByCalendarDate($cal_date){
		$res = $this->db->select("*")->from("tbl_about_the_day")->where('aboutday_date',$cal_date)->order_by('aboutday_date','desc')->limit('1')->get()->result_array();
		return $res;
	}

	public function getdailyUPSCMainQuesListByMonth($month,$year){
		$query = 'SELECT * from tbl_daily_main_question where YEAR(dmq_date) ='.$year.' order by dmq_date desc LIMIT 30';
		$res = $this->db->query($query)->result_array();
		return $res;	
	}

	public function getpymqListByMonth($month,$year){
		$query = 'SELECT * from tbl_previous_year_main_question where YEAR(pymq_date) ='.$year.' order by pymq_date desc LIMIT 30';
		$res = $this->db->query($query)->result_array();
		return $res;
	}


	public function getNPAListByMonth($month,$year){
		$query = 'SELECT * from tbl_news_paper_analysis where YEAR(npa_date) ='.$year.' order by npa_date desc LIMIT 30';
		$res = $this->db->query($query)->result_array();
		return $res;
	}

	public function getAboutDayList() {
		$res = $this->db->order_by("aboutday_id desc")->limit(4)->get('tbl_about_the_day')->result_array();
		return $res;
	}
	public function getNPAList() {
		$res = $this->db->order_by("npa_id desc")->limit(4)->get('tbl_news_paper_analysis')->result_array();
		return $res;
	}
	public function getRightSideBarListByComponent($component){
		$res = $this->db->where('rightsidebar_component',$component)->order_by("rightsidebar_id desc")->limit(4)->get('tbl_rightsidebar')->result_array();
		return $res;
	}



	public function getAboutDayListByID($aboutday_id){
		$res = $this->db->select("*")->from("tbl_about_the_day")->where('aboutday_id',$aboutday_id);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getyojDetailsByyoj_ID($yojId){
		$res = $this->db->select("*")->from("tbl_yojana")->where('yoj_id',$yojId);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getkurukshetraDetailsBykurukshetra_ID($kurukshetraId){
		$res = $this->db->select("*")->from("tbl_kurukshetra")->where('kurukshetra_id',$kurukshetraId);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getpypqDetailsBypypq_ID($pypqId){
		$res = $this->db->select("*")->from("tbl_previous_year_prelim_question")->where('pypq_id',$pypqId);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getdpqListByID($dpq_id){
		$res = $this->db->select("*")->from("tbl_daily_prelims_question")->where('dpq_id',$dpq_id);
		$res = $this->db->get()->result_array();
		return $res;	
	}

	public function getdmqListByID($dmq_id){
		$res = $this->db->select("*")->from("tbl_daily_main_question")->where('dmq_id',$dmq_id);
		$res = $this->db->get()->result_array();
		return $res;	
	}

	public function getrightSideBarInnerPageDataByID($rightsidebar_id){
		$res = $this->db->select("*")->from("tbl_rightsidebar")->where('rightsidebar_id',$rightsidebar_id);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getFeaturedImagesList(){
		$temp = $this->db->get("tbl_featured_image")->result_array();
		return $temp;
	}
	public function getAttachmentList(){
		$temp = $this->db->get("tbl_attachment")->result_array();
		return $temp;	
	}








	public function StoreDCAData($data_to_store){
		$res = $this->db->insert("tbl_dca",$data_to_store);
		return $res;
	}
	public function Addfeaturedimage($data_to_store){
		$res = $this->db->insert("tbl_featured_image",$data_to_store);
		return $res;
	}
	public function AddAttachment($data_to_store){
		$res = $this->db->insert("tbl_attachment",$data_to_store);
		return $res;
	}



	public function CheckFImageExistence($finame){
		$res = $this->db->select("*")->from("tbl_featured_image")->where('f_i_name',$finame);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function CheckAttachmentExistence($filename){
		$res = $this->db->select("*")->from("tbl_attachment")->where('a_name',$filename);
		$res = $this->db->get()->result_array();
		return $res;	
	}

	public function getDCADetails($dca_date){
		$res = $this->db->select("*")->from("tbl_dca")->where('dca_date',$dca_date);
		$res = $this->db->get()->result_array();
		return $res;
	}
	public function getDCADetailsByDCA_ID($dca_id){
		$res = $this->db->select("*")->from("tbl_dca")->where('dca_id',$dca_id);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getNPADetailsByNPA_ID($npa_id){
		$res = $this->db->select("*")->from("tbl_news_paper_analysis")->where('npa_id',$npa_id);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getpymqDetailsBypymq_ID($pymqId){
		$res = $this->db->select("*")->from("tbl_previous_year_main_question")->where('pymq_id',$pymqId);
		$res = $this->db->get()->result_array();
		return $res;
	}




	public function UpdateDCADetailsByDCA_ID($data_to_update,$dca_id){
		$this->db->where('dca_id',$dca_id);
		$this->db->update("tbl_dca",$data_to_update);
		return true;
	}






	public function getUsersList(){
		$temp = $this->db->get("tbl_users")->result_array();
		return $temp;
	}


	public function DelDCA($dca_id){
		$this->db->where('dca_id',$dca_id);
		$this->db->delete("tbl_dca");
		return true;
	}



}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'MainController/HomepageMethod';
$route['404_override'] = 'MainController/notfound';
$route['translate_uri_dashes'] = FALSE;

$route['common']  ='MainController/commonMethod';
$route['header']  ='MainController/HeaderMethod';

$route['homepage']  ='MainController/HomepageMethod';
$route['dailyCurrentAffairs']  ='MainController/DailyCurrentAffairs';
$route['dailyCurrentAffairsList']  ='MainController/DailyCurrentAffairsListMethod';

// News Paper Analysis
$route['newsPaperAnalysis']  ='MainController/newsPaperAnalysis';
$route['newsPaperAnalysisList']  ='MainController/newsPaperAnalysisListMethod';

//Daily UPSC Preliminary Questions
$route['dailyUPSCPrelimsQuestions']  ='MainController/dailyUPSCPrelimsQuestions';
$route['dailyUPSCPrelimsQuestionsPage']  ='MainController/dailyUPSCPrelimsQuestionsPage';

//Daily UPSC Mains questions
$route['dailyUPSCMainQuestions']  ='MainController/dailyUPSCMainQuestions';
$route['dailyUPSCMainQuestionsPage']  ='MainController/dailyUPSCMainQuestionsPage';

//yojana
$route['yojana']  ='MainController/yojana';
$route['yojanaList']  ='MainController/yojanaListMethod';


//Previous year prelims question
$route['preYearPrelimQues']  ='MainController/preYearPrelimQues';
$route['preYearPrelimQuesList']  ='MainController/preYearPrelimQuesListMethod';


//Previous year main question
$route['preYearMainQues']  ='MainController/preYearMainQues';
$route['preYearMainQuesList']  ='MainController/preYearMainQuesListMethod';

// Kurukshetra
$route['kurukshetra']  ='MainController/kurukshetra';
$route['kurukshetraList']  ='MainController/kurukshetraList';

$route['rightsidebarPages']  ='MainController/rightsidebarPages';
$route['rightsidebarInnerPages']  ='MainController/rightsidebarInnerPages';

$route['aboutTheDay']  ='MainController/aboutTheDay';
$route['aboutTheDayList'] = 'MainController/aboutTheDayList';

$route['CalendarDateDataNotFound'] = 'MainController/CalendarDateDataNotFound';


// ****************************************************************************************************


//AdminPanel Routes Files  - Start
$route['admin/login']  ='AdminCon/index';
$route['admin/common']  ='AdminCon/AdminDashboard';
$route['admin/logout']  ='AdminCon/logout';
$route['admin/dashboard']  ='AdminCon/AdminDashboardMain';
$route['admin/usersList'] = 'AdminCon/usersList';
$route['admin/subscribersList'] = 'AdminCon/subscribersList';

//Daily Current Affairs
$route['admin/addDailyCurrentAffairs'] = 'AdminCon/addDailyCurrentAffairs';
$route['admin/editDailyCurrentAffairs'] = 'AdminCon/editDailyCurrentAffairs';
$route['admin/dailyCurrentAffairsCategoryList'] = 'AdminCon/DailyCurrentAffairsCategoryList';

$route['admin/edit-log'] = 'AdminCon/EditLog';
$route['admin/AddTask'] = 'AdminCon/AddTaskMethod';

//Featured Image
$route['admin/addFeaturedImage'] = 'AdminCon/addFeaturedImage';
$route['admin/editFeaturedImage'] = 'AdminCon/editFeaturedImage';
$route['admin/featuredImageList'] = 'AdminCon/FeaturedImageList';

//attachments
$route['admin/addAttachment'] = 'AdminCon/addAttachment';
// $route['admin/editFeaturedImage'] = 'AdminCon/editFeaturedImage';
$route['admin/attachmentList'] = 'AdminCon/attachmentList';

//About the day
$route['admin/addAboutTheDay'] = 'AdminCon/addAboutTheDay';
$route['admin/editAboutTheDay'] = 'AdminCon/editAboutTheDay';
$route['admin/aboutTheDayCategoryList'] = 'AdminCon/aboutTheDayCategoryList';

//news paper analysis
$route['admin/addnewsPaperAnalysis'] = 'AdminCon/addnewsPaperAnalysis';
$route['admin/editnewsPaperAnalysis'] = 'AdminCon/editnewsPaperAnalysis';
$route['admin/newsPaperAnalysisList'] = 'AdminCon/newsPaperAnalysisList';

//UPSC Daily Prelim Questions
$route['admin/addUPSCPrelimQuestions'] = 'AdminCon/addUPSCPrelimQuestions';
$route['admin/editUPSCPrelimQuestions'] = 'AdminCon/editUPSCPrelimQuestions';
$route['admin/addUPSCPrelimQuestionsList'] = 'AdminCon/addUPSCPrelimQuestionsList';

//UPSC Daily Main Questions
$route['admin/addUPSCDailyQuestions'] = 'AdminCon/addUPSCDailyQuestions';
$route['admin/editUPSCDailyQuestions'] = 'AdminCon/editUPSCDailyQuestions';
$route['admin/addUPSCDailyQuestionsList'] = 'AdminCon/addUPSCDailyQuestionsList';


//Previous year Prelim Questions
$route['admin/addPreviousYearPrelimQuestions'] = 'AdminCon/addPreviousYearPrelimQuestions';
$route['admin/editPreviousYearPrelimQuestions'] = 'AdminCon/editPreviousYearPrelimQuestions';
$route['admin/previousYearPrelimQuestionsList'] = 'AdminCon/previousYearPrelimQuestionsList';


//Previous year Prelim Questions
$route['admin/addPreviousYearMainQuestions'] = 'AdminCon/addPreviousYearMainQuestions';
$route['admin/editPreviousYearMainQuestions'] = 'AdminCon/editPreviousYearMainQuestions';
$route['admin/previousYearMainQuestionsList'] = 'AdminCon/previousYearMainQuestionsList';

//Yojana
$route['admin/addyojana'] = 'AdminCon/addyojana';
$route['admin/edityojana'] = 'AdminCon/edityojana';
$route['admin/yojanaList'] = 'AdminCon/yojanaList';


//Kurukshetra
$route['admin/addKurukshetra'] = 'AdminCon/addKurukshetra';
$route['admin/editKurukshetra'] = 'AdminCon/editKurukshetra';
$route['admin/kurukshetraList'] = 'AdminCon/kurukshetraList';


// Add Previous Year Preliminary Question
$route['admin/addPreviousYearPrelimsQues'] = 'AdminCon/addPreviousYearPrelimsQues';
$route['admin/previousYearPrelimsQuesCategoryList'] = 'AdminCon/previousYearPrelimsQuesCategoryList';
$route['admin/addDailyMainQues'] = 'AdminCon/addDailyMainQues';
$route['admin/addGovnSchemes'] = 'AdminCon/addGovnSchemes';
$route['admin/addHinduAnalysis'] = 'AdminCon/addHinduAnalysis';
$route['admin/addStaticGK'] = 'AdminCon/addStaticGK';
$route['admin/subscribersList'] = 'AdminCon/subscribersList';

//Right Side Bar
$route['admin/addRightBarComponent'] = 'AdminCon/addRightBarComponent';
$route['admin/editRightBarComponent'] = 'AdminCon/editRightBarComponent';
$route['admin/rightBarComponentList'] = 'AdminCon/rightBarComponentListMethod';

$route['admin/email'] = 'AdminCon/email';

//AdminPanel Routes Files  - End

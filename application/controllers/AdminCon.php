<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admincon extends CI_Controller{
	public function __construct(){
		parent :: __construct();
		$this->load->model("Admin_model");
		$this->load->helper(array('form', 'url','security')); 
		$this->load->library(array("form_validation",'session','upload','pagination'));
	}
	public function index(){
		if($this->input->post("submit")){
			$this->form_validation->set_rules('username','Username','required|alpha');
			$this->form_validation->set_rules("password",'Password','required|min_length[8]|max_length[8]');
			if($this->form_validation->run() == false){
				$logindata['username'] = $this->input->post("username");
				$logindata['password'] = $this->input->post("password");
				$success = $this->Admin_model->checkUserExists($logindata);
				if($success){
					$this->session->set_userdata("user",$this->input->post("username"));
					$this->session->set_userdata("sess_auth",md5($logindata['username']));
					redirect("admin/dashboard");
				}else{
					$this->session->set_flashdata("ErrMsg","Invalid Login Credentials");
					redirect("admin/login");
				}
			}else{
				redirect("admin/login");
			}
		}
		$this->load->view("admin/login");
	}

	public function AdminDashboard(){
		$this->load->view("admin/common");
	}
	public function AdminDashboardMain(){
		$data['url'] = 'admin/dashboard';
		$user = $this->session->userdata("user");
		$data['dbDCACount'] = $this->Admin_model->getDCACount();

		$data['UsersListCount'] = (!empty($this->Admin_model->getUsersList())?count($this->Admin_model->getUsersList()):'0');
		$data['SubscriberListCount'] = (!empty($this->Admin_model->getSubscriberList())?count($this->Admin_model->getSubscriberList()):'0');

		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}
	public function usersList(){
		$data['url'] = 'admin/usersList';
		$data['dbUserData'] = $this->Admin_model->getUsersList();
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function subscribersList(){
		$data['url'] = 'admin/subscribersList';
		$data['dbSubUserData'] = $this->Admin_model->getSubscriberList();
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}

	public function FeaturedImageList(){
		$data['url'] = 'admin/featuredImageList';
		$data['dbFIList'] = $this->Admin_model->getFeaturedImagesList();
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}

	public function attachmentList(){
		$data['url'] = 'admin/attachmentList';
		$data['dbAttachmentList'] = $this->Admin_model->getAttachmentList();
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function addDailyCurrentAffairs(){
		$data['url'] = 'admin/addDailyCurrentAffairs';

		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();

		if($this->input->post("submit")){
			
			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){

				$data_to_insert["dca_date"] = trim($this->input->post('dca_date'));
				$data_to_insert["dca_lang"] = trim($this->input->post('dca_lang'));
				$data_to_insert["dca_category"] = (json_encode($this->input->post('dca_category')));
				$heading = str_replace(" ","-",($this->input->post('dca_heading')));
				$data_to_insert["dca_heading"] = (json_encode($heading));
				$data_to_insert["dca_tags"] = (json_encode($this->input->post('dca_tags')));
				$data_to_insert["dca_content"] = json_encode($this->input->post('dca_content'));
				$upload_time = $data_to_insert['dca_upload_time'] = time();
				$data_to_insert["dca_featured_image"] = trim($this->input->post('dca_featured_image'));
				$data_to_insert["dca_attachments"] = trim($this->input->post('dca_attachments'));
				$data_to_insert["dca_read_time"] = trim($this->input->post('dca_read_time'));
				$data_to_insert["dca_trigger_email"] = (($this->input->post('dca_trigger_email')=='on')?'yes':'no');    
				$dbDcaStored = $this->Admin_model->StoreDCAData($data_to_insert);
				if($dbDcaStored){
					$previewArr = array('id'=>$dbDcaStored,'heading'=>$heading);
					$this->session->set_flashdata("previewContent",$previewArr);
					$this->session->set_flashdata("Succ",'Your Article Stored Successfully! ..');
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function addnewsPaperAnalysis(){
		$data['url'] = 'admin/addnewsPaperAnalysis';

		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();

		if($this->input->post("submit")){

			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){


				$data_to_insert["npa_date"] = trim($this->input->post('npa_date'));
				$data_to_insert["npa_lang"] = trim($this->input->post('npa_lang'));
				$data_to_insert["npa_category"] = (json_encode($this->input->post('npa_category')));
				$heading = str_replace(" ","-",($this->input->post('npa_heading')));
				$data_to_insert["npa_heading"] = (json_encode($heading));
				$data_to_insert["npa_tags"] = (json_encode($this->input->post('npa_tags')));
				$data_to_insert["npa_content"] = json_encode($this->input->post('npa_content'));
				$upload_time = $data_to_insert['npa_upload_time'] = time();
				$data_to_insert["npa_featured_image"] = trim($this->input->post('npa_featured_image'));
				$data_to_insert["npa_attachments"] = trim($this->input->post('npa_attachments'));
				$data_to_insert["npa_read_time"] = trim($this->input->post('npa_read_time'));
				$data_to_insert["npa_trigger_email"] = (($this->input->post('npa_trigger_email')=='on')?'yes':'no');    
				$dbDcaStored = $this->Admin_model->StoreNPAData($data_to_insert);

				if($dbDcaStored){
					$previewArr = array('id'=>$dbDcaStored,'heading'=>$heading);
					$this->session->set_flashdata("previewContent",$previewArr);
					$this->session->set_flashdata("Succ",'Your Article Stored Successfully! ..');
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function addKurukshetra(){
		$data['url'] = 'admin/addKurukshetra';

		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();

		if($this->input->post("submit")){

			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){


				$data_to_insert["kurukshetra_date"] = trim($this->input->post('kurukshetra_date'));
				$data_to_insert["kurukshetra_lang"] = trim($this->input->post('kurukshetra_lang'));
				$data_to_insert["kurukshetra_category"] = (json_encode($this->input->post('kurukshetra_category')));
				$heading = str_replace(" ","-",($this->input->post('kurukshetra_heading')));
				$data_to_insert["kurukshetra_heading"] = (json_encode($heading));
				$data_to_insert["kurukshetra_tags"] = (json_encode($this->input->post('kurukshetra_tags')));
				$data_to_insert["kurukshetra_content"] = json_encode($this->input->post('kurukshetra_content'));
				$upload_time = $data_to_insert['kurukshetra_upload_time'] = time();
				$data_to_insert["kurukshetra_featured_image"] = trim($this->input->post('kurukshetra_featured_image'));
				$data_to_insert["kurukshetra_attachments"] = trim($this->input->post('kurukshetra_attachments'));
				$data_to_insert["kurukshetra_read_time"] = trim($this->input->post('kurukshetra_read_time'));
				$dbKurukshetraStored = $this->Admin_model->StorekurukshetraData($data_to_insert);

				if($dbKurukshetraStored){
					$this->session->set_flashdata("Succ",'Your Article Stored Successfully! ..');
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function addUPSCPrelimQuestions(){
		$data['url'] = 'admin/addUPSCPrelimQuestions';

		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();

		if($this->input->post("submit")){

			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){

				$data_to_insert["dpq_date"] = trim($this->input->post('dpq_date'));
				$data_to_insert["dpq_lang"] = trim($this->input->post('dpq_lang'));
				$data_to_insert["dpq_category"] = (json_encode($this->input->post('dpq_category')));
				$heading = str_replace(" ","-",($this->input->post('dpq_heading')));
				$data_to_insert["dpq_heading"] = (json_encode($heading));
				$data_to_insert["dpq_tags"] = (json_encode($this->input->post('dpq_tags')));
				$data_to_insert["dpq_content"] = json_encode($this->input->post('dpq_content'));
				$upload_time = $data_to_insert['dpq_upload_time'] = time();
				$data_to_insert["dpq_featured_image"] = trim($this->input->post('dpq_featured_image'));
				$data_to_insert["dpq_attachments"] = trim($this->input->post('dpq_attachments'));
				$data_to_insert["dpq_read_time"] = trim($this->input->post('dpq_read_time'));
				$data_to_insert["dpq_trigger_email"] = (($this->input->post('dpq_trigger_email')=='on')?'yes':'no');    

				$dbDPQStored = $this->Admin_model->StoreDPQData($data_to_insert);

				if($dbDPQStored){
					$previewArr = array('id'=>$dbDPQStored,'heading'=>$heading);
					$this->session->set_flashdata("previewContent",$previewArr);
					$this->session->set_flashdata("Succ",'Your Article Stored Successfully! ..');
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}





	public function editDailyCurrentAffairs(){
		$data['url'] = 'admin/editDailyCurrentAffairs';

		$dca_id = $this->input->get('dca_id');
		if(empty($dca_id)){
			redirect('admin/dailyCurrentAffairsCategoryList');
		}
		$dca_id = $this->input->get('dca_id');
		$data['dcaDbData'] = $this->Admin_model->getDCADetailsByDCA_ID($dca_id);
		if(empty($data['dcaDbData'])){
			$this->session->set_flashdata('Succ','Data Not Available !');
			redirect('admin/dailyCurrentAffairsCategoryList');
		}
		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();

		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		if($this->input->post("submit")){
			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){

				$data_to_update["dca_category"] = json_encode($this->input->post('dca_category'));
				$heading = str_replace(" ","-",($this->input->post('dca_heading')));
				$data_to_update["dca_heading"] = (json_encode($heading));
				$data_to_update["dca_tags"] = json_encode($this->input->post('dca_tags'));
				$data_to_update["dca_content"] = json_encode($this->input->post('dca_content'));
				$data_to_update["dca_lang"] = trim($this->input->post('dca_lang'));
				if($this->input->post('dca_date')>=date("Y-m-d")){
					$data_to_update['dca_upload_time'] = strtotime($this->input->post('dca_date'));
				}else{
					$data_to_update['dca_upload_time'] = strtotime(date('Y-m-d'));
				}
				$upload_time = $data_to_update['dca_upload_time'] = time();
				$data_to_update["dca_featured_image"] = $this->input->post('dca_featured_image');
				$data_to_update["dca_read_time"] = trim($this->input->post('dca_read_time'));
				$data_to_update["dca_attachments"] = $this->input->post('dca_attachments');
				$data_to_update["dca_trigger_email"] = (($this->input->post('dca_trigger_email')=='on')?'yes':'no');    

				$dbDcaUpdated = $this->Admin_model->UpdateDCADetailsByDCA_ID($data_to_update,$dca_id);

				if($dbDcaUpdated){
					$this->session->set_flashdata("Succ",'Your Article Updated Successfully! ..');
					redirect('admin/editDailyCurrentAffairs?dca_id='.$dca_id);
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}





	public function editUPSCPrelimQuestions(){
		$data['url'] = 'admin/editUPSCPrelimQuestions';

		$dpq_id = $this->input->get('dpq_id');
		if(empty($dpq_id)){
			redirect('admin/addUPSCPrelimQuestionsList');
		}
		$dpq_id = $this->input->get('dpq_id');
		$data['dpqDbData'] = $this->Admin_model->getdpqDetailsBydpq_ID($dpq_id);
		if(empty($data['dpqDbData'])){
			redirect('admin/addUPSCPrelimQuestionsList');
		}
		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();
		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		if($this->input->post("submit")){
			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){
				$heading = str_replace(" ","-",($this->input->post('dpq_heading')));
				$data_to_update["dpq_heading"] = (json_encode($heading));
				$data_to_update["dpq_category"] = json_encode($this->input->post('dpq_category'));
				$data_to_update["dpq_lang"] = ($this->input->post('dpq_lang'));
				$data_to_update["dpq_tags"] = json_encode($this->input->post('dpq_tags'));
				$data_to_update["dpq_content"] = json_encode($this->input->post('dpq_content'));
				if($this->input->post('dpq_date')>=date("Y-m-d")){
					$data_to_update['dpq_upload_time'] = strtotime($this->input->post('dpq_date'));
				}else{
					$data_to_update['dpq_upload_time'] = strtotime(date('Y-m-d'));
				}
				$upload_time = $data_to_update['dpq_upload_time'] = time();
				$data_to_update["dpq_featured_image"] = $this->input->post('dpq_featured_image');
				$data_to_update["dpq_read_time"] = trim($this->input->post('dpq_read_time'));
				$data_to_update["dpq_attachments"] = $this->input->post('dpq_attachments');
				$data_to_update["dpq_trigger_email"] = (($this->input->post('dpq_trigger_email')=='on')?'yes':'no');    

				$dbDPQUpdated = $this->Admin_model->UpdatedpqDetailsBydpq_ID($data_to_update,$dpq_id);
				if($dbDPQUpdated){
					$this->session->set_flashdata("Succ",'Your Article Updated Successfully! ..');
					redirect('admin/editUPSCPrelimQuestions?dpq_id='.$dpq_id);
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}

	public function editUPSCDailyQuestions(){
		$data['url'] = 'admin/editUPSCDailyQuestions';

		$dmq_id = $this->input->get('dmq_id');
		if(empty($dmq_id)){
			redirect('admin/addUPSCDailyQuestionsList');
		}
		$dmq_id = $this->input->get('dmq_id');
		$data['dmqDbData'] = $this->Admin_model->getdmqDetailsBydmq_ID($dmq_id);
		if(empty($data['dmqDbData'])){
			redirect('admin/addUPSCDailyQuestionsList');
		}
		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();
		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		if($this->input->post("submit")){
			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){
				$data_to_update["dmq_category"] = json_encode($this->input->post('dmq_category'));
				$data_to_update["dmq_tags"] = json_encode($this->input->post('dmq_tags'));
				$data_to_update["dmq_content"] = json_encode($this->input->post('dmq_content'));
				if($this->input->post('dmq_date')>=date("Y-m-d")){
					$data_to_update['dmq_upload_time'] = strtotime($this->input->post('dmq_date'));
				}else{
					$data_to_update['dmq_upload_time'] = strtotime(date('Y-m-d'));
				}
				$heading = str_replace(" ","-",($this->input->post('dmq_heading')));
				$data_to_update["dmq_heading"] = (json_encode($heading));
				$upload_time = $data_to_update['dmq_upload_time'] = time();
				$data_to_update["dmq_featured_image"] = $this->input->post('dmq_featured_image');
				$data_to_update["dmq_read_time"] = trim($this->input->post('dmq_read_time'));
				$data_to_update["dmq_lang"] = trim($this->input->post('dmq_lang'));
				$data_to_update["dmq_attachments"] = $this->input->post('dmq_attachments');
				$data_to_update["dmq_trigger_email"] = (($this->input->post('dmq_trigger_email')=='on')?'yes':'no');    

				$dbdmqUpdated = $this->Admin_model->UpdatedmqDetailsBydmq_ID($data_to_update,$dmq_id);

				if($dbdmqUpdated){
					$this->session->set_flashdata("Succ",'Your Article Updated Successfully! ..');
					redirect('admin/editUPSCDailyQuestions?dmq_id='.$dmq_id);
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}




	public function editnewsPaperAnalysis(){
		$data['url'] = 'admin/editnewsPaperAnalysis';

		$npa_id = $this->input->get('npa_id');
		if(empty($npa_id)){
			redirect('admin/newsPaperAnalysisList');
		}

		$npa_id = $this->input->get('npa_id');
		$data['npaDbData'] = $this->Admin_model->getNPADetailsByNPA_ID($npa_id);
		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();

		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		if($this->input->post("submit")){
			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){


				$heading = str_replace(" ","-",($this->input->post('npa_heading')));
				$data_to_update["npa_heading"] = (json_encode($heading));
				$data_to_update["npa_category"] = json_encode($this->input->post('npa_category'));
				$data_to_update["npa_lang"] = ($this->input->post('npa_lang'));
				$data_to_update["npa_tags"] = json_encode($this->input->post('npa_tags'));
				$data_to_update["npa_content"] = json_encode($this->input->post('npa_content'));

				if($this->input->post('npa_date')>=date("Y-m-d")){
					$data_to_update['npa_upload_time'] = strtotime($this->input->post('npa_date'));
				}else{
					$data_to_update['npa_upload_time'] = strtotime(date('Y-m-d'));
				}

				$upload_time = $data_to_update['npa_upload_time'] = time();
				$data_to_update["npa_featured_image"] = $this->input->post('npa_featured_image');
				$data_to_update["npa_read_time"] = trim($this->input->post('npa_read_time'));
				$data_to_update["npa_attachments"] = $this->input->post('npa_attachments');
				$data_to_update["npa_trigger_email"] = (($this->input->post('npa_trigger_email')=='on')?'yes':'no');    
				$dbnpaUpdated = $this->Admin_model->UpdateNPADetailsBynpa_ID($data_to_update,$npa_id);

				if($dbnpaUpdated){
					$this->session->set_flashdata("Succ",'Your Article Updated Successfully! ..');
					redirect('admin/editnewsPaperAnalysis?npa_id='.$npa_id);
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}



	public function editKurukshetra(){
		$data['url'] = 'admin/editKurukshetra';

		$kurukshetra_id = $this->input->get('kurukshetra_id');
		if(empty($kurukshetra_id)){
			redirect('admin/kurukshetraList');
		}

		$kurukshetra_id = $this->input->get('kurukshetra_id');
		$data['kurukshetraDbData'] = $this->Admin_model->getkurukshetraDetailsBykurukshetra_ID($kurukshetra_id);
		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();

		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		if($this->input->post("submit")){
			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){


				$heading = str_replace(" ","-",($this->input->post('kurukshetra_heading')));
				$data_to_update["kurukshetra_heading"] = (json_encode($heading));
				$data_to_update["kurukshetra_category"] = json_encode($this->input->post('kurukshetra_category'));
				$data_to_update["kurukshetra_lang"] = trim($this->input->post('kurukshetra_lang'));
				$data_to_update["kurukshetra_tags"] = json_encode($this->input->post('kurukshetra_tags'));
				$data_to_update["kurukshetra_content"] = json_encode($this->input->post('kurukshetra_content'));

				if($this->input->post('kurukshetra_date')>=date("Y-m-d")){
					$data_to_update['kurukshetra_upload_time'] = strtotime($this->input->post('kurukshetra_date'));
				}else{
					$data_to_update['kurukshetra_upload_time'] = strtotime(date('Y-m-d'));
				}

				$upload_time = $data_to_update['kurukshetra_upload_time'] = time();
				$data_to_update["kurukshetra_featured_image"] = $this->input->post('kurukshetra_featured_image');
				$data_to_update["kurukshetra_read_time"] = trim($this->input->post('kurukshetra_read_time'));
				$data_to_update["kurukshetra_attachments"] = $this->input->post('kurukshetra_attachments');
				$data_to_update["kurukshetra_trigger_email"] = (($this->input->post('kurukshetra_trigger_email')=='on')?'yes':'no');    
				$dbkurukshetraUpdated=$this->Admin_model->UpdatekurukshetraDetailsBykurukshetra_ID($data_to_update,$kurukshetra_id);

				if($dbkurukshetraUpdated){
					$this->session->set_flashdata("Succ",'Your Article Updated Successfully! ..');
					redirect('admin/editKurukshetra?kurukshetra_id='.$kurukshetra_id);
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function editYojana(){
		$data['url'] = 'admin/editYojana';

		$yoj_id = $this->input->get('yoj_id');
		if(empty($yoj_id)){
			redirect('admin/yojanaList');
		}

		$yoj_id = $this->input->get('yoj_id');
		$data['yojDbData'] = $this->Admin_model->getyojDetailsByyoj_ID($yoj_id);
		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();

		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		if($this->input->post("submit")){
			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){


				$heading = str_replace(" ","-",($this->input->post('yoj_heading')));
				$data_to_update["yoj_heading"] = (json_encode($heading));
				$data_to_update["yoj_category"] = json_encode($this->input->post('yoj_category'));
				$data_to_update["yoj_lang"] = trim($this->input->post('yoj_lang'));
				$data_to_update["yoj_tags"] = json_encode($this->input->post('yoj_tags'));
				$data_to_update["yoj_content"] = json_encode($this->input->post('yoj_content'));

				if($this->input->post('yoj_date')>=date("Y-m-d")){
					$data_to_update['yoj_upload_time'] = strtotime($this->input->post('yoj_date'));
				}else{
					$data_to_update['yoj_upload_time'] = strtotime(date('Y-m-d'));
				}

				$upload_time = $data_to_update['yoj_upload_time'] = time();
				$data_to_update["yoj_featured_image"] = $this->input->post('yoj_featured_image');
				$data_to_update["yoj_read_time"] = trim($this->input->post('yoj_read_time'));
				$data_to_update["yoj_attachments"] = $this->input->post('yoj_attachments');
				$data_to_update["yoj_trigger_email"] = (($this->input->post('yoj_trigger_email')=='on')?'yes':'no');    
				$dbyojUpdated = $this->Admin_model->UpdateyojDetailsByyoj_ID($data_to_update,$yoj_id);

				if($dbyojUpdated){
					$this->session->set_flashdata("Succ",'Your Article Updated Successfully! ..');
					redirect('admin/edityojana?yoj_id='.$yoj_id);
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function addAboutTheDay(){
		$data['url'] = 'admin/addAboutTheDay';
		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();

		if($this->input->post("submit")){

			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){

				$data_to_insert['aboutday_date'] = trim($this->input->post('aboutday_date'));
				$data_to_insert['aboutday_lang'] = trim($this->input->post('aboutday_lang'));
				$heading = str_replace(" ","-",($this->input->post('aboutday_heading')));
				$data_to_insert["aboutday_heading"] = (json_encode($heading));
				$data_to_insert['aboutday_category'] = json_encode($this->input->post('aboutday_category'));
				$data_to_insert['aboutday_featured_image'] = $this->input->post('aboutday_featured_image');
				$data_to_insert['aboutday_tags'] = json_encode($this->input->post('aboutday_tags'));
				$data_to_insert['aboutday_content'] = json_encode($this->input->post('aboutday_content'));
				$upload_time = $data_to_insert['aboutday_upload_time'] = time();
				$data_to_insert["aboutday_read_time"] = trim($this->input->post('aboutday_read_time'));
				$data_to_insert["aboutday_trigger_email"] = (($this->input->post('aboutday_trigger_email')=='on')?'yes':'no');    
				$StoreAboutDayData = $this->Admin_model->StoreAboutDayData($data_to_insert);
				if($StoreAboutDayData){
					$previewArr = array('id'=>$StoreAboutDayData,'heading'=>$heading);
					$this->session->set_flashdata("previewContent",$previewArr);
					$this->session->set_flashdata("Succ",'Your Article Stored Successfully! ..');
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function addUPSCDailyQuestions(){
		$data['url'] = 'admin/addUPSCDailyQuestions';
		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();

		if($this->input->post("submit")){

			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){
				$data_to_insert['dmq_date'] = $this->input->post('dmq_date');
				$data_to_insert['dmq_lang'] = $this->input->post('dmq_lang');
				$heading = str_replace(" ","-",($this->input->post('dmq_heading')));
				$data_to_insert["dmq_heading"] = (json_encode($heading));
				$data_to_insert['dmq_category'] = json_encode($this->input->post('dmq_category'));
				$data_to_insert['dmq_featured_image'] = $this->input->post('dmq_featured_image');
				$data_to_insert['dmq_tags'] = json_encode($this->input->post('dmq_tags'));
				$data_to_insert['dmq_content'] = json_encode($this->input->post('dmq_content'));
				$upload_time = $data_to_insert['dmq_upload_time'] = time();
				$data_to_insert["dmq_read_time"] = trim($this->input->post('dmq_read_time'));
				$data_to_insert["dmq_trigger_email"] = (($this->input->post('dmq_trigger_email')=='on')?'yes':'no'); 
				$StoredmqData = $this->Admin_model->StoredmqData($data_to_insert);
				if($StoredmqData){
					$previewArr = array('id'=>$StoredmqData,'heading'=>$heading);
					$this->session->set_flashdata("previewContent",$previewArr);
					$this->session->set_flashdata("Succ",'Your Article Stored Successfully! ..');
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function aboutTheDayCategoryList(){
		$data['url'] = 'admin/aboutTheDayCategoryList';

		if($this->input->post("submit")){
			$this->form_validation->set_rules("dca_date","Date","required");
			if($this->form_validation->run()== false){
				$aboutday_date =$this->input->post("aboutday_date");
				$aboutday_details = $this->Admin_model->getAboutDay($aboutday_date);
				if($aboutday_details){
					$data['aboutday_details'] = $aboutday_details;
				}else{
					$this->session->set_flashdata("Succ",'No Data Available for the Selected Date !');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}

		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function addUPSCDailyQuestionsList(){
		$data['url'] = 'admin/addUPSCDailyQuestionsList';

		if($this->input->post("submit")){
			$this->form_validation->set_rules("dmq_date","Date","required");
			if($this->form_validation->run()==true){
				$dmq_date =$this->input->post("dmq_date");
				$dmq_details = $this->Admin_model->getdmq($dmq_date);
				if($dmq_details){
					$data['dmq_details'] = $dmq_details;
				}else{
					$this->session->set_flashdata("Succ",'No Data Available for the Selected Date !');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}

		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}



	public function addUPSCPrelimQuestionsList(){
		$data['url'] = 'admin/addUPSCPrelimQuestionsList';

		if($this->input->post("submit")){
			$this->form_validation->set_rules("dpq_date","Date","required");
			if($this->form_validation->run()==true){
				$dpq_date =$this->input->post("dpq_date");
				$dpq_details = $this->Admin_model->getdpq($dpq_date);
				if($dpq_details){
					$data['dpq_details'] = $dpq_details;
				}else{
					$this->session->set_flashdata("Succ",'No Data Available for the Selected Date !');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}

		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function editAboutTheDay(){
		$data['url'] = 'admin/editAboutTheDay';

		$aboutday_id = $this->input->get('aboutday_id');
		if(empty($aboutday_id)){
			redirect('admin/aboutTheDayCategoryList');
		}

		$aboutDayId = $this->input->get('aboutday_id');
		$data['aboutDayData'] = $this->Admin_model->getAboutDayByAboutDayId($aboutDayId);
		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();



		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		if($this->input->post("submit")){
			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){

				$data_to_update['aboutday_category'] = json_encode($this->input->post('aboutday_category'));
				$data_to_update['aboutday_lang'] = trim($this->input->post('aboutday_lang'));
				$data_to_update['aboutday_featured_image'] = $this->input->post('aboutday_featured_image');
				$data_to_update['aboutday_tags'] = json_encode($this->input->post('aboutday_tags'));
				$heading = str_replace(" ","-",($this->input->post('aboutday_heading')));
				$data_to_update["aboutday_heading"] = (json_encode($heading));
				$data_to_update['aboutday_content'] = json_encode($this->input->post('aboutday_content'));
				$data_to_update["aboutday_read_time"] = trim($this->input->post('aboutday_read_time'));
				$data_to_update["aboutday_trigger_email"] = (($this->input->post('aboutday_trigger_email')=='on')?'yes':'no');    

				if($this->input->post('aboutday_date')>=date("Y-m-d")){
					$data_to_update['aboutday_upload_time'] = strtotime($this->input->post('aboutday_date'));
				}else{
					$data_to_update['aboutday_upload_time'] = strtotime(date('Y-m-d'));
				}

				$upload_time = $data_to_update['aboutday_upload_time'] = time();

				$aboutDay = $this->Admin_model->UpdateAboutDayDetailsByAboutDayId($data_to_update,$aboutDayId);

				if($aboutDay){
					$this->session->set_flashdata("Succ",'Your Article Updated Successfully! ..');
					redirect('admin/editAboutTheDay?aboutday_id='.$aboutDayId);
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}

	public function editRightBarComponent(){
		$data['url'] = 'admin/editRightBarComponent';
		$data['components'] = array(
			'governPolSchemes'=>'Government policies and Schemes',
			'staticGk'=>'Static GK',
			'historyAncientIndia'=>'History - Ancient India',
			'historyMedievalIndia'=>'History - Medieval India',
			'historyModernIndia'=>'History - Modern India',
			'indianArtCulture'=>'Indian Art and Culture',
			'geoWorld'=>'Geography - World geography',
			'geoIndian'=>'Geography - Indian Geography',
			'enbio'=>'Environment and Bio Diversity',
			'scienceTech'=>'Science and Technology',
			'indianPol'=>'Indian Polity',
			'govern'=>'Governance',
			'indianEco'=>'Indian Economy',
			'ethics'=>'Ethics',
			'optionalPaperSociology'=>'Optional Paper - Sociology',
			'optionalPaperPublicAdmin'=>'Optional Paper - Public Administration'
		);
		$rightsidebar_id = $this->input->get('rightsidebar_id');
		if(empty($rightsidebar_id)){
			redirect('admin/rightBarComponentList');
		}

		$rightsidebar_id = $this->input->get('rightsidebar_id');
		$data['rightsidebarDbData'] = $this->Admin_model->getrightsidebarByrightsidebarId($rightsidebar_id);
		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();



		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		if($this->input->post("submit")){
			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){

				$data_to_update['rightsidebar_category'] = json_encode($this->input->post('rightsidebar_category'));
				$data_to_update['rightsidebar_category'] = json_encode($this->input->post('rightsidebar_category'));
				$data_to_update['rightsidebar_lang'] = trim($this->input->post('rightsidebar_lang'));
				$heading = str_replace(" ","-",($this->input->post('rightsidebar_heading')));
				$data_to_update["rightsidebar_heading"] = (json_encode($heading));
				$data_to_update['rightsidebar_featured_image'] = $this->input->post('rightsidebar_featured_image');
				$data_to_update['rightsidebar_tags'] = json_encode($this->input->post('rightsidebar_tags'));
				$data_to_update['rightsidebar_content'] = json_encode($this->input->post('rightsidebar_content'));
				$data_to_update["rightsidebar_read_time"] = trim($this->input->post('rightsidebar_read_time'));
				$data_to_update["rightsidebar_attachments"] = trim($this->input->post('rightsidebar_attachments'));
				$data_to_update["rightsidebar_trigger_email"] = (($this->input->post('rightsidebar_trigger_email')=='on')?'yes':'no');

				if($this->input->post('rightsidebar_date')>=date("Y-m-d")){
					$data_to_update['rightsidebar_upload_time'] = strtotime($this->input->post('rightsidebar_date'));
				}else{
					$data_to_update['rightsidebar_upload_time'] = strtotime(date('Y-m-d'));
				}

				$upload_time = $data_to_update['rightsidebar_upload_time'] = time();
				$rightsidebar = $this->Admin_model->UpdaterightsidebarDetailsByrightsidebarId($data_to_update,$rightsidebar_id);
				if($rightsidebar){
					$this->session->set_flashdata("Succ",'Your Article Updated Successfully! ..');
					redirect('admin/editRightBarComponent?rightsidebar_id='.$rightsidebar_id);
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}



	public function DailyCurrentAffairsCategoryList(){
		$data['url'] = 'admin/dailyCurrentAffairsCategoryList';

		if($this->input->post("submit")){
			$this->form_validation->set_rules("dca_date","Date","required");
			if($this->form_validation->run()==true){
				$dca_date =$this->input->post("dca_date");
				$dca_details = $this->Admin_model->getDCADetails($dca_date);
				if(!empty($dca_details)){
					$data['dca_details'] = $dca_details;
				}else{
					$this->session->set_flashdata("Succ",'No Data Available for the Selected Date !');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}

		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function newsPaperAnalysisList(){
		$data['url'] = 'admin/newsPaperAnalysisList';
		if($this->input->post("submit")){
			$npa_date =$this->input->post("npa_date");
			$npa_details = $this->Admin_model->getNPADetails($npa_date);
			if($npa_details){
				$data['npa_details'] = $npa_details;
			}else{
				$this->session->set_flashdata("Succ",'No Data Available for the Selected Date !');
			}
		}
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}

	public function kurukshetraList(){
		$data['url'] = 'admin/kurukshetraList';
		if($this->input->post("submit")){
			$kurukshetra_date =$this->input->post("kurukshetra_date");
			$kurukshetra_details = $this->Admin_model->getkurukshetraDetails($kurukshetra_date);
			if($kurukshetra_details){
				$data['kurukshetra_details'] = $kurukshetra_details;
			}else{
				$this->session->set_flashdata("Succ",'No Data Available for the Selected Date !');
			}
		}
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function yojanaList(){
		$data['url'] = 'admin/yojanaList';
		if($this->input->post("submit")){
			$yoj_date =$this->input->post("yoj_date");
			$yoj_details = $this->Admin_model->getyojDetails($yoj_date);
			if($yoj_details){
				$data['yoj_details'] = $yoj_details;
			}else{
				$this->session->set_flashdata("Succ",'No Data Available for the Selected Date !');
			}
		}
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}




	public function addPreviousYearPrelimQuestions(){
		$data['url'] = 'admin/addPreviousYearPrelimQuestions';

		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();

		if($this->input->post("submit")){

			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){

				$data_to_insert["pypq_date"] = trim($this->input->post('pypq_date'));
				$data_to_insert["pypq_category"] = (json_encode($this->input->post('pypq_category')));
				$data_to_insert["pypq_lang"] = (trim($this->input->post('pypq_lang')));
				$heading = str_replace(" ","-",($this->input->post('pypq_heading')));
				$data_to_insert["pypq_heading"] = (json_encode($heading));
				$data_to_insert["pypq_tags"] = (json_encode($this->input->post('pypq_tags')));
				$data_to_insert["pypq_content"] = json_encode($this->input->post('pypq_content'));
				$upload_time = $data_to_insert['pypq_upload_time'] = time();
				$data_to_insert["pypq_featured_image"] = trim($this->input->post('pypq_featured_image'));
				$data_to_insert["pypq_attachments"] = trim($this->input->post('pypq_attachments'));
				$data_to_insert["pypq_read_time"] = trim($this->input->post('pypq_read_time'));
				$data_to_insert["pypq_trigger_email"] = (($this->input->post('pypq_trigger_email')=='on')?'yes':'no');    

				$dbPYPQStored = $this->Admin_model->StorePYPQData($data_to_insert);

				if($dbPYPQStored){
					$previewArr = array('id'=>$dbPYPQStored,'heading'=>$heading);
					$this->session->set_flashdata("previewContent",$previewArr);
					$this->session->set_flashdata("Succ",'Your Article Stored Successfully! ..');
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function addPreviousYearMainQuestions(){
		$data['url'] = 'admin/addPreviousYearMainQuestions';
		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();

		if($this->input->post("submit")){

			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){

				$data_to_insert["pymq_date"] = trim($this->input->post('pymq_date'));
				$data_to_insert["pymq_lang"] = trim($this->input->post('pymq_lang'));
				$data_to_insert["pymq_category"] = (json_encode($this->input->post('pymq_category')));
				$heading = str_replace(" ","-",($this->input->post('pymq_heading')));
				$data_to_insert["pymq_heading"] = (json_encode($heading));
				$data_to_insert["pymq_tags"] = (json_encode($this->input->post('pymq_tags')));
				$data_to_insert["pymq_content"] = json_encode($this->input->post('pymq_content'));
				$upload_time = $data_to_insert['pymq_upload_time'] = time();
				$data_to_insert["pymq_featured_image"] = trim($this->input->post('pymq_featured_image'));
				$data_to_insert["pymq_attachments"] = trim($this->input->post('pymq_attachments'));
				$data_to_insert["pymq_read_time"] = trim($this->input->post('pymq_read_time'));
				$data_to_insert["pymq_trigger_email"] = (($this->input->post('pymq_trigger_email')=='on')?'yes':'no');    

				$dbPYMQStored = $this->Admin_model->StorePYmqData($data_to_insert);

				if($dbPYMQStored){
					$previewArr = array('id'=>$dbPYMQStored,'heading'=>$heading);
					$this->session->set_flashdata("previewContent",$previewArr);
					$this->session->set_flashdata("Succ",'Your Article Stored Successfully! ..');
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}



	public function addyojana(){
		$data['url'] = 'admin/addyojana';
		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();

		if($this->input->post("submit")){

			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){

				$data_to_insert["yoj_date"] = trim($this->input->post('yoj_date'));
				$data_to_insert["yoj_lang"] = trim($this->input->post('yoj_lang'));
				$data_to_insert["yoj_category"] = (json_encode($this->input->post('yoj_category')));
				$heading = str_replace(" ","-",($this->input->post('yoj_heading')));
				$data_to_insert["yoj_heading"] = (json_encode($heading));
				$data_to_insert["yoj_tags"] = (json_encode($this->input->post('yoj_tags')));
				$data_to_insert["yoj_content"] = json_encode($this->input->post('yoj_content'));
				$upload_time = $data_to_insert['yoj_upload_time'] = time();
				$data_to_insert["yoj_featured_image"] = trim($this->input->post('yoj_featured_image'));
				$data_to_insert["yoj_attachments"] = trim($this->input->post('yoj_attachments'));
				$data_to_insert["yoj_read_time"] = trim($this->input->post('yoj_read_time'));
				$data_to_insert["yoj_trigger_email"] = (($this->input->post('yoj_trigger_email')=='on')?'yes':'no');    

				$dbyojanaStored = $this->Admin_model->StoreyojanaData($data_to_insert);

				if($dbyojanaStored){
					$previewArr = array('id'=>$dbyojanaStored,'heading'=>$heading);
					$this->session->set_flashdata("previewContent",$previewArr);
					$this->session->set_flashdata("Succ",'Your Article Stored Successfully! ..');
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function previousYearPrelimQuestionsList(){
		$data['url'] = 'admin/previousYearPrelimQuestionsList';
		if($this->input->post("submit")){
			$pypq_date =$this->input->post("pypq_date");
			$pypq_details = $this->Admin_model->getpypqDetails($pypq_date);
			if($pypq_details){
				$data['pypq_details'] = $pypq_details;
			}else{
				$this->session->set_flashdata("Succ",'No Data Available for the Selected Date !');
			}
		}
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}

	public function previousYearMainQuestionsList(){
		$data['url'] = 'admin/previousYearMainQuestionsList';
		if($this->input->post("submit")){
			$pymq_date =$this->input->post("pymq_date");
			$pymq_details = $this->Admin_model->getpymqDetails($pymq_date);
			if($pymq_details){
				$data['pymq_details'] = $pymq_details;
			}else{
				$this->session->set_flashdata("Succ",'No Data Available for the Selected Date !');
			}
		}
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}



	public function editPreviousYearPrelimQuestions(){
		$data['url'] = 'admin/editPreviousYearPrelimQuestions';

		$pypq_id = $this->input->get('pypq_id');
		if(empty($pypq_id)){
			redirect('admin/addUPSCPrelimQuestionsList');
		}
		$pypq_id = $this->input->get('pypq_id');
		$data['pypqDbData'] = $this->Admin_model->getpypqDetailsBypypq_ID($pypq_id);
		if(empty($data['pypqDbData'])){
			redirect('admin/addUPSCPrelimQuestionsList');
		}
		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();
		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		if($this->input->post("submit")){
			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){
				$heading = str_replace(" ","-",($this->input->post('pypq_heading')));
				$data_to_update["pypq_heading"] = (json_encode($heading));
				$data_to_update["pypq_category"] = json_encode($this->input->post('pypq_category'));
				$data_to_update["pypq_tags"] = json_encode($this->input->post('pypq_tags'));
				$data_to_update["pypq_content"] = json_encode($this->input->post('pypq_content'));
				$data_to_update["pypq_lang"] = trim($this->input->post('pypq_lang'));

				if($this->input->post('pypq_date')>=date("Y-m-d")){
					$data_to_update['pypq_upload_time'] = strtotime($this->input->post('pypq_date'));
				}else{
					$data_to_update['pypq_upload_time'] = strtotime(date('Y-m-d'));
				}
				$upload_time = $data_to_update['pypq_upload_time'] = time();
				$data_to_update["pypq_featured_image"] = $this->input->post('pypq_featured_image');
				$data_to_update["pypq_read_time"] = trim($this->input->post('pypq_read_time'));
				$data_to_update["pypq_attachments"] = $this->input->post('pypq_attachments');
				$data_to_update["pypq_trigger_email"] = (($this->input->post('pypq_trigger_email')=='on')?'yes':'no');    

				$dbpypqUpdated = $this->Admin_model->UpdatepypqDetailsBypypq_ID($data_to_update,$pypq_id);
				if($dbpypqUpdated){
					$this->session->set_flashdata("Succ",'Your Article Updated Successfully! ..');
					redirect('admin/editPreviousYearPrelimQuestions?pypq_id='.$pypq_id);
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}





	public function editPreviousYearMainQuestions(){
		$data['url'] = 'admin/editPreviousYearMainQuestions';

		$pymq_id = $this->input->get('pymq_id');
		if(empty($pymq_id)){
			redirect('admin/addUPSCMainQuestionsList');
		}
		$pymq_id = $this->input->get('pymq_id');
		$data['pymqDbData'] = $this->Admin_model->getpymqDetailsBypymq_ID($pymq_id);
		if(empty($data['pymqDbData'])){
			redirect('admin/addUPSCPrelimQuestionsList');
		}
		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();
		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		if($this->input->post("submit")){
			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){
				$heading = str_replace(" ","-",($this->input->post('pymq_heading')));
				$data_to_update["pymq_heading"] = (json_encode($heading));
				$data_to_update["pymq_category"] = json_encode($this->input->post('pymq_category'));
				$data_to_update["pymq_tags"] = json_encode($this->input->post('pymq_tags'));
				$data_to_update["pymq_content"] = json_encode($this->input->post('pymq_content'));
				$data_to_update["pymq_lang"] = trim($this->input->post('pymq_lang'));

				if($this->input->post('pymq_date')>=date("Y-m-d")){
					$data_to_update['pymq_upload_time'] = strtotime($this->input->post('pymq_date'));
				}else{
					$data_to_update['pymq_upload_time'] = strtotime(date('Y-m-d'));
				}
				$upload_time = $data_to_update['pymq_upload_time'] = time();
				$data_to_update["pymq_featured_image"] = $this->input->post('pymq_featured_image');
				$data_to_update["pymq_read_time"] = trim($this->input->post('pymq_read_time'));
				$data_to_update["pymq_attachments"] = $this->input->post('pymq_attachments');
				$data_to_update["pymq_trigger_email"] = (($this->input->post('pymq_trigger_email')=='on')?'yes':'no');    

				$dbpymqUpdated = $this->Admin_model->UpdatepymqDetailsBypymq_ID($data_to_update,$pymq_id);
				if($dbpymqUpdated){
					$this->session->set_flashdata("Succ",'Your Article Updated Successfully! ..');
					redirect('admin/editPreviousYearMainQuestions?pymq_id='.$pymq_id);
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}



	//Right Side Bar
	public function addRightBarComponent(){
		$data['url'] = 'admin/addRightBarComponent';

		//Start of CkEditor Plugin
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->form_validation->set_rules('sel_group', '"select Group"', 'trim|required|valid_email');
		$this->form_validation->set_rules('sel_team', '"select Team"', 'trim|required');
		$this->ckeditor->basePath = base_url().'assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array(  '-', 'Link','Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList','Outdent','Indent','Styles','Format','Anchor','Font','FontSize','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Find','Replace','TextColor','BGColor','Maximize')
		);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '99%';
		$this->ckeditor->config['height'] = '300px'; 
		//End of CKEDITOR Plugin

		$data['components'] = array(
			'governPolSchemes'=>'Government policies and Schemes',
			'staticGk'=>'Static GK',
			'historyAncientIndia'=>'History - Ancient India',
			'historyMedievalIndia'=>'History - Medieval India',
			'historyModernIndia'=>'History - Modern India',
			'indianArtCulture'=>'Indian Art and Culture',
			'geoWorld'=>'Geography - World geography',
			'geoIndian'=>'Geography - Indian Geography',
			'enbio'=>'Environment and Bio Diversity',
			'scienceTech'=>'Science and Technology',
			'indianPol'=>'Indian Polity',
			'govern'=>'Governance',
			'indianEco'=>'Indian Economy',
			'ethics'=>'Ethics',
			'optionalPaperSociology'=>'Optional Paper - Sociology',
			'optionalPaperPublicAdmin'=>'Optional Paper - Public Administration'
		);
		$data['FimagesList'] = $this->Admin_model->getFeaturedImagesList();
		$data['attachmentList'] = $this->Admin_model->getAttachmentList();

		if($this->input->post("submit")){

			$this->form_validation->set_rules("dca_date","Date","required");
			$this->form_validation->set_rules("dca_category","Category","required|min_length[5]");
			$this->form_validation->set_rules("dca_tags","Tags","required|min_length[5]");
			$this->form_validation->set_rules("dca_content","Daily Current Affairs Content","required|min_length[5]");
			$this->form_validation->set_rules("dca_trigger_email","Enable/Disable Email to Users","required");
			if($this->form_validation->run()==false){

				$data_to_insert["rightsidebar_date"] = trim($this->input->post('rightsidebar_date'));
				$data_to_insert["rightsidebar_lang"] = trim($this->input->post('rightsidebar_lang'));
				$data_to_insert["rightsidebar_category"] = (json_encode($this->input->post('rightsidebar_category')));
				$data_to_insert["rightsidebar_component"] = (trim($this->input->post('rightsidebar_component')));
				$heading = str_replace(" ","-",($this->input->post('rightsidebar_heading')));
				$data_to_insert["rightsidebar_heading"] = (json_encode($heading));
				$data_to_insert["rightsidebar_tags"] = (json_encode($this->input->post('rightsidebar_tags')));
				$data_to_insert["rightsidebar_content"] = json_encode($this->input->post('rightsidebar_content'));
				$upload_time = $data_to_insert['rightsidebar_upload_time'] = time();
				$data_to_insert["rightsidebar_featured_image"] = trim($this->input->post('rightsidebar_featured_image'));
				$data_to_insert["rightsidebar_attachments"] = trim($this->input->post('rightsidebar_attachments'));
				$data_to_insert["rightsidebar_read_time"] = trim($this->input->post('rightsidebar_read_time'));
				$data_to_insert["rightsidebar_trigger_email"] = (($this->input->post('rightsidebar_trigger_email')=='on')?'yes':'no');    	
				$dbrightsidebarStored = $this->Admin_model->StorerightsidebarData($data_to_insert);
				if($dbrightsidebarStored){
					$previewArr = array('id'=>$dbrightsidebarStored,'heading'=>$heading);
					$this->session->set_flashdata("previewContent",$previewArr);
					$this->session->set_flashdata("Succ",'Your Article Stored Successfully! ..');
				}else{
					$this->session->set_flashdata("Succ",'OOPS! Some Error Occured..Do it Again !..');
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}

	public function rightBarComponentListMethod(){
		$data['url'] = 'admin/rightBarComponentList';
		$data['components'] = array(
			'governPolSchemes'=>'Government policies and Schemes',
			'staticGk'=>'Static GK',
			'historyAncientIndia'=>'History - Ancient India',
			'historyMedievalIndia'=>'History - Medieval India',
			'historyModernIndia'=>'History - Modern India',
			'indianArtCulture'=>'Indian Art and Culture',
			'geoWorld'=>'Geography - World geography',
			'geoIndian'=>'Geography - Indian Geography',
			'enbio'=>'Environment and Bio Diversity',
			'scienceTech'=>'Science and Technology',
			'indianPol'=>'Indian Polity',
			'govern'=>'Governance',
			'indianEco'=>'Indian Economy',
			'ethics'=>'Ethics',
			'optionalPaperSociology'=>'Optional Paper - Sociology',
			'optionalPaperPublicAdmin'=>'Optional Paper - Public Administration'
		);
		if($this->input->post("submit")){
			$this->form_validation->set_rules("rightsidebar_date","Date","required");
			$this->form_validation->set_rules("rightsidebar_component","Component","required");
			if($this->form_validation->run()==true){
				$rightsidebar_date =$this->input->post("rightsidebar_date");
				$rightsidebar_component =$this->input->post("rightsidebar_component");
				$rightsidebar_details = $this->Admin_model->getrightsidebarData($rightsidebar_date,$rightsidebar_component);
				if($rightsidebar_details){
					$data['rightsidebar_details'] = $rightsidebar_details;
				}else{
					$this->session->set_flashdata("Succ",'No Data Available for the Selected Date !');
				}
			}else{
				$this->session->set_flashdata("Succ",'Enter Valid Inputs!');
				// $this->load->view("admin/common",$data);
			}
		}

		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function addFeaturedImage(){
		$data['url'] = 'admin/addFeaturedImage';
		if($this->input->post("submit")){
			$this->form_validation->set_rules("date","Date","required");
			$this->form_validation->set_rules("f_image","Daily Log","required|min_length[5]");
			if($this->form_validation->run()== false){
				$data_to_insert['f_i_name'] = $finame = trim(str_replace(' ', '', trim($this->input->post("f_i_name"))));
				$imgExist = $this->Admin_model->CheckFImageExistence($finame);
				if(!empty($imgExist)){
					$this->session->set_flashdata("Succ",'Featured Image Name Already Exist');
				}else{
						//Image Upload
					$config['upload_path'] = './uploads/featured_images/';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['file_name'] = trim($data_to_insert['f_i_name']).'.png';
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if($this->upload->do_upload('f_image')) {
						$done = $this->Admin_model->Addfeaturedimage($data_to_insert);
						if($done){
							$this->session->set_flashdata("Succ",'Featured Image Stored Successfully ! ..');
						}
					} else {
						$error = $this->upload->display_errors();
						$this->session->set_flashdata('Succ',$error);
					}
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		// $data['dbSubsData'] = $this->Admin_model->getSubsList($user);
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function editFeaturedImage(){
		$data['url'] = 'admin/editFeaturedImage';
		$f_i_id = $this->input->get('f_i_id');
		$data['dbFIData'] = $this->Admin_model->CheckFImageExistenceFI_id($f_i_id);
		if(empty($data['dbFIData'])){
			redirect('admin/featuredImageList');
		}

		// if($this->input->post("submit")){
		// 	$this->form_validation->set_rules("date","Date","required");
		// 	$this->form_validation->set_rules("f_image","Daily Log","required|min_length[5]");
		// 	if($this->form_validation->run()== false){
		// 		$data_to_insert['f_i_name'] = $finame = trim(str_replace(' ', '', trim($this->input->post("f_i_name"))));
		// 		$imgExist = $this->Admin_model->CheckFImageExistence($finame);
		// 		if(!empty($imgExist)){
		// 			$this->session->set_flashdata("Succ",'Featured Image Name Already Exist');
		// 		}else{
		// 				//Image Upload
		// 			$config['upload_path'] = './uploads/featured_images/';
		// 			$config['allowed_types'] = 'gif|jpg|jpeg|png';
		// 			$config['file_name'] = trim($data_to_insert['f_i_name']).'.png';
		// 			$this->load->library('upload', $config);
		// 			$this->upload->initialize($config);
		// 			if($this->upload->do_upload('f_image')) {
		// 				$done = $this->Admin_model->Addfeaturedimage($data_to_insert);
		// 				if($done){
		// 					$this->session->set_flashdata("Succ",'Featured Image Stored Successfully ! ..');
		// 				}
		// 			} else {
		// 				$error = $this->upload->display_errors();
		// 				$this->session->set_flashdata('Succ',$error);
		// 			}
		// 		}
		// 	}else{
		// 		$this->load->view("admin/common",$data);
		// 	}
		// }
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function addAttachment(){
		$data['url'] = 'admin/addAttachment';

		if($this->input->post("submit")){
			$this->form_validation->set_rules("date","Date","required");
			$this->form_validation->set_rules("f_image","Daily Log","required|min_length[5]");
			if($this->form_validation->run()== false){
				$data_to_store['a_name'] = $filename = trim(str_replace(' ', '', trim($this->input->post("a_name"))));
				$imgExist = $this->Admin_model->CheckAttachmentExistence($filename);
				if(!empty($imgExist)){
					$this->session->set_flashdata("Succ",'Attachment File Already Exist');
				}else{
						//Image Upload
					$config['upload_path'] = './uploads/attachments/';
					$config['allowed_types'] = 'pdf';
					$config['file_name'] = trim($data_to_store['a_name']).'.pdf';
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					$this->upload->do_upload('a_file');
					$done = $this->Admin_model->AddAttachment($data_to_store);
					if($done){
						$this->session->set_flashdata("Succ",'Hey! Attachment Stored Successfully ! ..');
					}
				}
			}else{
				$this->load->view("admin/common",$data);
			}
		}

		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function notfound(){
		$this->load->view("admin/notfound");
	}

	public function email(){
		$this->load->view('admin/email');
	}

	public function logout(){
		$this->load->view('admin/logout');
	}

}
?>
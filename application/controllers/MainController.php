<?php 
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class MainController extends CI_Controller{
	public function __construct(){
		parent :: __construct();
		$this->load->model("Mainmodel");
		$this->load->helper(array('form', 'url','security')); 
		$this->load->helper('url'); 
		$this->load->library(array("form_validation",'session','upload','pagination'));
		//Constants for Google Login
		define("CLIENTID","494841187585-3usih70pbtbtdbogpqha1hsq7bll13d0.apps.googleusercontent.com");
		define("CLIENTSECRET","F9ZJZU1kiMjEgZWG22FbKrw6");
		define("APPLICATIONNAME","future officers");
		define("LOCALREDIRECTURI","http://localhost/futureofficers/MainController/GoogleLogin/");
		define("LIVEREDIRECTURI","https://www.futureofficers.in/MainController/GoogleLogin");
		define("SCOPE","https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email");
	}


	public function GoogleLogin(){
		require_once "packages/googleLogin/google-api/vendor/autoload.php";
		$gClient = new Google_Client();
		$gClient->setClientId(CLIENTID);
		$gClient->setClientSecret(CLIENTSECRET);
		$gClient->setApplicationName(APPLICATIONNAME);
		$gClient->setRedirectUri(LOCALREDIRECTURI);
		$gClient->addScope(SCOPE);
		$login_url = $gClient->createAuthUrl();
		if (empty($_GET['code'])){
			redirect($login_url);
		}else{
			$token = $gClient->fetchAccessTokenWithAuthCode($_GET['code']);
			$oAuth = new Google_Service_Oauth2($gClient);
			$userData = $oAuth->userinfo_v2_me->get();
			if(!empty($userData['email'])and(!empty($userData['picture']))){
				$this->session->set_userdata('auth',md5($userData['email']));
				$this->session->set_userdata('useremail',$userData['email']);
				$this->session->set_userdata('userpicture',$userData['picture']);
				redirect("homepage");
			}else{
				$this->session->set_flashdata("msg","Please enter valid inputs and try again !");
				redirect("homepage");
			}
		}
	}


	public function commonMethod(){
		$this->load->view("common");
	}

	public function HeaderMethod(){
		$this->load->view('header');
	}

	public function SignUp(){
		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('useremail','Email','required|valid_email');
		$this->form_validation->set_rules('password','Password','required|min_length[8]|max_length[8]');
		if($this->form_validation->run() == true){
			$signup['username'] = $this->input->post("username");
			$signup['useremail'] = $useremail =  $this->input->post("useremail");
			$signup['password'] = md5($this->input->post("password"));
			$userExists = $this->Mainmodel->checkUserExists($useremail);
			if($userExists){
				$this->session->set_flashdata("msg","User Already Exist. Please Sign In with Email ID !");
				redirect('homepage');
			}else{
				$createUser = $this->Mainmodel->UserSignUp($signup);
				$this->session->set_flashdata("msg","Thank you for registering ! Please Sign In to continue");
				redirect("homepage");
			}
		}else{
			$this->session->set_flashdata("msg","Please Enter Valid Data");
			redirect("homepage");
		}
	}

	public function SignIn(){
		$this->form_validation->set_rules('useremail','Email','required|valid_email');
		$this->form_validation->set_rules('password','Password','required|min_length[8]|max_length[8]');
		if($this->form_validation->run() == true){
			$signin['useremail'] = $useremail =  $this->input->post("useremail");
			$signin['password'] = md5($this->input->post("password"));
			$userExists = $this->Mainmodel->UserSignIn($signin);
			if($userExists){
				$this->session->set_userdata('auth',md5($signin['useremail']));
				$this->session->set_userdata('useremail',$signin['useremail']);
				redirect("homepage");
			}else{
				$this->session->set_flashdata("msg","Invalid Login Credentials !");
				redirect('homepage');
			}
		}else{
			$this->session->set_flashdata("msg","Please Enter Valid Data");
			redirect("homepage");
		}
	}



	public function Subscribe(){
		$this->form_validation->set_rules('sub_email','Email','required|valid_email');
		if($this->form_validation->run() == true){
			$signup['sub_email'] = $useremail =  $this->input->post("sub_email");
			$userExists = $this->Mainmodel->checkEmailSubscription($signup);
			if($userExists){
				$this->session->set_flashdata("msg","User Already Exist. Please Sign In with Email ID !");
				redirect('homepage');
			}else{
				$createUser = $this->Mainmodel->AddSubscription($signup);
				$this->session->set_flashdata("msg","Thank you for Subscribing !");
				redirect("homepage");
			}
		}else{
			$this->session->set_flashdata("msg","Please Enter Valid Data");
			redirect("homepage");
		}
	}



	public function HomepageMethod(){
		$data['url'] = 'homepage';
		//Featured Image - Start
		$data['FeaturedImageList'] = $this->Mainmodel->getFeaturedImagesList();

		//Daily Current Affairs List
		$data['dcaList'] = $this->Mainmodel->getDCAList();
		foreach ($data['dcaList'] as $dcaKey => $dcaValue) {
			foreach ($data['FeaturedImageList'] as $fiKey => $fiValue) {
				if($dcaValue['dca_featured_image']==$fiValue['f_i_id']){
					$data['dcaList'][$dcaKey]['dca_featured_image'] = trim($fiValue['f_i_name']);
				}
			}
		}
		//About the Day List
		$data['aboutdayList'] = $this->Mainmodel->getAboutDayList();
		foreach ($data['aboutdayList'] as $aboutKey => $aboutVal) {
			foreach ($data['FeaturedImageList'] as $fiKey1 => $fiValue1) {
				if($aboutVal['aboutday_featured_image']==$fiValue1['f_i_id']){
					$data['aboutdayList'][$aboutKey]['aboutday_featured_image'] = trim($fiValue1['f_i_name']);
				}
			}
		}
		//News Paper Analysis
		$data['npaList'] = $this->Mainmodel->getNPAList();
		foreach ($data['npaList'] as $npakey => $npaval) {
			foreach ($data['FeaturedImageList'] as $fiKey1 => $fiValue1) {
				if($npaval['npa_featured_image']==$fiValue1['f_i_id']){
					$data['npaList'][$npakey]['npa_featured_image'] = trim($fiValue1['f_i_name']);
				}
			}
		}

		$this->load->view("common",$data);
	}



	public function DailyCurrentAffairs(){
		$data['url'] = 'dailyCurrentAffairs';
		if(!empty($this->input->get('selectedDate'))){
			$cal_date = $this->input->get('selectedDate');
			$calData = $this->Mainmodel->getdcaListByCalendarDate($cal_date);
			if(!empty($calData)){
				$rURL = site_url()."dailyCurrentAffairs?dca_id=".strrev(base64_encode($calData[0]['dca_id']))."&heading=".strtolower(json_decode($calData[0]['dca_heading']));
				redirect($rURL,'refresh');
			}else{
				redirect('CalendarDateDataNotFound');
			}
		}



		$dcaId = base64_decode(strrev($this->input->get('dca_id')));
		if(!empty($dcaId)){
			$data['dca'] = $this->Mainmodel->getDCADetailsByDCA_ID($dcaId);
			if(empty($data['dca'])){
				redirect('homepage');
			}
		}else{
			redirect('homepage');
		}
		$attachments = $this->Mainmodel->getAttachmentList();
		foreach ($data['dca'] as $dcaKey1 => $dcaValue1) {
			foreach ($attachments as $aKey => $aValue) {
				if($dcaValue1['dca_attachments']==$aValue['a_id']){
					$data['dca'][$dcaKey1]['dca_attachments'] = trim($aValue['a_name']);
				}
			}
		}
		$this->load->view("common",$data);
	}

	public function aboutTheDay(){
		$data['url'] = 'aboutTheDay';
		if(!empty($this->input->get('selectedDate'))){
			$cal_date = $this->input->get('selectedDate');
			$CalData = $this->Mainmodel->getAboutDayListByCalendarDate($cal_date);
			if(!empty($CalData)){
				$rURL = site_url()."aboutTheDay?aboutday_id=".strrev(base64_encode($CalData[0]['aboutday_id']))."&heading=".strtolower(json_decode($CalData[0]['aboutday_heading']));
				redirect($rURL,'refresh');
			}else{
				redirect('CalendarDateDataNotFound');
			}
		}

		$aboutday_id = base64_decode(strrev($this->input->get('aboutday_id')));
		if(!empty($aboutday_id)){
			$data['aboutTheDayData'] = $this->Mainmodel->getAboutDayListByID($aboutday_id);
			if(empty($data['aboutTheDayData'])){
				redirect('homepage');
			}
		}else{
			redirect('homepage');
		}

		$this->load->view("common",$data);
	}





	public function DailyCurrentAffairsListMethod(){
		$data['url'] = 'dailyCurrentAffairsList';
		$month = trim($this->input->get('month'));
		$year = trim($this->input->get('year'));
		$lang = trim($this->input->get('lang'));
		if(empty((int)$month)or(empty((int)$year))or(empty((int)$lang))){
			redirect('homepage');
		}
		$data['dcaMonthlyList'] = $this->Mainmodel->getDCAListByMonth($month,$year,$lang);
		if(empty($data['dcaMonthlyList'])){
			redirect('homepage');
		}
		$data['FeaturedImageList'] = $this->Mainmodel->getFeaturedImagesList();
		foreach ($data['dcaMonthlyList'] as $dcaKey => $dcaValue) {
			foreach ($data['FeaturedImageList'] as $fiKey => $fiValue) {
				if($dcaValue['dca_featured_image']==$fiValue['f_i_id']){
					$data['dcaMonthlyList'][$dcaKey]['dca_featured_image'] = trim($fiValue['f_i_name']);
				}
			}
		}
		$this->load->view("common",$data);
	}

	public function aboutTheDayList(){
		$data['url'] = 'aboutTheDayList';
		$month = trim($this->input->get('month'));
		$year = trim($this->input->get('year'));
		$lang = trim($this->input->get('lang'));
		if(empty((int)$month)or(empty((int)$year))or(empty((int)$lang))){
			redirect('homepage');
		}
		$data['aboutDayMonthlyList'] = $this->Mainmodel->getABboutDayListByMonth($month,$year,$lang);
		if(empty($data['aboutDayMonthlyList'])){
			redirect('homepage');
		}
		$data['FeaturedImageList'] = $this->Mainmodel->getFeaturedImagesList();
		foreach ($data['aboutDayMonthlyList'] as $dcaKey => $dcaValue) {
			foreach ($data['FeaturedImageList'] as $fiKey => $fiValue) {
				if($dcaValue['aboutday_featured_image']==$fiValue['f_i_id']){
					$data['aboutDayMonthlyList'][$dcaKey]['aboutday_featured_image'] = trim($fiValue['f_i_name']);
				}
			}
		}
		$this->load->view("common",$data);
	}



	public function newsPaperAnalysisListMethod(){
		$data['url'] = 'newsPaperAnalysisList';
		$month = ($this->input->get('month'));
		$year = ($this->input->get('year'));

		if(empty((int)$month)or(empty((int)$year))){
			redirect('homepage');
		}
		$data['npaMonthlyList'] = $this->Mainmodel->getNPAListByMonth($month,$year);
		if(empty($data['npaMonthlyList'])){
			redirect('homepage');
		}
		$data['FeaturedImageList'] = $this->Mainmodel->getFeaturedImagesList();
		foreach ($data['npaMonthlyList'] as $npaKey => $npaValue) {
			foreach ($data['FeaturedImageList'] as $fiKey => $fiValue) {
				if($npaValue['npa_featured_image']==$fiValue['f_i_id']){
					$data['npaMonthlyList'][$npaKey]['npa_featured_image'] = trim($fiValue['f_i_name']);
				}
			}
		}
		$this->load->view("common",$data);
	}


	public function yojanaListMethod(){
		$data['url'] = 'yojanaList';
		$month = ($this->input->get('month'));
		$year = ($this->input->get('year'));
		if(empty((int)$month)or(empty((int)$year))){
			redirect('homepage');
		}
		$data['yojanaMonthlyList'] = $this->Mainmodel->getyojanaListByMonth($month,$year);
		if(empty($data['yojanaMonthlyList'])){
			redirect('homepage');
		}
		$data['FeaturedImageList'] = $this->Mainmodel->getFeaturedImagesList();
		foreach ($data['yojanaMonthlyList'] as $yojanaKey => $yojanaValue) {
			foreach ($data['FeaturedImageList'] as $fiKey => $fiValue) {
				if($yojanaValue['yoj_featured_image']==$fiValue['f_i_id']){
					$data['yojanaMonthlyList'][$yojanaKey]['yoj_featured_image'] = trim($fiValue['f_i_name']);
				}
			}
		}
		$this->load->view("common",$data);
	}



	public function preYearMainQuesListMethod(){
		$data['url'] = 'preYearMainQuesList';
		$month = ($this->input->get('month'));
		$year = ($this->input->get('year'));
		if(empty((int)$month)or(empty((int)$year))){
			redirect('homepage');
		}
		$data['pymqMonthlyList'] = $this->Mainmodel->getpymqListByMonth($month,$year);
		if(empty($data['pymqMonthlyList'])){
			redirect('homepage');
		}
		$data['FeaturedImageList'] = $this->Mainmodel->getFeaturedImagesList();
		foreach ($data['pymqMonthlyList'] as $pymqKey => $pymqValue) {
			foreach ($data['FeaturedImageList'] as $fiKey => $fiValue) {
				if($pymqValue['pymq_featured_image']==$fiValue['f_i_id']){
					$data['pymqMonthlyList'][$pymqKey]['pymq_featured_image'] = trim($fiValue['f_i_name']);
				}
			}
		}
		$this->load->view("common",$data);
	}


	public function preYearPrelimQuesListMethod(){
		$data['url'] = 'preYearPrelimQuesList';
		$month = ($this->input->get('month'));
		$year = ($this->input->get('year'));
		if(empty((int)$month)or(empty((int)$year))){
			redirect('homepage');
		}
		$data['pypqMonthlyList'] = $this->Mainmodel->getpypqListByMonth($month,$year);
		if(empty($data['pypqMonthlyList'])){
			redirect('homepage');
		}
		$data['FeaturedImageList'] = $this->Mainmodel->getFeaturedImagesList();
		foreach ($data['pypqMonthlyList'] as $pypqKey => $pypqValue) {
			foreach ($data['FeaturedImageList'] as $fiKey => $fiValue) {
				if($pypqValue['pypq_featured_image']==$fiValue['f_i_id']){
					$data['pypqMonthlyList'][$pypqKey]['pypq_featured_image'] = trim($fiValue['f_i_name']);
				}
			}
		}
		$this->load->view("common",$data);
	}


	public function kurukshetraList(){
		$data['url'] = 'kurukshetraList';
		$month = ($this->input->get('month'));
		$year = ($this->input->get('year'));
		if(empty((int)$month)or(empty((int)$year))){
			redirect('homepage');
		}
		$data['kurukshetraMonthlyList'] = $this->Mainmodel->getkurukshetraListByMonth($month,$year);
		if(empty($data['kurukshetraMonthlyList'])){
			redirect('homepage');
		}
		$data['FeaturedImageList'] = $this->Mainmodel->getFeaturedImagesList();
		foreach ($data['kurukshetraMonthlyList'] as $kurukshetraKey => $kurukshetraValue) {
			foreach ($data['FeaturedImageList'] as $fiKey => $fiValue) {
				if($kurukshetraValue['kurukshetra_featured_image']==$fiValue['f_i_id']){
					$data['kurukshetraMonthlyList'][$kurukshetraKey]['kurukshetra_featured_image'] = trim($fiValue['f_i_name']);
				}
			}
		}
		$this->load->view("common",$data);
	}





	// Daily UPSC Prelims Questions

	public function dailyUPSCPrelimsQuestions(){
		$data['url'] = 'dailyUPSCPrelimsQuestions';
		$month = ($this->input->get('month'));
		$year = ($this->input->get('year'));
		if(empty((int)$month)or(empty((int)$year))){
			redirect('homepage');
		}
		$data['dailyUPSCPrelimsQuesMonthlyList'] = $this->Mainmodel->getdailyUPSCPRelimsQuesListByMonth($month,$year);
		if(empty($data['dailyUPSCPrelimsQuesMonthlyList'])){
			redirect('homepage');
		}
		$data['FeaturedImageList'] = $this->Mainmodel->getFeaturedImagesList();
		foreach ($data['dailyUPSCPrelimsQuesMonthlyList'] as $npaKey => $npaValue) {
			foreach ($data['FeaturedImageList'] as $fiKey => $fiValue) {
				if($npaValue['dpq_featured_image']==$fiValue['f_i_id']){
					$data['dailyUPSCPrelimsQuesMonthlyList'][$npaKey]['dpq_featured_image'] = trim($fiValue['f_i_name']);
				}
			}
		}
		$this->load->view("common",$data);
	}

	public function dailyUPSCPrelimsQuestionsPage(){
		$data['url'] = 'dailyUPSCPrelimsQuestionsPage';
		// By Calendar Date
		if(!empty($this->input->get('selectedDate'))){
			$dpq_cal_date = $this->input->get('selectedDate');
			$dpqData = $this->Mainmodel->getdpqListByCalendarDate($dpq_cal_date);
			if(!empty($dpqData)){
				$rURL = site_url()."dailyUPSCPrelimsQuestionsPage?dpq_id=".strrev(base64_encode($dpqData[0]['dpq_id']))."&heading=".strtolower(json_decode($dpqData[0]['dpq_heading']));
				redirect($rURL,'refresh');
			}else{
				redirect('CalendarDateDataNotFound');
			}
		}
		//by DPQ ID
		$dpq_id = base64_decode(strrev($this->input->get('dpq_id')));
		if(!empty($dpq_id)){
			$data['dpqData'] = $this->Mainmodel->getdpqListByID($dpq_id);
			if(empty($data['dpqData'])){
				redirect('homepage');
			}
		}

		$this->load->view("common",$data);
	}


	//dailyUPSCMainQuestions
	public function dailyUPSCMainQuestions(){
		$data['url'] = 'dailyUPSCMainQuestions';
		$month = ($this->input->get('month'));
		$year = ($this->input->get('year'));
		if(empty((int)$month)or(empty((int)$year))){
			redirect('homepage');
		}
		$data['dailyUPSCMainQuesMonthlyList'] = $this->Mainmodel->getdailyUPSCMainQuesListByMonth($month,$year);
		if(empty($data['dailyUPSCMainQuesMonthlyList'])){
			redirect('homepage');
		}
		$data['FeaturedImageList'] = $this->Mainmodel->getFeaturedImagesList();
		foreach ($data['dailyUPSCMainQuesMonthlyList'] as $npaKey => $npaValue) {
			foreach ($data['FeaturedImageList'] as $fiKey => $fiValue) {
				if($npaValue['dmq_featured_image']==$fiValue['f_i_id']){
					$data['dailyUPSCMainQuesMonthlyList'][$npaKey]['dmq_featured_image'] = trim($fiValue['f_i_name']);
				}
			}
		}
		$this->load->view("common",$data);
	}	

	public function dailyUPSCMainQuestionsPage(){
		$data['url'] = 'dailyUPSCMainQuestionsPage';

		if(!empty($this->input->get('selectedDate'))){
			$cal_date = $this->input->get('selectedDate');
			$calData = $this->Mainmodel->getdmqqListByCalendarDate($cal_date);
			if(!empty($calData)){
				$rURL = site_url()."dailyUPSCMainQuestionsPage?dmq_id=".strrev(base64_encode($calData[0]['dmq_id']))."&heading=".strtolower(json_decode($calData[0]['dmq_heading']));
				redirect($rURL,'refresh');
			}else{
				redirect('CalendarDateDataNotFound');
			}
		}


		$dmq_id = base64_decode(strrev($this->input->get('dmq_id')));
		if(!empty($dmq_id)){
			$data['dmqData'] = $this->Mainmodel->getdmqListByID($dmq_id);
			if(empty($data['dmqData'])){
				redirect('homepage');
			}
		}else{
			redirect('homepage');
		}




		$this->load->view("common",$data);
	}



	public function rightsidebarPages(){
		$data['url'] = 'rightsidebarPages';
		$component = ($this->input->get('component'));
		if(empty($component)){
			redirect('homepage');
		}
		$data['rightSideBarData'] = $this->Mainmodel->getRightSideBarListByComponent($component);

		if(empty($data['rightSideBarData'])){
			redirect('homepage');
		}

		$data['FeaturedImageList'] = $this->Mainmodel->getFeaturedImagesList();
		foreach ($data['rightSideBarData'] as $npaKey => $npaValue) {
			foreach ($data['FeaturedImageList'] as $fiKey => $fiValue) {
				if($npaValue['rightsidebar_featured_image']==$fiValue['f_i_id']){
					$data['rightSideBarData'][$npaKey]['rightsidebar_featured_image'] = trim($fiValue['f_i_name']);
				}
			}
		}
		$this->load->view("common",$data);
	}


	public function rightsidebarInnerPages(){
		$data['url'] = 'rightsidebarInnerPages';
		$rightsidebar_id = base64_decode(strrev($this->input->get('rightsidebar_id')));
		if(!empty($rightsidebar_id)){
			$data['rightSideBarInnerPageData'] = $this->Mainmodel->getrightSideBarInnerPageDataByID($rightsidebar_id);
			if(empty($data['rightSideBarInnerPageData'])){
				redirect('homepage');
			}
		}else{
			redirect('homepage');
		}

		$this->load->view("common",$data);
	}

	public function newsPaperAnalysis(){
		$data['url'] = 'newsPaperAnalysis';

		if(!empty($this->input->get('selectedDate'))){
			$cal_date = $this->input->get('selectedDate');
			$npa = $this->Mainmodel->getnpaListByCalendarDate($cal_date);
			if(!empty($npa)){
				$rURL = site_url()."newsPaperAnalysis?npa_id=".strrev(base64_encode($npa[0]['npa_id']))."&heading=".strtolower(json_decode($npa[0]['npa_heading']));
				redirect($rURL,'refresh');
			}else{
				redirect('CalendarDateDataNotFound');
			}
		}


		$npaId = base64_decode(strrev($this->input->get('npa_id')));
		if(!empty($npaId)){
			$data['npa'] = $this->Mainmodel->getNPADetailsByNPA_ID($npaId);
			if(empty($data['npa'])){
				redirect('homepage');
			}
		}else{
			redirect('homepage');
		}
		$attachments = $this->Mainmodel->getAttachmentList();
		foreach ($data['npa'] as $npakey => $npaVal) {
			foreach ($attachments as $aKey => $aValue) {
				if($npaVal['npa_attachments']==$aValue['a_id']){
					$data['npa'][$npakey]['npa_attachments'] = trim($aValue['a_name']);
				}
			}
		}
		$this->load->view("common",$data);
	}



	public function yojana(){
		$data['url'] = 'yojana';
		if(!empty($this->input->get('selectedDate'))){
			$cal_date = $this->input->get('selectedDate');
			$yojanaData = $this->Mainmodel->getyojanaListByCalendarDate($cal_date);
			if(!empty($yojanaData)){
				$rURL = site_url()."yojana?yoj_id=".strrev(base64_encode($yojanaData[0]['yoj_id']))."&heading=".strtolower(json_decode($yojanaData[0]['yoj_heading']));
				redirect($rURL,'refresh');
			}else{
				redirect('CalendarDateDataNotFound');
			}
		}


		$yojId = base64_decode(strrev($this->input->get('yoj_id')));
		if(!empty($yojId)){
			$data['yoj'] = $this->Mainmodel->getyojDetailsByyoj_ID($yojId);
			if(empty($data['yoj'])){
				redirect('homepage');
			}
		}else{
			redirect('homepage');
		}
		$attachments = $this->Mainmodel->getAttachmentList();
		foreach ($data['yoj'] as $yojkey => $yojVal) {
			foreach ($attachments as $aKey => $aValue) {
				if($yojVal['yoj_attachments']==$aValue['a_id']){
					$data['yoj'][$yojkey]['yoj_attachments'] = trim($aValue['a_name']);
				}
			}
		}
		$this->load->view("common",$data);
	}


	public function preYearMainQues(){
		$data['url'] = 'preYearMainQues';

		if(!empty($this->input->get('selectedDate'))){
			$cal_date = $this->input->get('selectedDate');
			$pymqData = $this->Mainmodel->getpymqListByCalendarDate($cal_date);
			if(!empty($pymqData)){
				$rURL = site_url()."preYearMainQues?pymq_id=".strrev(base64_encode($pymqData[0]['pymq_id']))."&heading=".strtolower(json_decode($pymqData[0]['pymq_heading']));
				redirect($rURL,'refresh');
			}else{
				redirect('CalendarDateDataNotFound');
			}
		}


		$pymqId = base64_decode(strrev($this->input->get('pymq_id')));
		if(!empty($pymqId)){
			$data['pymq'] = $this->Mainmodel->getpymqDetailsBypymq_ID($pymqId);
			if(empty($data['pymq'])){
				redirect('homepage');
			}
		}else{
			redirect('homepage');
		}
		$attachments = $this->Mainmodel->getAttachmentList();
		foreach ($data['pymq'] as $pymqkey => $pymqVal) {
			foreach ($attachments as $aKey => $aValue) {
				if($pymqVal['pymq_attachments']==$aValue['a_id']){
					$data['pymq'][$pymqkey]['pymq_attachments'] = trim($aValue['a_name']);
				}
			}
		}
		$this->load->view("common",$data);
	}


	public function preYearPrelimQues(){
		$data['url'] = 'preYearPrelimQues';


		if(!empty($this->input->get('selectedDate'))){
			$cal_date = $this->input->get('selectedDate');
			$pypq = $this->Mainmodel->getpypqListByCalendarDate($cal_date);
			if(!empty($pypq)){
				$rURL = site_url()."preYearPrelimQues?pypq_id=".strrev(base64_encode($pypq[0]['pypq_id']))."&heading=".strtolower(json_decode($pypq[0]['pypq_heading']));
				redirect($rURL,'refresh');
			}else{
				redirect('CalendarDateDataNotFound');
			}
		}



		$pypqId = base64_decode(strrev($this->input->get('pypq_id')));
		if(!empty($pypqId)){
			$data['pypq'] = $this->Mainmodel->getpypqDetailsBypypq_ID($pypqId);
			if(empty($data['pypq'])){
				redirect('homepage');
			}
		}else{
			redirect('homepage');
		}
		$attachments = $this->Mainmodel->getAttachmentList();
		foreach ($data['pypq'] as $pypqkey => $pypqVal) {
			foreach ($attachments as $aKey => $aValue) {
				if($pypqVal['pypq_attachments']==$aValue['a_id']){
					$data['pypq'][$pypqkey]['pypq_attachments'] = trim($aValue['a_name']);
				}
			}
		}
		$this->load->view("common",$data);
	}



	public function kurukshetra(){
		$data['url'] = 'kurukshetra';

		if(!empty($this->input->get('selectedDate'))){
			$cal_date = $this->input->get('selectedDate');
			$kurukshetraData = $this->Mainmodel->getkurukshetraListByCalendarDate($cal_date);
			if(!empty($kurukshetraData)){
				$rURL = site_url()."kurukshetra?kurukshetra_id=".strrev(base64_encode($kurukshetraData[0]['kurukshetra_id']))."&heading=".strtolower(json_decode($kurukshetraData[0]['kurukshetra_heading']));
				redirect($rURL,'refresh');
			}else{
				redirect('CalendarDateDataNotFound');
			}
		}


		$kurukshetraId = base64_decode(strrev($this->input->get('kurukshetra_id')));
		if(!empty($kurukshetraId)){
			$data['kurukshetra'] = $this->Mainmodel->getkurukshetraDetailsBykurukshetra_ID($kurukshetraId);
			if(empty($data['kurukshetra'])){
				redirect('homepage');
			}
		}else{
			redirect('homepage');
		}
		$attachments = $this->Mainmodel->getAttachmentList();
		foreach ($data['kurukshetra'] as $kurukshetrakey => $kurukshetraVal) {
			foreach ($attachments as $aKey => $aValue) {
				if($kurukshetraVal['kurukshetra_attachments']==$aValue['a_id']){
					$data['kurukshetra'][$kurukshetrakey]['kurukshetra_attachments'] = trim($aValue['a_name']);
				}
			}
		}
		$this->load->view("common",$data);
	}




	public function notfound(){
		$this->load->view("notfound");
	}

	public function CalendarDateDataNotFound(){
		$data['url'] = 'CalendarDateDataNotFound';
		$this->load->view("common",$data);
	}

	public function SignOut(){
		session_destroy();
		redirect("homepage");
	}

}
?>